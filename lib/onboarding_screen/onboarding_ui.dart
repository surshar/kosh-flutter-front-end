import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/login_screen/login_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnboardingUi extends StatefulWidget {
  @override
  _OnboardingUiState createState() => _OnboardingUiState();
}

class _OnboardingUiState extends State<OnboardingUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  PageController _pageController = new PageController();
  var btnVisible = false;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor:
          adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            appBar(
                true,
                height * 0.1,
                width,
                adaptiveThemeMode == AdaptiveThemeMode.light
                    ? textdarkblue
                    : white,
                adaptiveThemeMode == AdaptiveThemeMode.light
                    ? textgrey
                    : white),
            Container(
              height: height * 0.69,
              child: buildPageView(context, height, width),
            ),
            Container(
              height: height * 0.04,
              child: Center(
                child: SmoothPageIndicator(
                  controller: _pageController,
                  count: 3,
                  effect: ExpandingDotsEffect(
                    activeDotColor: adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : textgreen,
                    dotColor: greyindicator,
                    dotHeight: width * 0.02,
                    dotWidth: width * 0.02,
                    radius: 16,
                  ),
                  onDotClicked: (index) => _pageController.animateToPage(index,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.bounceInOut),
                ),
              ),
            ),
            Expanded(
              child: Visibility(
                visible: btnVisible == true ? true : false,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          Get.offAll(()=>LoginUi());
                        },
                        shape: CircleBorder(),
                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                            ? buttongreen
                            : buttongreen,
                        child: Padding(
                          padding: EdgeInsets.all(width * 0.05),
                          child: Icon(
                            Icons.arrow_forward,
                            color: white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildPageView(BuildContext context, double height, double width) {
    return PageView(
      controller: _pageController,
      onPageChanged: (page) {
        if (page == 2) {
          btnVisible = true;
          setState(() {});
        } else {
          btnVisible = false;
          setState(() {});
        }
      },
      children: [
        buildPage(
            context, height, width, TITLEONE, 'assets/cuate.svg', SUBTITLEONE),
        buildPage(context, height, width, TITLETWO,
            'assets/onboarding_second.svg', SUBTITLETWO),
        buildPage(context, height, width, TITLETHREE, 'assets/onboarding_third.svg',
            SUBTITLETHREE)
      ],
    );
  }

  buildPage(BuildContext context, double height, double width, String title,
      String imageName, String subTitle) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            height: height * 0.1,
            width: width,
            margin: EdgeInsets.only(top: height * 0.02),
            child: textWidget(
                title,
                ScreenUtil().setSp(80, allowFontScalingSelf: true),
                FontWeight.w600,
                adaptiveThemeMode == AdaptiveThemeMode.light
                    ? textdarkblue
                    : white,
                TextAlign.start)),
        Container(
          height: height * 0.5,
          width: height * 0.5,
          child: Container(
              margin: EdgeInsets.all(width * 0.05),
              child: SvgPicture.asset(imageName)),
        ),
        Container(
            height: height * 0.06,
            width: width,
            child: textWidget(
                subTitle,
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w400,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.center)),
      ],
    );
  }
}
