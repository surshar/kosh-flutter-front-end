import 'dart:convert';
import 'dart:io';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ApiClient {
  Future<http.Response> getLoginResponse(String email, String password) async {
    var postBody = jsonEncode({"email": "$email", "password": "$password"});

    final response = await http.post("${BASE_URL}/api/auth/login",
        headers: {"Content-Type": "application/json", "Accept": "application/json"},
        body: postBody);
    return response;
  }

  Future<http.Response> getSignUpResponse(String firstName, String lastName, String email,
      String password, String confirmPassword, String dob, String mobile_no) async {
    var headers = {'Content-Type': 'application/json', 'Accept': 'application/json'};
    var request = http.Request('POST', Uri.parse('${BASE_URL}/api/auth/register'));
    request.body = json.encode({
      "firstName": firstName,
      "lastName": lastName,
      "email": email,
      "password": password,
      "confirmPassword": confirmPassword,
      "dob": dob,
      "mobile_no": mobile_no
    });
    request.headers.addAll(headers);
    var streamedResponse = await request.send();
    return await http.Response.fromStream(streamedResponse);
  }

  Future<http.Response> getResetPasswordResponse(String email) async {
    var postBody = jsonEncode({"email": "$email"});
    final response = await http.post("${BASE_URL}/api/auth/reset-password",
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
        body: postBody);
    return response;
  }

  Future<http.Response> getChangePasswordResponse(String email, String otp, String password) async {
    var postBody = jsonEncode({"email": "$email", "otp": "$otp", "password": "$password"});
    final response = await http.post("${BASE_URL}/api/auth/change-password",
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
        body: postBody);
    return response;
  }

  Future<http.Response> getLogoutResponse() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);
    String refreshToken = preferences.getString(REFRESHTOKEN);

    Map<String, String> mheaders = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $authKey'
    };

    var postBody = jsonEncode({"refreshToken": "$refreshToken"});
    final response =
        await http.post("${BASE_URL}/api/auth/logout", headers: mheaders, body: postBody);
    return response;
  }

  Future<http.Response> getKycDetailsResponse(String verification_type) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);
    Map<String, String> mheaders = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $authKey'
    };

    var postBody = jsonEncode({"verification_type": "$verification_type"});
    final response =
        await http.post("${BASE_URL}/api/users/initiate-kyc", headers: mheaders, body: postBody);
    return response;
  }

  Future<http.Response> getAllBucketResponse() async {
    var request = http.Request('POST', Uri.parse('${BASE_URL}/api/buckets/view-all'));
    request.body = '''''';

    var streamedResponse = await request.send();
    return await http.Response.fromStream(streamedResponse);
  }

  Future<http.Response> getBucketDetailResponse(int bucket_id) async {
    var headers = {'Content-Type': 'application/json'};
    var request = http.Request('POST', Uri.parse('${BASE_URL}/api/buckets/view'));
    request.body = json.encode({"bucket_id": bucket_id});
    request.headers.addAll(headers);

    var response = await request.send();
    return await http.Response.fromStream(response);
  }

  Future<http.Response> getkycDocumentResponse(File imageFile, String vaultLink) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);
    Get.dialog(CupertinoActivityIndicator());
    var headers = {'Authorization': 'bearer ${authKey}'};
    var request =
        http.MultipartRequest('POST', Uri.parse('${BASE_URL}/api/users/upload-kyc-document'));
    request.fields.addAll({'url': '${vaultLink}'});
    request.files.add(await http.MultipartFile.fromPath('image', '${imageFile.path}'));
    request.headers.addAll(headers);

    var streamedResponse = await request.send();
    Get.back();
    return await http.Response.fromStream(streamedResponse);
  }

  Future<http.Response> getKycVerifyResponse(String doc_type, String front_or_back) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);
    var headers = {'Authorization': 'bearer ${authKey}', 'Content-Type': 'application/json'};
    var request = http.Request('POST', Uri.parse('${BASE_URL}/api/users/verify-document'));
    request.body = json.encode({"doc_type": "${doc_type}", "front_or_back": "${front_or_back}"});
    request.headers.addAll(headers);
    var streamedResponse = await request.send();

    return await http.Response.fromStream(streamedResponse);
  }

  Future<http.Response> getSelfieResponse() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);
    Get.dialog(CupertinoActivityIndicator());

    var headers = {'Authorization': 'bearer ${authKey}'};
    var request = http.Request('POST', Uri.parse('${BASE_URL}/api/users/verify-selfie'));
    request.body = '''''';
    request.headers.addAll(headers);

    var streamedResponse = await request.send();
    Get.back();
    return await http.Response.fromStream(streamedResponse);
  }

  Future<http.Response> getInstanstKYCResponse() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);

    var headers = {'Authorization': 'bearer ${authKey}'};
    var request = http.Request('POST', Uri.parse('${BASE_URL}/api/users/instant-kyc-approval'));
    request.body = '''''';
    request.headers.addAll(headers);

    var streamedResponse = await request.send();
    return await http.Response.fromStream(streamedResponse);
  }

  Future<http.Response> getOtpVerificationResponse(String email, String otp_action) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);

    Map<String, String> mheaders = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $authKey'
    };
    var postBody = jsonEncode({"email": "$email", "otp_action": "$otp_action"});
    final response =
        await http.post("${BASE_URL}/api/auth/generate-otp", headers: mheaders, body: postBody);
    return response;
  }

  Future<http.Response> getTokenResponse(int token_id) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);

    var headers = {'Authorization': 'bearer ${authKey}', 'Content-Type': 'application/json'};
    var request = http.Request('POST', Uri.parse('${BASE_URL}/api/graphs/token'));
    request.body = json.encode({"token_id": token_id});
    request.headers.addAll(headers);

    var streamedResponse = await request.send();
    return await http.Response.fromStream(streamedResponse);
  }

  Future<http.Response> getBucketGraphResponse(int bucket_id) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String authKey = preferences.getString(APITOKEN);

    var headers = {'Authorization': 'bearer ${authKey}', 'Content-Type': 'application/json'};
    var request = http.Request('POST', Uri.parse('${BASE_URL}/api/graphs/bucket'));
    request.body = json.encode({"bucket_id": bucket_id});
    request.headers.addAll(headers);

    var streamedResponse = await request.send();
    return await http.Response.fromStream(streamedResponse);
  }
}
