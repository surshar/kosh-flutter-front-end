
import 'dart:convert';

PpverifyModel ppverifyModelFromJson(String str) => PpverifyModel.fromJson(json.decode(str));

String ppverifyModelToJson(PpverifyModel data) => json.encode(data.toJson());

class PpverifyModel {
  PpverifyModel({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory PpverifyModel.fromJson(Map<String, dynamic> json) => PpverifyModel(
    success: json["success"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.type,
    this.details,
  });

  String type;
  Details details;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    type: json["type"],
    details: Details.fromJson(json["details"]),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "details": details.toJson(),
  };
}

class Details {
  Details({
    this.countryCode,
    this.dob,
    this.doe,
    this.doi,
    this.gender,
    this.givenName,
    this.surname,
    this.nationality,
    this.passportNum,
    this.placeOfBirth,
    this.placeOfIssue,
    this.type,
    this.tag,
    this.mrz,
  });

  CountryCode countryCode;
  CountryCode dob;
  CountryCode doe;
  CountryCode doi;
  CountryCode gender;
  CountryCode givenName;
  CountryCode surname;
  CountryCode nationality;
  CountryCode passportNum;
  CountryCode placeOfBirth;
  CountryCode placeOfIssue;
  CountryCode type;
  String tag;
  Mrz mrz;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
    countryCode: CountryCode.fromJson(json["country_code"]),
    dob: CountryCode.fromJson(json["dob"]),
    doe: CountryCode.fromJson(json["doe"]),
    doi: CountryCode.fromJson(json["doi"]),
    gender: CountryCode.fromJson(json["gender"]),
    givenName: CountryCode.fromJson(json["given_name"]),
    surname: CountryCode.fromJson(json["surname"]),
    nationality: CountryCode.fromJson(json["nationality"]),
    passportNum: CountryCode.fromJson(json["passport_num"]),
    placeOfBirth: CountryCode.fromJson(json["place_of_birth"]),
    placeOfIssue: CountryCode.fromJson(json["place_of_issue"]),
    type: CountryCode.fromJson(json["type"]),
    tag: json["tag"],
    mrz: Mrz.fromJson(json["mrz"]),
  );

  Map<String, dynamic> toJson() => {
    "country_code": countryCode.toJson(),
    "dob": dob.toJson(),
    "doe": doe.toJson(),
    "doi": doi.toJson(),
    "gender": gender.toJson(),
    "given_name": givenName.toJson(),
    "surname": surname.toJson(),
    "nationality": nationality.toJson(),
    "passport_num": passportNum.toJson(),
    "place_of_birth": placeOfBirth.toJson(),
    "place_of_issue": placeOfIssue.toJson(),
    "type": type.toJson(),
    "tag": tag,
    "mrz": mrz.toJson(),
  };
}

class CountryCode {
  CountryCode({
    this.conf,
    this.value,
  });

  int conf;
  String value;

  factory CountryCode.fromJson(Map<String, dynamic> json) => CountryCode(
    conf: json["conf"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
  };
}

class Mrz {
  Mrz({
    this.line1,
    this.line2,
    this.conf,
  });

  String line1;
  String line2;
  int conf;

  factory Mrz.fromJson(Map<String, dynamic> json) => Mrz(
    line1: json["line1"],
    line2: json["line2"],
    conf: json["conf"],
  );

  Map<String, dynamic> toJson() => {
    "line1": line1,
    "line2": line2,
    "conf": conf,
  };
}
