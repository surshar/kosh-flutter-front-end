import 'dart:io';
import 'dart:ui';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:cryptocase/kyc_screen/aadharverify_model.dart';
import 'package:cryptocase/kyc_screen/aadharverifyback_model.dart';
import 'package:cryptocase/kyc_screen/kyc_detaila_model.dart';
import 'package:cryptocase/kyc_screen/kyc_details_controller.dart';
import 'package:cryptocase/kyc_screen/panverify_model.dart';
import 'package:cryptocase/kyc_screen/ppverify_model.dart';
import 'package:cryptocase/kyc_screen/ppverifyback_model.dart';
import 'package:cryptocase/kyc_screen/upload_selfie_ui.dart';
import 'package:cryptocase/kyc_screen/verify_document_controller.dart';
import 'package:cryptocase/kyc_screen/voterid_verify_model.dart';
import 'package:cryptocase/kyc_screen/voteridback_verify_model.dart';
import 'package:cryptocase/login_screen/login_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../api_client.dart';

class KYCDetailsUI extends StatefulWidget {
  @override
  _KYCDetailsUIState createState() => _KYCDetailsUIState();
}

class _KYCDetailsUIState extends State<KYCDetailsUI> {
  AdaptiveThemeMode adaptiveThemeMode;
  final kyccontroller = Get.put(KycDetailController());
  final verifydocumentcontroller = Get.put(VerifyDocumentController());
  var btnVisible = false;
  String Options;
  var token;
  KycModel _kycModel;
  File _pancardImage;
  File _frontImage;
  File _backImage;

  bool isPanCardUploaded = false;
  bool isFrontUploaded = false;
  bool isbackUploaded = false;
  bool isLoggedin = false;

  @override
  void initState() {
    super.initState();
    getTheme();
    checkLogin();
  }

  getDropValue(String DocType) {
    var docMap = {'Passport': 'passport', 'Aadhaar Card': 'aadhar', 'Voter ID': 'voterID'};

    return docMap[DocType] ?? 'passport';
  }

  Future<bool> sendDocument(File imageFile, String vaultLink) async {
    http.Response response = await ApiClient().getkycDocumentResponse(imageFile, vaultLink);

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  getdocLink() async {
    _kycModel = await kyccontroller.fetchKycDetailsResponse(getDropValue(Options));
    UPPANCARD = _kycModel.data.panCard;
    UPLOADSELFIE = _kycModel.data.selfie;
    UPLOADBACK = _kycModel.data.documentBack;
    UPLOADFRONT = _kycModel.data.documentFront;
  }

  verifyDocument(String doc_type, String front) async {
    Get.dialog(CupertinoActivityIndicator());
    PanverifyModel _model = await verifydocumentcontroller.fetchPanVerifyResponse(doc_type, front);

    if (!_model.success) {
      Get.back();
      _pancardImage = null;
      redSnack("document not Verified. Please Upload Again..");
      isPanCardUploaded = false;
    } else {
      Get.back();
      isPanCardUploaded = _model.success;
    }

    setState(() {});
  }

  aadharVerifyDocument(String doc_type, String front) async {
    Get.dialog(CupertinoActivityIndicator());

    AadharverifyModel _model =
        await verifydocumentcontroller.fetchAadharVerifyResponse(doc_type, front);

    if (!_model.success) {
      Get.back();
      _frontImage = null;
      redSnack("document not Verified. Please Upload Again..");
      isFrontUploaded = false;
    } else {
      Get.back();
      isFrontUploaded = _model.success;
    }

    setState(() {});
  }

  aadharBackVerifydocument(String doc_type, String back) async {
    Get.dialog(CupertinoActivityIndicator());
    AadharverifybackModel _model =
        await verifydocumentcontroller.fetchAadharVerifyBackResponse(doc_type, back);

    if (!_model.success) {
      Get.back();
      _backImage = null;
      redSnack("document not Verified. Please Upload Again..");
      isbackUploaded = false;
    } else {
      Get.back();
      isbackUploaded = _model.success;
    }

    setState(() {});
  }

  passportVerifydocument(String doc_type, String front) async {
    Get.dialog(CupertinoActivityIndicator());
    PpverifyModel _model =
        await verifydocumentcontroller.fetchPassportverifyResponse(doc_type, front);
    if (!_model.success) {
      Get.back();
      _frontImage = null;
      redSnack("document not Verified. Please Upload Again..");
      isFrontUploaded = false;
    } else {
      Get.back();
      isFrontUploaded = _model.success;
    }

    setState(() {});
  }

  passportBackVerifydocument(String doc_type, String back) async {
    Get.dialog(CupertinoActivityIndicator());
    PpverifyBackModel _model =
        await verifydocumentcontroller.fetchPassportVerifyBackResponse(doc_type, back);
    if (!_model.success) {
      Get.back();
      _backImage = null;
      redSnack("document not Verified. Please Upload Again..");
      isbackUploaded = false;
    } else {
      Get.back();
      isbackUploaded = _model.success;
    }

    setState(() {});
  }

  voterIDVerifyDocument(String doc_type, String front) async {
    Get.dialog(CupertinoActivityIndicator());
    VoteridVerifyModel _model =
        await verifydocumentcontroller.fetchVoterIDVerifyResponse(doc_type, front);
    if (!_model.success) {
      Get.back();
      _frontImage = null;
      redSnack("document not Verified. Please Upload Again..");
      isFrontUploaded = false;
    } else {
      Get.back();
      isFrontUploaded = _model.success;
    }

    setState(() {});
  }

  voterIDBackVerifyDocument(String doc_type, String back) async {
    Get.dialog(CupertinoActivityIndicator());
    VoteridbackVerifyModel _model =
        await verifydocumentcontroller.fetchVoterIDBackVerifyResponse(doc_type, back);
    if (!_model.success) {
      Get.back();
      _backImage = null;
      redSnack("document not Verified. Please Upload Again..");
      isbackUploaded = false;
    } else {
      Get.back();
      isbackUploaded = _model.success;
    }

    setState(() {});
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  void checkLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getBool(ACCESSTOKEN) ?? false;
    isLoggedin = preferences.getBool(LOGGED_IN) ?? false;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        resizeToAvoidBottomInset: false,
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              appBarsignup(
                  height * 0.1,
                  width,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  "Enter KYC details"),
              Container(
                  margin: EdgeInsets.only(left: 0, top: 7),
                  height: height * 0.70,
                  child: buildMainScreen(context, width, height)),
              GestureDetector(
                onTap: () {
                  isLoggedin ? Get.offAll(HomeScreen()) : Get.offAll(LoginUi());
                },
                child: Container(
                  height: height * 0.06,
                  child: Center(
                    child: textWidget(
                        "Skip",
                        ScreenUtil().setSp(40, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                        TextAlign.start),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 0, right: 0),
                height: height * 0.07,
                child: GestureDetector(
                  onTap: () async {
                    if (Options == null) {
                      redSnack("Select Document Type");
                    } else if (PANCARDNO == null) {
                      redSnack("Pancard number missing");
                    } else if (_pancardImage == null) {
                      redSnack("Upload Pancard");
                    } else if (_frontImage == null) {
                      redSnack("Upload front side of selected Document");
                    } else if (_backImage == null) {
                      redSnack("Upload back side of selected Document");
                    } else {
                      Get.to(() => UploadSelfieUi());
                    }
                  },
                  child: loginBottomButton(
                      adaptiveThemeMode == AdaptiveThemeMode.light ? buttongreen : buttongreen,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                      "Next"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  buildMainScreen(BuildContext context, double width, double height) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          textWidget(
              VERIFYIDENTY,
              ScreenUtil().setSp(50, allowFontScalingSelf: true),
              FontWeight.w600,
              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              TextAlign.start),
          Padding(
            padding: const EdgeInsets.only(top: 30.0),
            child: textWidget(
                PANCARDNO,
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
          Container(
            height: height * 0.08,
            margin: EdgeInsets.only(top: 10),
            child: TextField(
              style: TextStyle(
                fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                fontFamily: 'WorkSans',
                fontWeight: FontWeight.w400,
              ),
              decoration: new InputDecoration(
                fillColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                filled: true,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                    )),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                    )),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                    )),
                hintStyle: TextStyle(
                    fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w400,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white),
                contentPadding: EdgeInsets.all(20.0),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 13.0),
            child: textWidget(
                UPLOADPANCARD,
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
          GestureDetector(
            onTap: () {
              if (Options != null) {
                isPanCardUploaded
                    ? _pancardImage == null
                        ? takePanCardPhotoDialog(height, width, context)
                        : viewPhoto("PAN CARD", _pancardImage)
                    : takePanCardPhotoDialog(height, width, context);
              } else {
                redSnack("Select Document type");
              }
            },
            child: Container(
                margin: EdgeInsets.only(top: 10),
                width: width * 0.9,
                height: height * 0.08,
                decoration: BoxDecoration(
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    border: Border.all(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        isPanCardUploaded ? "View Photo" : UPLOADPANCARD,
                        textAlign: TextAlign.center,
                        style: (TextStyle(
                          fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w400,
                        )),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: isPanCardUploaded
                          ? Icon(
                              Icons.download_done_rounded,
                              color: buttongreen,
                            )
                          : SvgPicture.asset(
                              'assets/upload_icon.svg',
                            ),
                    )
                  ],
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 13.0),
            child: textWidget(
                UPLOADDOCUMNET,
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            width: width * 0.9,
            height: height * 0.08,
            decoration: BoxDecoration(
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
              borderRadius: BorderRadius.all(Radius.circular(12)),
              border: Border.all(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                  width: 1),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: DropdownButton<String>(
                isExpanded: true,
                items: SELECTDOCUMENT.map((value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                value: Options,
                underline: SizedBox.shrink(),
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : textgrey,
                  fontFamily: 'WorkSans',
                  fontWeight: FontWeight.w400,
                ),
                hint: Text(
                  DOCUMENTTYPE,
                  style: (TextStyle(
                    fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w400,
                  )),
                ),
                onChanged: (value) {
                  setState(() {
                    Options = value.toString();
                    getdocLink();
                  });
                },
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () {
                  if (Options != null) {
                    takeFrontPhotoDialog(height, width, context, Options);
                  } else {
                    redSnack("Select Document type");
                  }
                },
                child: Container(
                    margin: EdgeInsets.only(top: 15),
                    width: width * 0.4,
                    decoration: BoxDecoration(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      border: Border.all(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? bordercolor : white,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 17, bottom: 13),
                          child: isFrontUploaded
                              ? GestureDetector(
                                  onTap: () {
                                    viewPhoto("front side of $Options", _frontImage);
                                  },
                                  child: Icon(
                                    Icons.download_done_rounded,
                                    color: buttongreen,
                                    size: 50,
                                  ),
                                )
                              : SvgPicture.asset(
                                  'assets/upload_icon.svg',
                                  height: height * 0.05,
                                ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(left: 17, right: 17, bottom: 13),
                            child: Text(
                              isFrontUploaded
                                  ? "View Image"
                                  : Options != null
                                      ? 'Upload front side of $Options'
                                      : 'Select Document type',
                              textAlign: TextAlign.center,
                              style: (TextStyle(
                                fontSize: 12,
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.w400,
                              )),
                            )),
                      ],
                    )),
              ),
              GestureDetector(
                onTap: () {
                  if (Options != null) {
                    takeBackPhotoDialog(height, width, context, Options);
                  } else {
                    redSnack("Select Document type");
                  }
                },
                child: Container(
                    margin: EdgeInsets.only(top: 15),
                    width: width * 0.4,
                    decoration: BoxDecoration(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      border: Border.all(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? bordercolor : white,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 17, bottom: 13),
                          child: isbackUploaded
                              ? GestureDetector(
                                  onTap: () {
                                    viewPhoto("back side of $Options", _backImage);
                                  },
                                  child: Icon(
                                    Icons.download_done_rounded,
                                    color: buttongreen,
                                    size: 50,
                                  ),
                                )
                              : SvgPicture.asset(
                                  'assets/upload_icon.svg',
                                  height: height * 0.05,
                                ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(left: 17, right: 17, bottom: 13),
                            child: Text(
                              isbackUploaded
                                  ? "View image"
                                  : Options != null
                                      ? 'Upload back side of $Options'
                                      : 'Select Document type',
                              textAlign: TextAlign.center,
                              style: (TextStyle(
                                fontSize: 12,
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.w400,
                              )),
                            )),
                      ],
                    )),
              ),
            ],
          ),
        ],
      ),
    );
  }

  takePanCardPhotoDialog(double height, double width, BuildContext dialogContext) {
    showDialog(
        context: this.context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (context, msetState) {
              return Dialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            textWidget(
                                "Upload Your PAN CARD",
                                ScreenUtil().setSp(45, allowFontScalingSelf: true),
                                FontWeight.w700,
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                                TextAlign.start),
                            GestureDetector(
                              onTap: () async {
                                Get.back();
                              },
                              child: Icon(
                                Icons.close,
                                color: adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textgrey
                                    : textgrey,
                                size: 20,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 30),
                          child: Center(
                              child: _pancardImage != null
                                  ? Image.file(
                                      _pancardImage,
                                      height: width * 0.6,
                                      width: width * 0.6,
                                      fit: BoxFit.fill,
                                    )
                                  : GestureDetector(
                                      onTap: () {
                                        if (Options != null) {
                                          _takepancardImage(msetState);
                                        } else {
                                          redSnack("Select Document type");
                                        }
                                      },
                                      child: Icon(
                                        Icons.camera_alt_outlined,
                                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                                            ? textgrey
                                            : textgrey,
                                        size: 150,
                                      ),
                                    )),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  _pancardImage = null;
                                  msetState(() {});
                                },
                                child: Container(
                                    margin: EdgeInsets.only(right: 5),
                                    height: height * 0.05,
                                    decoration: BoxDecoration(
                                        color: textred,
                                        borderRadius: BorderRadius.all(Radius.circular(8))),
                                    child: Center(
                                      child: textWidget(
                                          "Remove",
                                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                          FontWeight.w600,
                                          white,
                                          TextAlign.start),
                                    )),
                              ),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () async {
                                  Navigator.pop(dialogContext);

                                  if (_pancardImage != null) {
                                    isPanCardUploaded = true;
                                    await sendDocument(_pancardImage, UPPANCARD);
                                    verifyDocument("pan", "front");
                                  } else {
                                    redSnack("Invalid photo");
                                  }

                                  setState(() {});
                                },
                                child: Container(
                                    margin: EdgeInsets.only(left: 5),
                                    height: height * 0.05,
                                    decoration: BoxDecoration(
                                        color: _pancardImage == null ? Colors.grey : textgreen,
                                        borderRadius: BorderRadius.all(Radius.circular(8))),
                                    child: Center(
                                      child: textWidget(
                                          "Upload",
                                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                          FontWeight.w600,
                                          white,
                                          TextAlign.start),
                                    )),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  _takepancardImage(setState) async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50, preferredCameraDevice: CameraDevice.rear);
    _pancardImage = File(image.path);
    setState(() {});
  }

  takeFrontPhotoDialog(double height, double width, BuildContext dialogContext, var option) {
    showDialog(
        context: this.context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (context, msetState) {
              return Dialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                //this right here
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: textWidget(
                                  "Upload Front Side of $option",
                                  ScreenUtil().setSp(45, allowFontScalingSelf: true),
                                  FontWeight.w700,
                                  adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                                  TextAlign.start),
                            ),
                            GestureDetector(
                              onTap: () async {
                                Get.back();
                              },
                              child: Icon(
                                Icons.close,
                                color: adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textgrey
                                    : textgrey,
                                size: 20,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 30),
                          child: Center(
                              child: _frontImage != null
                                  ? Image.file(
                                      _frontImage,
                                      height: width * 0.6,
                                      width: width * 0.6,
                                      fit: BoxFit.fill,
                                    )
                                  : GestureDetector(
                                      onTap: () {
                                        _takefrontImage(msetState);
                                      },
                                      child: Icon(
                                        Icons.camera_alt_outlined,
                                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                                            ? textgrey
                                            : textgrey,
                                        size: 150,
                                      ),
                                    )),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  _frontImage = null;
                                  msetState(() {});
                                },
                                child: Container(
                                    margin: EdgeInsets.only(right: 5),
                                    height: height * 0.05,
                                    decoration: BoxDecoration(
                                        color: textred,
                                        borderRadius: BorderRadius.all(Radius.circular(8))),
                                    child: Center(
                                      child: textWidget(
                                          "Remove",
                                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                          FontWeight.w600,
                                          white,
                                          TextAlign.start),
                                    )),
                              ),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () async {
                                  Navigator.pop(dialogContext);

                                  if (_frontImage != null) {
                                    // isFrontUploaded = true;

                                    await sendDocument(_frontImage, UPLOADFRONT);

                                    if (getDropValue(Options) == "passport") {
                                      passportVerifydocument(getDropValue(Options), "front");
                                    } else if (getDropValue(Options) == "aadhar") {
                                      aadharVerifyDocument(getDropValue(Options), "front");
                                    } else {
                                      voterIDVerifyDocument(getDropValue(Options), "front");
                                    }
                                  } else {
                                    redSnack("Invalid photo");
                                  }

                                  setState(() {});
                                },
                                child: Container(
                                    margin: EdgeInsets.only(left: 5),
                                    height: height * 0.05,
                                    decoration: BoxDecoration(
                                        color: _frontImage == null ? Colors.grey : textgreen,
                                        borderRadius: BorderRadius.all(Radius.circular(8))),
                                    child: Center(
                                      child: textWidget(
                                          "Upload",
                                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                          FontWeight.w600,
                                          white,
                                          TextAlign.start),
                                    )),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  _takefrontImage(setState) async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50, preferredCameraDevice: CameraDevice.rear);
    _frontImage = File(image.path);
    setState(() {});
  }

  takeBackPhotoDialog(double height, double width, BuildContext dialogContext, var option) {
    showDialog(
        context: this.context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (context, msetState) {
              return Dialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                //this right here
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: textWidget(
                                  "Upload Back Side of $option",
                                  ScreenUtil().setSp(45, allowFontScalingSelf: true),
                                  FontWeight.w700,
                                  adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                                  TextAlign.start),
                            ),
                            GestureDetector(
                              onTap: () async {
                                Get.back();
                              },
                              child: Icon(
                                Icons.close,
                                color: adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textgrey
                                    : textgrey,
                                size: 20,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 30),
                          child: Center(
                              child: _backImage != null
                                  ? Image.file(
                                      _backImage,
                                      height: width * 0.6,
                                      width: width * 0.6,
                                      fit: BoxFit.fill,
                                    )
                                  : GestureDetector(
                                      onTap: () {
                                        _takebackImage(msetState);
                                      },
                                      child: Icon(
                                        Icons.camera_alt_outlined,
                                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                                            ? textgrey
                                            : textgrey,
                                        size: 150,
                                      ),
                                    )),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  _backImage = null;
                                  msetState(() {});
                                },
                                child: Container(
                                    margin: EdgeInsets.only(right: 5),
                                    height: height * 0.05,
                                    decoration: BoxDecoration(
                                        color: textred,
                                        borderRadius: BorderRadius.all(Radius.circular(8))),
                                    child: Center(
                                      child: textWidget(
                                          "Remove",
                                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                          FontWeight.w600,
                                          white,
                                          TextAlign.start),
                                    )),
                              ),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () async {
                                  Navigator.pop(dialogContext);

                                  if (_backImage != null) {
                                    await sendDocument(_backImage, UPLOADBACK);

                                    if (getDropValue(Options) == "passport") {
                                      passportBackVerifydocument(getDropValue(Options), "back");
                                    } else if (getDropValue(Options) == "aadhar") {
                                      aadharBackVerifydocument(getDropValue(Options), "back");
                                    } else {
                                      voterIDBackVerifyDocument(getDropValue(Options), "back");
                                    }
                                  } else {
                                    redSnack("Invalid photo");
                                  }

                                  setState(() {});
                                },
                                child: Container(
                                    margin: EdgeInsets.only(left: 5),
                                    height: height * 0.05,
                                    decoration: BoxDecoration(
                                        color: _backImage == null ? Colors.grey : textgreen,
                                        borderRadius: BorderRadius.all(Radius.circular(8))),
                                    child: Center(
                                      child: textWidget(
                                          "Upload",
                                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                          FontWeight.w600,
                                          white,
                                          TextAlign.start),
                                    )),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  _takebackImage(setState) async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50, preferredCameraDevice: CameraDevice.rear);
    _backImage = File(image.path);
    setState(() {});
  }

  viewPhoto(
    String title,
    File image,
  ) {
    showDialog(
        context: this.context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (context, msetState) {
              return Dialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                //this right here
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: textWidget(
                                  "$title",
                                  ScreenUtil().setSp(45, allowFontScalingSelf: true),
                                  FontWeight.w700,
                                  adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                                  TextAlign.start),
                            ),
                            GestureDetector(
                              onTap: () {
                                Get.back();
                              },
                              child: Icon(
                                Icons.close,
                                color: adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textgrey
                                    : textgrey,
                                size: 20,
                              ),
                            ),
                          ],
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.all(20),
                            child: Image.file(
                              image,
                              height: 200,
                              width: 200,
                              fit: BoxFit.fill,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        });
  }

  upload(File imageFile, String doc) async {
    var uri = Uri.parse(doc);

    var response = await http.put(
      uri,
      body: imageFile.readAsBytesSync(),
    );
  }
}
