
import 'dart:convert';

PanverifyModel panverifyModelFromJson(String str) => PanverifyModel.fromJson(json.decode(str));

String panverifyModelToJson(PanverifyModel data) => json.encode(data.toJson());

class PanverifyModel {
  PanverifyModel({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory PanverifyModel.fromJson(Map<String, dynamic> json) => PanverifyModel(
    success: json["success"],
    data: json["success"]? List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))): null,
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.type,
    this.details,
  });

  String type;
  Details details;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    type: json["type"],
    details: Details.fromJson(json["details"]),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "details": details.toJson(),
  };
}

class Details {
  Details({
    this.name,
    this.father,
    this.date,
    this.panNo,
    this.dateOfIssue,
    this.tag,
  });

  Date name;
  Date father;
  Date date;
  Date panNo;
  Date dateOfIssue;
  String tag;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
    name: Date.fromJson(json["name"]),
    father: Date.fromJson(json["father"]),
    date: Date.fromJson(json["date"]),
    panNo: Date.fromJson(json["pan_no"]),
    dateOfIssue: Date.fromJson(json["date_of_issue"]),
    tag: json["tag"],
  );

  Map<String, dynamic> toJson() => {
    "name": name.toJson(),
    "father": father.toJson(),
    "date": date.toJson(),
    "pan_no": panNo.toJson(),
    "date_of_issue": dateOfIssue.toJson(),
    "tag": tag,
  };
}

class Date {
  Date({
    this.conf,
    this.value,
  });

  int conf;
  String value;

  factory Date.fromJson(Map<String, dynamic> json) => Date(
    conf: json["conf"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
  };
}
