
import 'dart:convert';

VoteridVerifyModel voteridVerifyModelFromJson(String str) => VoteridVerifyModel.fromJson(json.decode(str));

String voteridVerifyModelToJson(VoteridVerifyModel data) => json.encode(data.toJson());

class VoteridVerifyModel {
  VoteridVerifyModel({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory VoteridVerifyModel.fromJson(Map<String, dynamic> json) => VoteridVerifyModel(
    success: json["success"],
    data: json["success"] ? List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))): null,
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.type,
    this.details,
  });

  String type;
  Details details;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    type: json["type"],
    details: Details.fromJson(json["details"]),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "details": details.toJson(),
  };
}

class Details {
  Details({
    this.voterid,
    this.name,
    this.gender,
    this.relation,
    this.dob,
    this.doc,
    this.age,
    this.tag,
  });

  Age voterid;
  Age name;
  Age gender;
  Age relation;
  Age dob;
  Age doc;
  Age age;
  String tag;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
    voterid: Age.fromJson(json["voterid"]),
    name: Age.fromJson(json["name"]),
    gender: Age.fromJson(json["gender"]),
    relation: Age.fromJson(json["relation"]),
    dob: Age.fromJson(json["dob"]),
    doc: Age.fromJson(json["doc"]),
    age: Age.fromJson(json["age"]),
    tag: json["tag"],
  );

  Map<String, dynamic> toJson() => {
    "voterid": voterid.toJson(),
    "name": name.toJson(),
    "gender": gender.toJson(),
    "relation": relation.toJson(),
    "dob": dob.toJson(),
    "doc": doc.toJson(),
    "age": age.toJson(),
    "tag": tag,
  };
}

class Age {
  Age({
    this.conf,
    this.value,
  });

  int conf;
  String value;

  factory Age.fromJson(Map<String, dynamic> json) => Age(
    conf: json["conf"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
  };
}
