import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/kyc_screen/aadharverify_model.dart';
import 'package:cryptocase/kyc_screen/aadharverifyback_model.dart';
import 'package:cryptocase/kyc_screen/panverify_model.dart';
import 'package:cryptocase/kyc_screen/ppverify_model.dart';
import 'package:cryptocase/kyc_screen/ppverifyback_model.dart';
import 'package:cryptocase/kyc_screen/voterid_verify_model.dart';
import 'package:cryptocase/kyc_screen/voteridback_verify_model.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../api_client.dart';

class VerifyDocumentController extends GetxController {
  var verifyDocumentResponse = PanverifyModel().obs;

  //pan controller
  Future<PanverifyModel> fetchPanVerifyResponse(
      String doc_type, String front_or_back) async {
    var responseResult = PanverifyModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
          await ApiClient().getKycVerifyResponse(doc_type, front_or_back);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = PanverifyModel.fromJson(jsonMap);
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
    return responseResult;
  }

  //aadhar front controller
  Future<AadharverifyModel> fetchAadharVerifyResponse(
      String doc_type, String front_or_back) async {
    var responseResult = AadharverifyModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
          await ApiClient().getKycVerifyResponse(doc_type, front_or_back);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = AadharverifyModel.fromJson(jsonMap);
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
    return responseResult;
  }

  //aadhar back controller
  Future<AadharverifybackModel> fetchAadharVerifyBackResponse(
      String doc_type, String front_or_back) async {
    var responseResult = AadharverifybackModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
          await ApiClient().getKycVerifyResponse(doc_type, front_or_back);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = AadharverifybackModel.fromJson(jsonMap);
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
    return responseResult;
  }


  //passport controller
  Future<PpverifyModel> fetchPassportverifyResponse(
      String doc_type, String front_or_back) async {
    var responseResult = PpverifyModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
      await ApiClient().getKycVerifyResponse(doc_type, front_or_back);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = PpverifyModel.fromJson(jsonMap);

    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
    return responseResult;
  }

  //passport back controller
  Future<PpverifyBackModel> fetchPassportVerifyBackResponse(
      String doc_type, String front_or_back) async {
    var responseResult = PpverifyBackModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
      await ApiClient().getKycVerifyResponse(doc_type, front_or_back);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = PpverifyBackModel.fromJson(jsonMap);
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
    return responseResult;
  }

  //voterID controller
  Future<VoteridVerifyModel> fetchVoterIDVerifyResponse(
      String doc_type, String front_or_back) async {
    var responseResult = VoteridVerifyModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
      await ApiClient().getKycVerifyResponse(doc_type, front_or_back);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = VoteridVerifyModel.fromJson(jsonMap);
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
    return responseResult;
  }

  //voterID back controller
  Future<VoteridbackVerifyModel> fetchVoterIDBackVerifyResponse(
      String doc_type, String front_or_back) async {
    var responseResult = VoteridbackVerifyModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
      await ApiClient().getKycVerifyResponse(doc_type, front_or_back);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = VoteridbackVerifyModel.fromJson(jsonMap);
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
    return responseResult;
  }

}
