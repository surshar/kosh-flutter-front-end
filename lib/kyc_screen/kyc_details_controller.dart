import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/kyc_screen/kyc_detaila_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../api_client.dart';

class KycDetailController extends GetxController {
  var kycControllerResponse = KycModel().obs;

  Future<KycModel> fetchKycDetailsResponse(String verification_type) async {
    Get.dialog(CupertinoActivityIndicator());

    var responseResult = KycModel();
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response = await ApiClient().getKycDetailsResponse(verification_type);
      var jsonString = response.body;
      if (jsonString == "Forbidden") {
        Get.back();
        redSnack("Forbidden");
      } else {
        var jsonMap = json.decode(jsonString);

        debugPrint('myDebug KycDetailController.fetchKycDetailsResponse  $jsonMap');
        responseResult = KycModel.fromJson(jsonMap);

        if (response.statusCode == 200 || response.statusCode == 201) {
          Get.back();
          print(responseResult.success);

          return responseResult;
        } else {
          Get.back();
          print(responseResult.success);
          redSnack("Kyc Upload Failed");
          return responseResult;
        }
      }
    } else {
      redSnack("Please check your internet connection");
      return responseResult;
    }
  }

  Future<bool> fetchVerifyDocumentResponse(File imageFile, String docLink) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response = await ApiClient().getkycDocumentResponse(imageFile, docLink);

      if (response.statusCode != 200) {
        Get.back();
        print("doc upload controller : ${response.body}");
        print("doc upload controller : ${response.statusCode}");
        greenSnack("Upload Successfully");
        return true;
      } else {
        Get.back();
        redSnack("Upload Failed");
        return false;
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
      return false;
    }
  }
}
