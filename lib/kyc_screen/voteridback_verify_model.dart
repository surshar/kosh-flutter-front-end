
import 'dart:convert';

VoteridbackVerifyModel voteridbackVerifyModelFromJson(String str) => VoteridbackVerifyModel.fromJson(json.decode(str));

String voteridbackVerifyModelToJson(VoteridbackVerifyModel data) => json.encode(data.toJson());

class VoteridbackVerifyModel {
  VoteridbackVerifyModel({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory VoteridbackVerifyModel.fromJson(Map<String, dynamic> json) => VoteridbackVerifyModel(
    success: json["success"],
    data: json["success"]? List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))): null,
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.type,
    this.details,
  });

  String type;
  Details details;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    type: json["type"],
    details: Details.fromJson(json["details"]),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "details": details.toJson(),
  };
}

class Details {
  Details({
    this.voterid,
    this.pin,
    this.address,
    this.gender,
    this.date,
    this.dob,
    this.age,
    this.tag,
    this.type,
  });

  Age voterid;
  Age pin;
  Address address;
  Age gender;
  Age date;
  Age dob;
  Age age;
  String tag;
  Type type;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
    voterid: Age.fromJson(json["voterid"]),
    pin: Age.fromJson(json["pin"]),
    address: Address.fromJson(json["address"]),
    gender: Age.fromJson(json["gender"]),
    date: Age.fromJson(json["date"]),
    dob: Age.fromJson(json["dob"]),
    age: Age.fromJson(json["age"]),
    tag: json["tag"],
    type: Type.fromJson(json["type"]),
  );

  Map<String, dynamic> toJson() => {
    "voterid": voterid.toJson(),
    "pin": pin.toJson(),
    "address": address.toJson(),
    "gender": gender.toJson(),
    "date": date.toJson(),
    "dob": dob.toJson(),
    "age": age.toJson(),
    "tag": tag,
    "type": type.toJson(),
  };
}

class Address {
  Address({
    this.conf,
    this.value,
    this.line1,
    this.line2,
    this.city,
    this.pin,
    this.state,
    this.locality,
    this.houseNumber,
    this.street,
    this.district,
    this.landmark,
  });

  int conf;
  String value;
  String line1;
  String line2;
  String city;
  String pin;
  String state;
  String locality;
  String houseNumber;
  String street;
  String district;
  String landmark;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    conf: json["conf"],
    value: json["value"],
    line1: json["line1"],
    line2: json["line2"],
    city: json["city"],
    pin: json["pin"],
    state: json["state"],
    locality: json["locality"],
    houseNumber: json["house_number"],
    street: json["street"],
    district: json["district"],
    landmark: json["landmark"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
    "line1": line1,
    "line2": line2,
    "city": city,
    "pin": pin,
    "state": state,
    "locality": locality,
    "house_number": houseNumber,
    "street": street,
    "district": district,
    "landmark": landmark,
  };
}

class Age {
  Age({
    this.conf,
    this.value,
  });

  int conf;
  String value;

  factory Age.fromJson(Map<String, dynamic> json) => Age(
    conf: json["conf"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
  };
}

class Type {
  Type({
    this.value,
  });

  String value;

  factory Type.fromJson(Map<String, dynamic> json) => Type(
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "value": value,
  };
}
