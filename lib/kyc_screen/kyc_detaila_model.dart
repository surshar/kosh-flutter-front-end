
import 'dart:convert';

KycModel kycModelFromJson(String str) => KycModel.fromJson(json.decode(str));

String kycModelToJson(KycModel data) => json.encode(data.toJson());

class KycModel {
  KycModel({
    this.success,
    this.data,
  });

  bool success;
  Data data;

  factory KycModel.fromJson(Map<String, dynamic> json) => KycModel(
    success: json["success"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.documentFront,
    this.documentBack,
    this.selfie,
    this.panCard,
  });

  String documentFront;
  String documentBack;
  String selfie;
  String panCard;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    documentFront: json["documentFront"],
    documentBack: json["documentBack"],
    selfie: json["selfie"],
    panCard: json["panCard"],
  );

  Map<String, dynamic> toJson() => {
    "documentFront": documentFront,
    "documentBack": documentBack,
    "selfie": selfie,
    "panCard": panCard,
  };
}
