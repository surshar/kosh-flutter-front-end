
import 'dart:convert';

PpverifyBackModel ppverifyBackModelFromJson(String str) => PpverifyBackModel.fromJson(json.decode(str));

String ppverifyBackModelToJson(PpverifyBackModel data) => json.encode(data.toJson());

class PpverifyBackModel {
  PpverifyBackModel({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory PpverifyBackModel.fromJson(Map<String, dynamic> json) => PpverifyBackModel(
    success: json["success"],
    data: json["success"]? List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))): null,
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.type,
    this.details,
  });

  String type;
  Details details;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    type: json["type"],
    details: Details.fromJson(json["details"]),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "details": details.toJson(),
  };
}

class Details {
  Details({
    this.passportNum,
    this.address,
    this.fileNum,
    this.father,
    this.mother,
    this.oldDoi,
    this.oldPassportNum,
    this.oldPlaceOfIssue,
    this.pin,
    this.spouse,
    this.tag,
  });

  Father passportNum;
  Address address;
  Father fileNum;
  Father father;
  Father mother;
  Father oldDoi;
  Father oldPassportNum;
  Father oldPlaceOfIssue;
  Father pin;
  Father spouse;
  String tag;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
    passportNum: Father.fromJson(json["passport_num"]),
    address: Address.fromJson(json["address"]),
    fileNum: Father.fromJson(json["file_num"]),
    father: Father.fromJson(json["father"]),
    mother: Father.fromJson(json["mother"]),
    oldDoi: Father.fromJson(json["old_doi"]),
    oldPassportNum: Father.fromJson(json["old_passport_num"]),
    oldPlaceOfIssue: Father.fromJson(json["old_place_of_issue"]),
    pin: Father.fromJson(json["pin"]),
    spouse: Father.fromJson(json["spouse"]),
    tag: json["tag"],
  );

  Map<String, dynamic> toJson() => {
    "passport_num": passportNum.toJson(),
    "address": address.toJson(),
    "file_num": fileNum.toJson(),
    "father": father.toJson(),
    "mother": mother.toJson(),
    "old_doi": oldDoi.toJson(),
    "old_passport_num": oldPassportNum.toJson(),
    "old_place_of_issue": oldPlaceOfIssue.toJson(),
    "pin": pin.toJson(),
    "spouse": spouse.toJson(),
    "tag": tag,
  };
}

class Address {
  Address({
    this.conf,
    this.value,
    this.city,
    this.pin,
    this.line1,
    this.line2,
    this.state,
    this.locality,
    this.houseNumber,
    this.street,
    this.district,
    this.landmark,
  });

  int conf;
  String value;
  String city;
  String pin;
  String line1;
  String line2;
  String state;
  String locality;
  String houseNumber;
  String street;
  String district;
  String landmark;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    conf: json["conf"],
    value: json["value"],
    city: json["city"],
    pin: json["pin"],
    line1: json["line1"],
    line2: json["line2"],
    state: json["state"],
    locality: json["locality"],
    houseNumber: json["house_number"],
    street: json["street"],
    district: json["district"],
    landmark: json["landmark"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
    "city": city,
    "pin": pin,
    "line1": line1,
    "line2": line2,
    "state": state,
    "locality": locality,
    "house_number": houseNumber,
    "street": street,
    "district": district,
    "landmark": landmark,
  };
}

class Father {
  Father({
    this.conf,
    this.value,
  });

  int conf;
  String value;

  factory Father.fromJson(Map<String, dynamic> json) => Father(
    conf: json["conf"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
  };
}
