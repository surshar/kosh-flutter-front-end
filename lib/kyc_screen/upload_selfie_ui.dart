import 'dart:convert';
import 'dart:io';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/api_client.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:cryptocase/kyc_screen/kyc_details_controller.dart';
import 'package:cryptocase/kyc_screen/verify_document_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

class UploadSelfieUi extends StatefulWidget {
  @override
  _UploadSelfieUiState createState() => _UploadSelfieUiState();
}

class _UploadSelfieUiState extends State<UploadSelfieUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  final kyccontroller = Get.put(KycDetailController());
  final verifydocumentcontroller = Get.put(VerifyDocumentController());
  File _imageSelfie;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  sendDocument(File imageFile, String vaultLink) async {
    http.Response response = await ApiClient().getkycDocumentResponse(imageFile, vaultLink);
  }

  verifyDocument(String doc_type, String front_or_back) {
    verifydocumentcontroller.fetchPanVerifyResponse(doc_type, front_or_back);
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            appBarsignup(
                height * 0.1,
                width,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                "Upload Selfie"),
            Expanded(
                child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: GestureDetector(
                      onTap: () async {
                        await _imgFromCamera();

                        await sendDocument(_imageSelfie, UPLOADSELFIE);

                        var response = await ApiClient().getSelfieResponse();
                        var responseResult = json.decode(response.body);

                        if (responseResult["success"] == true) {
                          greenSnack("Selfie picture uploaded");
                        } else {
                          redSnack("${responseResult["error"]["message"]}");
                        }
                      },
                      child: Container(
                        child: _imageSelfie != null
                            ? Image.file(
                                _imageSelfie,
                                height: width * 0.8,
                                width: width * 0.8,
                                fit: BoxFit.fill,
                              )
                            : SvgPicture.asset(
                                'assets/upload_icon.svg',
                                height: width * 0.4,
                                width: width * 0.4,
                              ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: _imageSelfie != null ? false : true,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30.0),
                      child: textWidget(
                          "Please take a Selfie",
                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                          FontWeight.w600,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                          TextAlign.center),
                    ),
                  )
                ],
              ),
            )),
            Container(
              margin: EdgeInsets.only(left: 0, right: 0),
              height: height * 0.07,
              child: GestureDetector(
                onTap: () async {
                  if (_imageSelfie != null) {
                    var response = await ApiClient().getInstanstKYCResponse();

                    var responseResult = json.decode(response.body);

                    if (responseResult["success"] == true) {
                      Get.to(() => HomeScreen());
                      greenSnack("Kyc Verified Successfully");
                    } else {
                      redSnack("${responseResult["error"]["message"]}");
                    }
                  } else {
                    redSnack("Invalid Photo");
                  }
                },
                child: loginBottomButton(
                    // width * 0.9,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? buttongreen : buttongreen,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                    "Upload Selfie"),
              ),
            )
          ],
        ),
      ),
    );
  }

  _imgFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50, preferredCameraDevice: CameraDevice.front);
    _imageSelfie = File(image.path);
    setState(() {});
  }
}
