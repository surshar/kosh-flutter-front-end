
import 'dart:convert';

AadharverifyModel aadharverifyModelFromJson(String str) => AadharverifyModel.fromJson(json.decode(str));

String aadharverifyModelToJson(AadharverifyModel data) => json.encode(data.toJson());

class AadharverifyModel {
  AadharverifyModel({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory AadharverifyModel.fromJson(Map<String, dynamic> json) => AadharverifyModel(
    success: json["success"],
    data: json["success"]? List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))): null,
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.type,
    this.details,
  });

  String type;
  Details details;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    type: json["type"],
    details: Details.fromJson(json["details"]),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "details": details.toJson(),
  };
}

class Details {
  Details({
    this.name,
    this.phone,
    this.aadhaar,
    this.address,
    this.father,
    this.husband,
    this.pin,
    this.tag,
    this.gender,
    this.yob,
    this.dob,
    this.mother,
    this.qr,
  });

  Father name;
  Father phone;
  Aadhaar aadhaar;
  Address address;
  Father father;
  Father husband;
  Father pin;
  String tag;
  Father gender;
  Father yob;
  Father dob;
  Father mother;
  Qr qr;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
    name: Father.fromJson(json["name"]),
    phone: json["phone"] == null ? null : Father.fromJson(json["phone"]),
    aadhaar: Aadhaar.fromJson(json["aadhaar"]),
    address: json["address"] == null ? null : Address.fromJson(json["address"]),
    father: Father.fromJson(json["father"]),
    husband: json["husband"] == null ? null : Father.fromJson(json["husband"]),
    pin: json["pin"] == null ? null : Father.fromJson(json["pin"]),
    tag: json["tag"],
    gender: json["gender"] == null ? null : Father.fromJson(json["gender"]),
    yob: json["yob"] == null ? null : Father.fromJson(json["yob"]),
    dob: json["dob"] == null ? null : Father.fromJson(json["dob"]),
    mother: json["mother"] == null ? null : Father.fromJson(json["mother"]),
    qr: json["qr"] == null ? null : Qr.fromJson(json["qr"]),
  );

  Map<String, dynamic> toJson() => {
    "name": name.toJson(),
    "phone": phone == null ? null : phone.toJson(),
    "aadhaar": aadhaar.toJson(),
    "address": address == null ? null : address.toJson(),
    "father": father.toJson(),
    "husband": husband == null ? null : husband.toJson(),
    "pin": pin == null ? null : pin.toJson(),
    "tag": tag,
    "gender": gender == null ? null : gender.toJson(),
    "yob": yob == null ? null : yob.toJson(),
    "dob": dob == null ? null : dob.toJson(),
    "mother": mother == null ? null : mother.toJson(),
    "qr": qr == null ? null : qr.toJson(),
  };
}

class Aadhaar {
  Aadhaar({
    this.conf,
    this.value,
    this.ismasked,
  });

  int conf;
  String value;
  String ismasked;

  factory Aadhaar.fromJson(Map<String, dynamic> json) => Aadhaar(
    conf: json["conf"],
    value: json["value"],
    ismasked: json["ismasked"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
    "ismasked": ismasked,
  };
}

class Address {
  Address({
    this.conf,
    this.value,
    this.city,
    this.pin,
    this.line1,
    this.line2,
    this.careOf,
    this.state,
    this.locality,
    this.landmark,
    this.houseNumber,
    this.street,
    this.district,
  });

  int conf;
  String value;
  String city;
  String pin;
  String line1;
  String line2;
  String careOf;
  String state;
  String locality;
  String landmark;
  String houseNumber;
  String street;
  String district;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    conf: json["conf"],
    value: json["value"],
    city: json["city"],
    pin: json["pin"],
    line1: json["line1"],
    line2: json["line2"],
    careOf: json["care_of"],
    state: json["state"],
    locality: json["locality"],
    landmark: json["landmark"],
    houseNumber: json["house_number"],
    street: json["street"],
    district: json["district"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
    "city": city,
    "pin": pin,
    "line1": line1,
    "line2": line2,
    "care_of": careOf,
    "state": state,
    "locality": locality,
    "landmark": landmark,
    "house_number": houseNumber,
    "street": street,
    "district": district,
  };
}

class Father {
  Father({
    this.conf,
    this.value,
  });

  int conf;
  String value;

  factory Father.fromJson(Map<String, dynamic> json) => Father(
    conf: json["conf"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "conf": conf,
    "value": value,
  };
}

class Qr {
  Qr({
    this.value,
  });

  String value;

  factory Qr.fromJson(Map<String, dynamic> json) => Qr(
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "value": value,
  };
}
