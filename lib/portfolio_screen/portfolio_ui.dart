import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/all_bucket_controller.dart';
import 'package:cryptocase/home_screen/all_bucket_model.dart';
import 'package:cryptocase/my_bucket_screen/my_bucket_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PortfilioUi extends StatefulWidget {
  @override
  _PortfilioUiState createState() => _PortfilioUiState();
}

class _PortfilioUiState extends State<PortfilioUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  bool isLoggedin;

  @override
  void initState() {
    super.initState();
    getTheme();
    checkLogin();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  void checkLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    isLoggedin = preferences.getBool(LOGGED_IN) ?? false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light
          ? backgroundgrey
          : backgroundlightblue,
      body: Container(
        padding: EdgeInsets.all(20),
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: ListView(
            children: [
              Container(
                margin: EdgeInsets.only(top: height * 0.02),
                child: appbarwithIcons(
                    "Explore buckets 🧐",
                    "assets/search_icon.svg",
                    height * 0.06,
                    width,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : white,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textgrey
                        : white,
                    false),
              ),
              Container(
                height: isLoggedin == true ? height * 0.25 : height,
                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: FutureBuilder<AllBucketModel>(
                      future:
                          AllBucketController().fetchAllBucketResponse(),
                      builder: (context, value) {
                        if (!value.hasData) {
                          return Center(
                            child: Container(
                                width: width * 0.90,
                                height: width * 0.45,
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                                  color: adaptiveThemeMode ==
                                      AdaptiveThemeMode.light
                                      ? white
                                      : darktheme,
                                ),
                                child: const CupertinoActivityIndicator()),
                          );
                        } else {
                          return ListView.builder(
                              itemCount: value.data.data == null
                                  ? 0
                                  : value.data.data.length,
                              scrollDirection: isLoggedin == true
                                  ? Axis.horizontal
                                  : Axis.vertical,
                              itemBuilder: (context, index) {
                                return Container(
                                    margin: EdgeInsets.only(left: 9),
                                    height: double.infinity,
                                    width: width * 0.9,
                                    child: getHedgeAgainstInrCard(
                                        index.toString(),
                                        height,
                                        width,
                                        adaptiveThemeMode,
                                        isLoggedin,
                                        context,
                                        index,
                                        datum: value.data.data[index],
                                        onTap: () {
                                      Get.to(() =>MyBucketUi());
                                    }));
                              });
                        }
                        //
                      }),
                ),
              ),
              Visibility(
                visible: isLoggedin == true ? true : false,
                child: textWidget(
                    "My buckets 💰💵",
                    ScreenUtil().setSp(55, allowFontScalingSelf: true),
                    FontWeight.w600,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : white,
                    TextAlign.start),
              ),
              Visibility(
                visible: isLoggedin == true ? true : false,
                child: GestureDetector(
                  onTap: () {
                    Get.to(()=>MyBucketUi());
                  },
                  child: Container(
                      width: width * 0.9,
                      child: getmyBucketCard(height, width, adaptiveThemeMode)),
                ),
              ),
              Visibility(
                visible: isLoggedin == true ? true : false,
                child: GestureDetector(
                  onTap: () {
                    Get.to(()=>MyBucketUi());
                  },
                  child: Container(
                      width: width * 0.9,
                      child: getmyBucketCard(height, width, adaptiveThemeMode)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
