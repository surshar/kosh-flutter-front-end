const String APPNAME = "Cryptocase";

const String INVEST = "Invest in\nLatest Trend";
String HEDGEBUCKETNAME = "";
String HEDGEBUCKETDESCRIPTION = "";

const List SELECTDOCUMENT = ['Passport', 'Aadhaar Card', 'Voter ID'];

const String LOGINTITLE = "Hello\nLogin Here!";
const String FORGOTPASSWORD = "Forgot Password?";
const String DONTHAVEACCOUNT = "Don't Have an account?";
const String SIGNUP = "Sign Up";
const String CONTINUE = "Continue as a guest";
const String PANCARDNO = "Pancard No.";
const String UPLOADPANCARD = "Upload your Pan card";
const String UPLOADDOCUMNET = "Upload your Document";
const String DOCUMENTTYPE = "Select Document Type";
const String CREATEACCOUNT = "Create your account";
const String FILLFORM = "Fill the From given below";
const String NAME = "Name";
const String VERIFYIDENTY =
    "We need few additional details in order to verify your identity";

const String LOGGED_IN = "isLoggedIn";
const String TIMESTAMP = "timestamp";
const String THEMEMODE = "themeMode";
const String CURRENCYRUPESS = "₹";
const String GOOGLESIGNIN = "Sign in with Google";
const String FACEBOOKSIGNIN = "Sign in with Facebook";
const String FOURDIGITCODE = "Enter 6 digits code";
const String ENTERFOURDIGIT =
    "Enter the 6 digits code that you received on your registered Email.";
const String SETNEWPASSWORD =
    "Set the new password for your account so you can login and access all the features.";
const String ENTERMOBILENUMBER =
    "Enter your email address for the verification proccess, we send 6 digits code to your email address";
const CONFIRMREBALANCEAMOUNT = "Confirm rebalance amount";
const LEARNMORE = " Learn More";
const CONFIRMINVEST = "Confirm Investing Amount";
const WALLETBALANCE = "Wallet Balance";
const MININVESTMENT = "Min. Investment Amount: ₹ 2000";
const CONFIRMSELL = "Confirm Sell Amount";
const MINSELL = "Min. Sell Amount: ₹ 2000";
const CANCELINSTALLMENT = "All installments will be cancelled from now on.";
const PROFILE = "Profile";
const ALINAJAMES = "Alina James";
const KYCVERIFIED = "KYC Verified ";
const MOBILENUMBER = "M: +91 900223812";
const PAYMENTMETHOD = "Set up payment method";
const BANKOFINDIA = "Bank of India";
const BANKNUMBER = "**** **** **89 3134";
const ADDFUNDS = "Add funds";
const WITHDRAWINR = "Withdraw INR";
const HELP = "Help";
const KYC = "USER KYC";
const ABOUT = "About";
const PRIVACY = "Privacy";
const TRADEHISTORY = "Trade History";
const RATESTAR = "Rate Us";
const SHARE = "Share";
const CURRENTBALANCE = "Current balance";
const ENTERAMOUNT = "Enter amount";
const AMOUNTWITHDRAW =
    "Amount to withdraw should be between \n ₹500 to ₹ 5,000";
const PROCESSTIME = "Processing time upto 10 mins";
const String TRANSFERTEXT = "Transfer to our account";
const String NOTETEXTONE = "Plase deposit from your own personal account";
const String NOTETEXTTWO =
    "Enter your name in your bank’s transaction remarks.";
const String NOTETEXTTHREE = "Use RTGS for deposits > 1 lakh";
const String BALANCETEXT = "Your INR balance will be updated in 4 hours";
const String TRANSACTION = "Enter details of your transaction";
const String TEXTTRANSFERED = "Account transferred";
const String TRANSACTIONID = "Transaction Id";
const String FINDTEXT = "How to find this?";
const String ETHEREUMTEXT =
    "Ethereum (ETH) has become a\nmarket-leading bitcoin";
const String ADDWALLET =
    "You have added ₹4,500 in your\nwallet. Updated balance is ₹7,500";
const String EMAIL = "email";
const String LOGOUT = "Logout";
const String LOGIN = "Login";

const String THEME = "Theme";


const String TITLEONE = "Buy into diversified portfolios";
const String TITLETWO = "Curated by Crypto Experts";
const String TITLETHREE = "Easy to Use App";

const String SUBTITLEONE = "Derisk from single token volatility";
const String SUBTITLETWO = "No More Tracking Candle Charts, Worry-free Investment";
const String SUBTITLETHREE = "Simplified Portfolio View, Seamless Experience";

const List SELECTDAY = [
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '10',
  '11',
  '12',
  '13',
  '14',
  '15',
  '16',
  '17',
  '18',
  '19',
  '20',
  '21',
  '22',
  '23',
  '24',
  '25',
  '26',
  '27',
  '28',
  '29',
  '30',
  '31'
];

const List SELECTYEAR = [
  '1901',
  '1902',
  '1903',
  '1904',
  '1905',
  '1906',
  '1907',
  '1908',
  '1909',
  '1910',
  '1911',
  '1912',
  '1913',
  '1914',
  '1915',
  '1916',
  '1917',
  '1918',
  '1919',
  '1920',
  '1921',
  '1922',
  '1923',
  '1924',
  '1925',
  '1926',
  '1927',
  '1928',
  '1929',
  '1930',
  '1931',
  '1932',
  '1933',
  '1934',
  '1935',
  '1936',
  '1937',
  '1938',
  '1939',
  '1940',
  '1941',
  '1942',
  '1943',
  '1944',
  '1945',
  '1946',
  '1947',
  '1948',
  '1949',
  '1950',
  '1951',
  '1952',
  '1953',
  '1954',
  '1955',
  '1956',
  '1957',
  '1958',
  '1959',
  '1960',
  '1961',
  '1962',
  '1963',
  '1964',
  '1965',
  '1966',
  '1967',
  '1968',
  '1969',
  '1970',
  '1971',
  '1972',
  '1973',
  '1974',
  '1975',
  '1976',
  '1977',
  '1978',
  '1979',
  '1980',
  '1981',
  '1982',
  '1983',
  '1984',
  '1985',
  '1986',
  '1987',
  '1988',
  '1989',
  '1990',
  '1991',
  '1992',
  '1993',
  '1994',
  '1995',
  '1996',
  '1997',
  '1998',
  '1999',
  '2000',
  '2001',
  '2002',
  '2003',
  '2004',
  '2005',
  '2006',
  '2007',
  '2008',
  '2009',
  '2010',
  '2011',
  '2012',
  '2013',
  '2014',
  '2015',
  '2016',
  '2017',
  '2018',
  '2019',
  '2020',
  '2021'
];

const List SELECTMONTHS = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];

const String BASE_URL = "https://thehouseofbakers.com/crypto-app";
const String REFRESHTOKEN = "refresh_token";
const String ACCESSTOKEN = "access_token";
const String APITOKEN = "";
 String UPLOADFRONT = "";
 String UPLOADBACK = "";
 String UPLOADSELFIE = "";
 String UPPANCARD = "";
