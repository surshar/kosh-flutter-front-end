import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/home_screen/all_bucket_model.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:cryptocase/home_screen/notification_ui.dart';
import 'package:cryptocase/login_screen/login_controller.dart';
import 'package:cryptocase/login_screen/login_ui.dart';
import 'package:cryptocase/login_screen/rebalance_screen.dart';
import 'package:cryptocase/registration_screen/signup_ui.dart';
import 'package:cryptocase/my_bucket_screen/my_bucket_ui.dart';
import 'package:cryptocase/sell_invest_screen/invest_screen.dart';
import 'package:cryptocase/sell_invest_screen/sell_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'colors.dart';

Text textWidget(
    String text, double textsize, FontWeight textWeight, Color textcolor, TextAlign mtextAlign) {
  return Text(
    text,
    style: TextStyle(
      fontSize: textsize,
      fontWeight: textWeight,
      fontFamily: 'WorkSans',
      color: textcolor,
    ),
    textAlign: mtextAlign,
  );
}

Container appBar(bool isSkip, double mheight, double mwidth, Color textcolor1, Color textcolor2) {
  return Container(
    height: mheight,
    width: mwidth,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        textWidget("Cryptocase", ScreenUtil().setSp(50, allowFontScalingSelf: true),
            FontWeight.w600, textcolor1, TextAlign.start),
        Visibility(
            visible: isSkip ? true : false,
            child: GestureDetector(
              onTap: () {
                Get.offAll(LoginUi());
              },
              child: textWidget("Skip", ScreenUtil().setSp(40, allowFontScalingSelf: true),
                  FontWeight.w500, textcolor2, TextAlign.start),
            )),
      ],
    ),
  );
}

Container appbarwithIcons(String title, String mimage, double mheight, double mwidth,
    Color textcolor1, Color textcolor2, bool noti) {
  return Container(
    height: mheight,
    width: mwidth,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        textWidget(title, ScreenUtil().setSp(55, allowFontScalingSelf: true), FontWeight.w700,
            textcolor1, TextAlign.start),
        GestureDetector(
          onTap: () {
            if (noti) {
              Get.to(Notification_Ui());
            } else {}
          },
          child: SvgPicture.asset(
            mimage,
            height: mwidth * 0.06,
            width: mwidth * 0.06,
            fit: BoxFit.cover,
            color: textcolor2,
          ),
        )
      ],
    ),
  );
}

Container appBarsignup(
    double mheight, double mwidth, Color buttoncolor1, Color textcolor2, String text) {
  return Container(
    height: mheight,
    width: mwidth,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Icon(Icons.arrow_back_ios, color: buttoncolor1)),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: textWidget(text, ScreenUtil().setSp(50, allowFontScalingSelf: true),
                FontWeight.w600, textcolor2, TextAlign.start),
          ),
        )
      ],
    ),
  );
}

Container loginBottomButton(Color buttoncolor1, Color textcolor2, String text) {
  return Container(
      decoration:
          BoxDecoration(color: buttoncolor1, borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Center(
        child: textWidget(text, ScreenUtil().setSp(35, allowFontScalingSelf: true), FontWeight.w600,
            textcolor2, TextAlign.start),
      ));
}

//For TextField
Widget getTextField({String hint, Color color, Color hintColor}) {
  return Container(
    child: TextField(
      decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: textdarkblue, width: 1),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: textborder, width: 1),
          ),
          contentPadding: EdgeInsets.all(20),
          filled: true,
          fillColor: color,
          hintText: hint,
          hintStyle: TextStyle(
              fontSize: 13, fontWeight: FontWeight.w400, color: hintColor, fontFamily: 'WorkSans')),
    ),
  );
}

//For Google and Facebook Sign Up Button
Container getSignUpButton(String imageName, String text) {
  return Container(
    decoration:
        BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), color: buttonbgcolor),
    child: Padding(
      padding: EdgeInsets.all(15),
      child: Row(
        children: [
          SvgPicture.asset(imageName),
          Expanded(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'WorkSans',
                  fontSize: 16,
                  fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
    ),
  );
}

//For Login Button
Widget getButton(String text) {
  return Container(
    decoration:
        BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8)), color: buttongreen),
    child: Center(
      child: Padding(
        padding: EdgeInsets.all(15),
        child: Text(
          text,
          style: TextStyle(color: white, fontFamily: 'WorkSans', fontWeight: FontWeight.w600),
        ),
      ),
    ),
  );
}

class BucketData {
  final id, name, description;

  BucketData(this.id, this.name, this.description);
}

Widget getHedgeAgainstInrCard(String id, double height, double width,
    AdaptiveThemeMode adaptiveThemeMode, bool isLoggedin, BuildContext context, int postion,
    {@required Datum datum, @required Function onTap}) {
  return GestureDetector(
    onTap: () {
      Get.to(MyBucketUi(), arguments: datum.id);
      HEDGEBUCKETNAME = datum.bucketName.toString();
      HEDGEBUCKETDESCRIPTION = datum.bucketDescription.toString();
    },
    child: Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(17)),
          color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme),
      child: Column(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10, top: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: width * 0.12,
                    height: width * 0.12,
                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)), color: textblue),
                    child: SvgPicture.asset(
                      'assets/drop_hedge_icon.svg',
                      fit: BoxFit.fill,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      child: Html(
                        data: datum.bucketName,
                        defaultTextStyle: TextStyle(
                            fontSize: ScreenUtil().setSp(45, allowFontScalingSelf: true),
                            fontWeight: FontWeight.w600,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)),
                        color: buttongreen),
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                    child: textWidget(
                        "+5.47%",
                        ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                        TextAlign.start),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 100,
              padding: EdgeInsets.only(left: 10, top: 10),
              child: Container(
                height: 100,
                child: SingleChildScrollView(
                  child: Html(
                    data: datum.bucketDescription,
                    defaultTextStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(40, allowFontScalingSelf: true),
                      fontWeight: FontWeight.w500,
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                if (isLoggedin) {
                  if (postion == 0) {
                    Get.to(MyBucketUi(), arguments: 0);
                  } else {
                    Get.to(MyBucketUi(), arguments: 1);
                  }
                } else {
                  // buildBottomeLoginScreen(context, adaptiveThemeMode);
                  if (postion == 0) {
                    Get.to(MyBucketUi(), arguments: 0);
                  } else {
                    Get.to(MyBucketUi(), arguments: 1);
                  }
                }
              },
              child: Container(
                margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12)), color: buttongreen),
                child: Center(
                  child: Text(
                    "Invest Now",
                    style: TextStyle(
                        color: white,
                        fontFamily: 'WorkSans',
                        fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

Widget getmyBucketCard(double height, double width, AdaptiveThemeMode adaptiveThemeMode) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(17)),
        color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(left: 15, top: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: width * 0.12,
                height: width * 0.12,
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)), color: textblue),
                child: SvgPicture.asset(
                  'assets/drop_hedge_icon.svg',
                  fit: BoxFit.fill,
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: textWidget(
                      HEDGEBUCKETNAME,
                      ScreenUtil().setSp(45, allowFontScalingSelf: true),
                      FontWeight.w600,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                      TextAlign.start),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Get.to(RebalanceScreen());
                },
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)),
                      color: buttongreen),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                  child: textWidget(
                      "Rebalance",
                      ScreenUtil().setSp(35, allowFontScalingSelf: true),
                      FontWeight.w500,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                      TextAlign.start),
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          child: textWidget(
              "Amount Invested",
              ScreenUtil().setSp(40, allowFontScalingSelf: true),
              FontWeight.w500,
              adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : textgrey,
              TextAlign.start),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textWidget(
                  "₹ 5,505",
                  ScreenUtil().setSp(60, allowFontScalingSelf: true),
                  FontWeight.w800,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  TextAlign.start),
              textWidget(
                  "+ ₹ 1,021 (5.47%)",
                  ScreenUtil().setSp(45, allowFontScalingSelf: true),
                  FontWeight.w500,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textgreen : textgreen,
                  TextAlign.start),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          color: adaptiveThemeMode == AdaptiveThemeMode.light ? dividercolor : textgrey,
          height: 1,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textWidget(
                  "Distribution",
                  ScreenUtil().setSp(35, allowFontScalingSelf: true),
                  FontWeight.w800,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : textgrey,
                  TextAlign.start),
              textWidget("45%", ScreenUtil().setSp(35, allowFontScalingSelf: true), FontWeight.w500,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white, TextAlign.start),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
          child: LinearPercentIndicator(
            lineHeight: 8.0,
            percent: 0.4,
            backgroundColor: Color.fromRGBO(0, 0, 0, 0.05),
            progressColor: textgreen,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 20),
          child: Container(
            height: height * 0.05,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Get.to(SellScreenUi());
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)), color: textred),
                      child: Center(
                        child: Text(
                          "Sell",
                          style: TextStyle(
                              color: white,
                              fontFamily: 'WorkSans',
                              fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Get.to(InvestScreenUi());
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)), color: buttongreen),
                      child: Center(
                        child: Text(
                          "Buy more",
                          style: TextStyle(
                              color: white,
                              fontFamily: 'WorkSans',
                              fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

Container buildBottomeLoginScreen(BuildContext context, AdaptiveThemeMode adaptiveThemeMode) {
  var width = MediaQuery.of(context).size.width;
  var height = MediaQuery.of(context).size.height;
  final logincontroller = Get.put(LoginController());
  bool _isObscure2 = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  showModalBottomSheet(
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius:
            BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
      ),
      context: context,
      builder: (context) {
        TextEditingController phoneController = TextEditingController();
        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: StatefulBuilder(
              builder: (BuildContext context, StateSetter stateSetter) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Container(
                        margin: EdgeInsets.only(top: height * 0.02),
                        height: height * 0.01,
                        width: width * 0.2,
                        decoration: BoxDecoration(
                          color:
                              adaptiveThemeMode == AdaptiveThemeMode.light ? dividercolor : white,
                          borderRadius: BorderRadius.circular(7),
                        ),
                      ),
                    ),
                    SizedBox(height: height * 0.03),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                      child: Text(
                        'Login Now',
                        style: TextStyle(
                            color:
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            fontSize: ScreenUtil().setSp(50, allowFontScalingSelf: true),
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(height: height * 0.02),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                      child: Text(
                        'If you want to access all the features. you need to login',
                        style: TextStyle(
                            color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(height: height * 0.04),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                      child: Text(
                        'E-mail id',
                        style: TextStyle(
                            color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            fontSize: ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(height: height * 0.02),
                    Container(
                      padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                      child: TextField(
                        controller: emailController,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          color:
                              adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w400,
                        ),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor:
                              adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12.0),
                            borderSide: BorderSide(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light ? white : textgrey,
                            ),
                          ),
                          hintStyle: TextStyle(
                            fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                          ),
                          hintText: 'Email Address',
                          contentPadding: EdgeInsets.all(20),
                        ),
                      ),
                    ),
                    SizedBox(height: height * 0.02),
                    Padding(
                      padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                      child: Text(
                        'Password',
                        style: TextStyle(
                            color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            fontSize: ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(height: height * 0.02),
                    Container(
                      padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                      child: TextFormField(
                        obscureText: !_isObscure2,
                        controller: passwordController,
                        validator: (value) {
                          return value.length < 8
                              ? 'Password must be less than 8 characters'
                              : null;
                        },
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          color:
                              adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w400,
                        ),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor:
                              adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12.0),
                            borderSide: BorderSide(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light ? white : textgrey,
                            ),
                          ),
                          hintStyle: TextStyle(
                              fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w400,
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white),
                          hintText: 'Password',
                          contentPadding: EdgeInsets.all(20),
                          suffixIcon: IconButton(
                            icon: Icon(
                              _isObscure2 ? Icons.visibility : Icons.visibility_off,
                            ),
                            onPressed: () {
                              stateSetter(() {
                                _isObscure2 = !_isObscure2;
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: height * 0.04),
                    GestureDetector(
                      onTap: () {
                        if (emailController.text.trim().isEmpty) {
                          redSnack("Email field is empty");
                        } else if (passwordController.text.trim().isEmpty) {
                          redSnack("Password field is empty");
                        } else {
                          setUserInformation();
                          String email = emailController.text.trim();
                          String password = passwordController.text.trim();
                          logincontroller.fetchLoginResponse(email, password);
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                        child: getButton('Login'),
                      ),
                    ),
                    SizedBox(height: height * 0.04),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          textWidget(
                              DONTHAVEACCOUNT,
                              ScreenUtil().setSp(40, allowFontScalingSelf: true),
                              FontWeight.w400,
                              adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                              TextAlign.end),
                          SizedBox(width: width * 0.01),
                          GestureDetector(
                            onTap: () {
                              Get.offAll(SignupUi());
                            },
                            child: textWidget(
                                SIGNUP,
                                ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                FontWeight.w600,
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textblue
                                    : buttongreen,
                                TextAlign.end),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: height * 0.04),
                  ],
                );
              },
            ),
          ),
        );
      });
}

Container appbarrebalance(bool isSkip, double mheight, double mwidth, Color buttoncolor1,
    Color buttoncolor2, Color textcolor1, String text, String imagename) {
  return Container(
    height: mheight,
    width: mwidth,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Icon(Icons.arrow_back_ios, color: buttoncolor1)),
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: textWidget(text, ScreenUtil().setSp(50, allowFontScalingSelf: true),
              FontWeight.w600, textcolor1, TextAlign.start),
        ),
        Visibility(
            visible: isSkip ? true : false,
            child: SvgPicture.asset(
              imagename,
              color: buttoncolor2,
            ))
      ],
    ),
  );
}

Container appbarinvestsell(
  double mheight,
  double mwidth,
  Color buttoncolor1,
) {
  return Container(
    height: mheight,
    width: mwidth,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Icon(Icons.arrow_back_ios, color: buttoncolor1)),
      ],
    ),
  );
}

void alertDailog(BuildContext context, Color containercolor, Color textcolor1, Color textcolor2,
    Color textcolor3) {
  var width = MediaQuery.of(context).size.width;
  var height = MediaQuery.of(context).size.height;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          child: Container(
              width: width * 0.9,
              height: height * 0.5,
              decoration: BoxDecoration(
                color: containercolor,
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset("assets/rafiki.svg"),
                  SizedBox(
                    height: height * 0.03,
                  ),
                  textWidget(
                      "Something’s not right",
                      ScreenUtil().setSp(50, allowFontScalingSelf: true),
                      FontWeight.w500,
                      textcolor1,
                      TextAlign.center),
                  SizedBox(
                    height: height * 0.01,
                  ),
                  textWidget(
                      "There’s a technical issue at your bank",
                      ScreenUtil().setSp(35, allowFontScalingSelf: true),
                      FontWeight.w500,
                      textcolor2,
                      TextAlign.center),
                  Container(
                    height: height * 0.12,
                    alignment: Alignment.bottomCenter,
                    child: textWidget(
                        "Please try again later",
                        ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        FontWeight.w500,
                        textcolor3,
                        TextAlign.center),
                  ),
                ],
              )),
        );
      });
}

Widget DepositeIntTextField(
    bool isSkip, String hint, Color color, Color hintColor, Color iconColor, double size) {
  return TextField(
    decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(color: textdarkblue, width: 1),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(color: textborder, width: 1),
        ),
        contentPadding: EdgeInsets.all(size),
        filled: true,
        fillColor: color,
        hintText: hint,
        suffixIcon: Visibility(
          visible: isSkip ? true : false,
          child: Icon(Icons.copy, color: iconColor),
        ),
        hintStyle: TextStyle(
            fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
            fontWeight: FontWeight.w400,
            color: hintColor,
            fontFamily: 'WorkSans')),
  );
}

Container NewAppBar(bool isSkip, double mheight, double mwidth, String text, double textsize,
    Color color, IconButton icon1, IconButton icon2) {
  return Container(
    height: mheight,
    width: mwidth,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        icon1,
        Expanded(
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontFamily: 'WorkSans',
                fontSize: textsize,
                color: color,
                fontWeight: FontWeight.w600),
          ),
        ),
        Visibility(visible: isSkip ? true : false, child: icon2),
      ],
    ),
  );
}

void redSnack(String msg) {
  Get.snackbar("Error", msg,
      backgroundColor: Colors.red,
      colorText: Colors.white,
      snackStyle: SnackStyle.GROUNDED,
      duration: Duration(seconds: 3),
      isDismissible: true,
      dismissDirection: SnackDismissDirection.HORIZONTAL,
      snackPosition: SnackPosition.TOP);
}

void greenSnack(String msg) {
  Get.snackbar("Success", msg,
      backgroundColor: Colors.green,
      colorText: Colors.white,
      snackStyle: SnackStyle.GROUNDED,
      duration: Duration(seconds: 2),
      isDismissible: true,
      dismissDirection: SnackDismissDirection.HORIZONTAL,
      snackPosition: SnackPosition.TOP);
}

void setUserInformation() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.setBool(LOGGED_IN, true);
  Get.offAll(HomeScreen());
}
