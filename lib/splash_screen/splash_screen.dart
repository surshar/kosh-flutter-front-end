import 'dart:async';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:cryptocase/login_screen/login_ui.dart';
import 'package:cryptocase/theme_screen/theme_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AdaptiveThemeMode adaptiveThemeMode;
  bool isThemeselected;
  bool isLoggedin;
  int timeStamp;

  @override
  void initState() {
    super.initState();
    getTheme();
    checkTheme();
    checkLogin();

    Timer(
      Duration(seconds: 3),
      () => _navigationDecider(),
    );
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
        backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        body: Center(
            child: adaptiveThemeMode == AdaptiveThemeMode.light
                ? textWidget(APPNAME, ScreenUtil().setSp(80, allowFontScalingSelf: true),
                    FontWeight.w600, textdarkblue, TextAlign.start)
                : textWidget(APPNAME, ScreenUtil().setSp(80, allowFontScalingSelf: true),
                    FontWeight.w600, white, TextAlign.start)));
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  void checkTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isThemeselected = prefs.getBool('isThemSelected') ?? false;
  }

  void checkLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    timeStamp = preferences.getInt(TIMESTAMP);
    print("TIMESTAMP FROM SPLASHSCREEN : ${timeStamp}");
    isLoggedin = (preferences.getBool(LOGGED_IN) ?? false);

    setState(() {});
  }

  bool isTimeValid(int timeStamp) {
    int timelimit = 14400000 + timeStamp;
    var currentTime = DateTime.now().millisecondsSinceEpoch;
    return timelimit >= currentTime;
  }

  _navigationDecider() {
    if (timeStamp == null) {
      Get.offAll(ThemeUi());
    } else {
      if (isThemeselected) {
        if (isLoggedin) {
          if (isTimeValid(timeStamp)) {
            Get.offAll(() => HomeScreen());
          } else {
            Get.offAll(() => LoginUi());
          }
        } else {
          Get.offAll(() => LoginUi());
        }
      } else {
        Get.offAll(() => ThemeUi());
      }
    }
  }
}
