import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/login_screen/done_Screen.dart';
import 'package:cryptocase/token_screen/token_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class RebalanceScreen extends StatefulWidget {
  @override
  _RebalanceScreenState createState() => _RebalanceScreenState();
}

class _RebalanceScreenState extends State<RebalanceScreen> {
  AdaptiveThemeMode adaptiveThemeMode;
  TextEditingController amountController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: ()=> FocusScope.of(context).unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor:
            adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                child: appbarrebalance(
                    true,
                    height * 0.1,
                    width,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : white,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textgrey
                        : white,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : white,
                    'Rebalance amount',
                    'assets/share_icon.svg'),
              ),
              Container(
                  margin: EdgeInsets.only(left: 0, top: height * 0.01),
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  height: height * 0.75,
                  child: buildMainScreen(context, width, height)),
              Expanded(
                child: GestureDetector(
                  onVerticalDragUpdate: (dragUpdateDetails) {
                    Get.to(DoneScreen(), arguments: ('Rebalance'));
                  },
                  child: Container(
                    color: buttongreen,
                    width: width,
                    height: height * 0.11,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: width * 0.3),
                          child: SvgPicture.asset(
                            'assets/swipe_up.svg',
                            height: height * 0.02,
                            width: width * 0.02,
                            color: white,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 12.0),
                          child: textWidget(
                              'Swipe up to confirm',
                              ScreenUtil().setSp(35, allowFontScalingSelf: true),
                              FontWeight.w500,
                              adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? white
                                  : white,
                              TextAlign.start),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  buildMainScreen(BuildContext context, double width, double height) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          textWidget(
              CONFIRMREBALANCEAMOUNT,
              ScreenUtil().setSp(47, allowFontScalingSelf: true),
              FontWeight.w500,
              adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textdarkblue
                  : white,
              TextAlign.start),
          Row(
            children: [
              textWidget(
                  'you have to pay ₹ 2500',
                  ScreenUtil().setSp(35, allowFontScalingSelf: true),
                  FontWeight.w400,
                  adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textdarkblue
                      : white,
                  TextAlign.start),
              textWidget(
                  LEARNMORE,
                  ScreenUtil().setSp(35, allowFontScalingSelf: true),
                  FontWeight.w400,
                  adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textblue
                      : textgreen,
                  TextAlign.start),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: height * 0.06),
            child:Container(
              width: width,
              child: TextFormField(
                keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false),
                textInputAction: TextInputAction.done,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                controller: amountController,
                onChanged: (text) {
                  amountController.text != null
                      ? amountController.text
                      : amountController.text = "2000";
                  setState(() {});
                },
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                  color: adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textlightgrey
                      : white,
                  fontFamily: 'WorkSans',
                  fontWeight: FontWeight.w500,
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                            ? white
                            : textdarkblue,
                      )),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                            ? white
                            : textdarkblue,
                      )),
                  hintStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                      fontFamily: 'WorkSans',
                      fontWeight: FontWeight.w500,
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? darktheme
                          : white),
                  // hintText:"2000",
                  prefixIcon: Padding(
                    padding: EdgeInsets.only(left: width*0.27,right: 10),
                    child: Text(CURRENCYRUPESS,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w500,
                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                              ? darktheme
                              : white),),
                  ),
                  hoverColor: adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textdarkblue
                      : white,
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 50.0, right: 50, top: height * 0.01, bottom: height * 0.01),
            child: Divider(
              thickness: 1,
              color: adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textgrey
                  : darktheme,
            ),
          ),
          Center(
            child: textWidget(
                'you are not able to change the amount',
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w400,
                adaptiveThemeMode == AdaptiveThemeMode.light
                    ? textgreylight
                    : white,
                TextAlign.start),
          ),
          Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: height * 0.04),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      textWidget(
                          'Balance to coins',
                          ScreenUtil().setSp(37, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : white,
                          TextAlign.start),
                      textWidget(
                          'Weightage(%)',
                          ScreenUtil().setSp(37, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : white,
                          TextAlign.start),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Get.to(TokenUi());
                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10.0, right: 8, bottom: 8, left: 8),
                        child: SvgPicture.asset('assets/ethereum_icon.svg'),
                      ),
                      Expanded(
                        child: textWidget(
                            'Ethereum (ETH)',
                            ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            FontWeight.w400,
                            adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white,
                            TextAlign.start),
                      ),
                      textWidget(
                          '90%',
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : white,
                          TextAlign.start),
                    ],
                  ),
                ),
                Divider(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textgrey
                      : darktheme,
                ),
                GestureDetector(
                  onTap: (){
                    Get.to(TokenUi());

                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SvgPicture.asset('assets/bitcoin_icon.svg'),
                      ),
                      Expanded(
                        child: textWidget(
                            'Bit Coin (BTC)',
                            ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            FontWeight.w400,
                            adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white,
                            TextAlign.start),
                      ),
                      textWidget(
                          '65%',
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : white,
                          TextAlign.start),
                    ],
                  ),
                ),
                Divider(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textgrey
                      : darktheme,
                ),
                GestureDetector(
                  onTap: (){
                    Get.to(TokenUi());

                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SvgPicture.asset('assets/cardano_icon.svg'),
                      ),
                      Expanded(
                        child: textWidget(
                            'Cardano (ADA)',
                            ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            FontWeight.w400,
                            adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white,
                            TextAlign.start),
                      ),
                      textWidget(
                          '45%',
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : white,
                          TextAlign.start),
                    ],
                  ),
                ),
                Divider(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textgrey
                      : darktheme,
                ),
                GestureDetector(
                  onTap: (){
                    Get.to(TokenUi());
                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SvgPicture.asset('assets/polkadot_icon.svg'),
                      ),
                      Expanded(
                        child: textWidget(
                            'Polkadot (DOT)',
                            ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            FontWeight.w400,
                            adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white,
                            TextAlign.start),
                      ),
                      textWidget(
                          '50%',
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : white,
                          TextAlign.start),
                    ],
                  ),
                ),
                Divider(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textgrey
                      : darktheme,
                ),
                GestureDetector(
                  onTap: (){
                    Get.to(TokenUi());

                  },
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SvgPicture.asset('assets/binance_icon.svg'),
                      ),
                      Expanded(
                        child: textWidget(
                            'Binance Coin (BNB)',
                            ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            FontWeight.w400,
                            adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white,
                            TextAlign.start),
                      ),
                      textWidget(
                          '05%',
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : white,
                          TextAlign.start),
                    ],
                  ),
                ),
                Divider(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textgrey
                      : darktheme,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
