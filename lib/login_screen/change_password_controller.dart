import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/login_screen/login_ui.dart';
import 'package:cryptocase/login_screen/reset_password_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../api_client.dart';

class ChangePasswordController extends GetxController {
  var changepasswordResponse = ChangePasswordModel().obs;

  fetchChangePasswordResponse(String email, String otp, String password) async {
    Get.dialog(CupertinoActivityIndicator());
    var responseResult = ChangePasswordModel();
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response = await ApiClient().getChangePasswordResponse(email, otp, password);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = ChangePasswordModel.fromJson(jsonMap);
      if (response.statusCode == 200 || response.statusCode == 201) {
        Get.offAll(LoginUi());
        greenSnack("Password Changed Successfully");
      } else {
        Get.back();
        redSnack("Failed");
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
  }
}
