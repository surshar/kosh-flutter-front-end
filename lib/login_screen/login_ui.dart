import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:cryptocase/home_screen/home_ui.dart';
import 'package:cryptocase/login_screen/change_password_controller.dart';
import 'package:cryptocase/login_screen/reset_password_controller.dart';
import 'package:cryptocase/registration_screen/signup_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_controller.dart';

class LoginUi extends StatefulWidget {
  @override
  _LoginUiState createState() => _LoginUiState();
}

class _LoginUiState extends State<LoginUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  bool _passwordVisible = false;
  bool _isObscure1 = false;
  bool _isObscure2 = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneNumber = TextEditingController();
  TextEditingController _newPasswordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();

  final logincontroller = Get.put(LoginController());
  final resetPassController = Get.put(ResetPasswordController());
  final changePassController = Get.put(ChangePasswordController());
  var _otp, _email;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
    logincontroller.dispose();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: ()=> FocusScope.of(context).unfocus(),
      child: Scaffold(
        // resizeToAvoidBottomInset: false,
        backgroundColor:
            adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                appBar(
                    false,
                    height * 0.1,
                    width,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : white,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textgrey
                        : white),
                Container(
                    margin: EdgeInsets.only(left: 0, top: height * 0.01),
                    height: height * 0.75,
                    child: buildMainScreen(context, width, height)),
                Container(
                  height: height * 0.06,
                  child: GestureDetector(
                    onTap: () {
                      if (emailController.text.trim().isEmpty) {
                        redSnack("Email field is empty");
                      } else if (passwordController.text.trim().isEmpty) {
                        redSnack("Password field is empty");
                      } else if (passwordController.text.trim().length != 8) {
                        redSnack("Invalid Password");
                      } else {
                        String email = emailController.text.trim();
                        String password = passwordController.text.trim();
                        logincontroller.fetchLoginResponse(email, password);
                      }
                    },
                    child: loginBottomButton(
                        adaptiveThemeMode == AdaptiveThemeMode.light
                            ? buttongreen
                            : buttongreen,
                        adaptiveThemeMode == AdaptiveThemeMode.light
                            ? white
                            : white,
                        "Login"),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  buildMainScreen(BuildContext context, double width, double height) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          textWidget(
              LOGINTITLE,
              ScreenUtil().setSp(70, allowFontScalingSelf: true),
              FontWeight.w600,
              adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textdarkblue
                  : white,
              TextAlign.start),
          Container(
            height: height * 0.08,
            margin: EdgeInsets.only(
                right: 5, top: height * 0.02, bottom: height * 0.02),
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              controller: emailController,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                color: adaptiveThemeMode == AdaptiveThemeMode.light
                    ? textlightgrey
                    : white,
                fontFamily: 'WorkSans',
                fontWeight: FontWeight.w400,
              ),
              cursorColor: adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textgrey
                  : white,
              decoration: InputDecoration(
                fillColor: adaptiveThemeMode == AdaptiveThemeMode.light
                    ? white
                    : darktheme,
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textborder
                          : white,
                    )),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textborder
                          : white,
                    )),
                hintStyle: TextStyle(
                    fontSize:
                        ScreenUtil().setSp(35, allowFontScalingSelf: true),
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w400,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light
                        ? darktheme
                        : white),
                hintText: 'Email Address',
                hoverColor: adaptiveThemeMode == AdaptiveThemeMode.light
                    ? textdarkblue
                    : white,
                contentPadding: EdgeInsets.all(20.0),
              ),
            ),
          ),
          Container(
            height: height * 0.08,
            margin: EdgeInsets.only(right: 5, top: height * 0.01),
            child: TextFormField(
              controller: passwordController,
              obscureText: !_passwordVisible,
              validator: (value) {
                return value.length < 8 ? 'Invalid Password' : null;
              },
              style: TextStyle(
                fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                color: adaptiveThemeMode == AdaptiveThemeMode.light
                    ? textlightgrey
                    : white,
                fontFamily: 'WorkSans',
                fontWeight: FontWeight.w400,
              ),
              cursorColor: adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textgrey
                  : white,
              decoration: InputDecoration(
                fillColor: adaptiveThemeMode == AdaptiveThemeMode.light
                    ? white
                    : darktheme,
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textborder
                          : white,
                    )),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textborder
                          : white,
                    )),
                hintStyle: TextStyle(
                    fontSize:
                        ScreenUtil().setSp(35, allowFontScalingSelf: true),
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w400,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light
                        ? darktheme
                        : white),
                hintText: 'Password',
                suffixIcon: IconButton(
                    icon: Icon(
                      // Based on passwordVisible state choose the icon
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: textborder,
                    ),
                    onPressed: () {
                      // Update the state i.e. toggle the state of passwordVisible variable
                      setState(() {
                        _passwordVisible = !_passwordVisible;
                      });
                    }),
                hoverColor: adaptiveThemeMode == AdaptiveThemeMode.light
                    ? textdarkblue
                    : white,
                contentPadding: EdgeInsets.all(20.0),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              buildForgetPasswordScreen(context);
            },
            child: Container(
              margin: EdgeInsets.only(top: height * 0.02, right: width * 0.02),
              alignment: Alignment.bottomRight,
              child: textWidget(
                  FORGOTPASSWORD,
                  ScreenUtil().setSp(40, allowFontScalingSelf: true),
                  FontWeight.w600,
                  adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textblue
                      : textgreen,
                  TextAlign.end),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: height * 0.05, bottom: height * 0.02),
            child: Center(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  textWidget(
                      DONTHAVEACCOUNT,
                      ScreenUtil().setSp(40, allowFontScalingSelf: true),
                      FontWeight.w400,
                      adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textgrey
                          : white,
                      TextAlign.end),
                  SizedBox(width: width * 0.01),
                  GestureDetector(
                    onTap: () {
                      Get.offAll(SignupUi());
                    },
                    child: textWidget(
                        SIGNUP,
                        ScreenUtil().setSp(40, allowFontScalingSelf: true),
                        FontWeight.w600,
                        adaptiveThemeMode == AdaptiveThemeMode.light
                            ? textblue
                            : textgreen,
                        TextAlign.end),
                  )
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Get.offAll(HomeScreen());
            },
            child: Center(
              child: textWidget(
                  CONTINUE,
                  ScreenUtil().setSp(40, allowFontScalingSelf: true),
                  FontWeight.w600,
                  adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textblue
                      : textgreen,
                  TextAlign.end),
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: height * 0.02, bottom: height * 0.02),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: Divider(
                    color: adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textgrey
                        : white,
                  ),
                ),
                SizedBox(
                  width: width * 0.02,
                ),
                textWidget(
                    'OR SIGN IN WITH',
                    ScreenUtil().setSp(50, allowFontScalingSelf: true),
                    FontWeight.w600,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : white,
                    TextAlign.end),
                SizedBox(
                  width: width * 0.02,
                ),
                Expanded(
                  child: Divider(
                    color: adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textgrey
                        : white,
                  ),
                ),
              ],
            ),
          ),
          Container(
              height: height * 0.08,
              width: width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Column(
                        children: [
                          SvgPicture.asset(
                            'assets/google_logo.svg',
                            height: 30,
                            width: 30,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: textWidget(
                                "Google",
                                ScreenUtil()
                                    .setSp(35, allowFontScalingSelf: true),
                                FontWeight.w500,
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textdarkblue
                                    : white,
                                TextAlign.end),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Column(
                        children: [
                          SvgPicture.asset(
                            'assets/facebook_logo.svg',
                            height: 35,
                            width: 35,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: textWidget(
                                "Facebook",
                                ScreenUtil()
                                    .setSp(35, allowFontScalingSelf: true),
                                FontWeight.w500,
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textdarkblue
                                    : white,
                                TextAlign.end),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }

  buildForgetPasswordScreen(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    TextEditingController phoneNumberController = TextEditingController();
    showModalBottomSheet(
        backgroundColor:
            adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          TextEditingController phoneController = TextEditingController();
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: StatefulBuilder(
                builder: (BuildContext context, StateSetter stateSetter) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: height * 0.02),
                          height: height * 0.01,
                          width: width * 0.2,
                          decoration: BoxDecoration(
                            color: dividercolor,
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.03),
                      Padding(
                        padding: EdgeInsets.only(left: width * 0.06),
                        child: textWidget(
                            FORGOTPASSWORD,
                            ScreenUtil().setSp(50, allowFontScalingSelf: true),
                            FontWeight.w600,
                            adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white,
                            TextAlign.end),
                      ),
                      SizedBox(height: height * 0.02),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: Text(
                          ENTERMOBILENUMBER,
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textgrey
                                      : white,
                              fontSize: ScreenUtil()
                                  .setSp(30, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: Text(
                          'Email Address',
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textgrey
                                      : white,
                              fontSize: ScreenUtil()
                                  .setSp(40, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(height: 10),
                      /*Container(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: new TextFormField(
                          controller: phoneNumber,
                          maxLength: 10,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor:
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? white
                                    : darktheme,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? white
                                        : textgrey,
                              ),
                            ),
                            hintStyle: TextStyle(
                              fontSize: ScreenUtil()
                                  .setSp(35, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w400,
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textgrey
                                      : white,
                            ),
                            hintText: '  Phone Number',
                            prefixIcon: Container(
                              decoration: new BoxDecoration(
                                border: Border(
                                  right: BorderSide(
                                      width: 0.5, color: Colors.grey),
                                ),
                              ),
                              height: 45.0,
                              width: 20,
                              margin: const EdgeInsets.all(3.0),
                              //width: 300.0,
                              child: Center(
                                  child: Text(
                                '+91',
                                style: TextStyle(
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.w400,
                                  color: adaptiveThemeMode ==
                                          AdaptiveThemeMode.light
                                      ? darktheme
                                      : white,
                                ),
                              )),
                            ),
                            counterText: '',
                            contentPadding: EdgeInsets.all(20),
                          ),
                        ),
                      ),*/
                      Container(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: TextFormField(
                          controller: phoneNumber,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(
                            fontSize: ScreenUtil()
                                .setSp(35, allowFontScalingSelf: true),
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textlightgrey
                                : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          ),
                          cursorColor:
                              adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? textgrey
                                  : white,
                          decoration: InputDecoration(
                            fillColor:
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? white
                                    : darktheme,
                            filled: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12.0),
                                borderSide: BorderSide(
                                  color: adaptiveThemeMode ==
                                          AdaptiveThemeMode.light
                                      ? textborder
                                      : white,
                                )),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12.0),
                                borderSide: BorderSide(
                                  color: adaptiveThemeMode ==
                                          AdaptiveThemeMode.light
                                      ? textborder
                                      : white,
                                )),
                            hintStyle: TextStyle(
                                fontSize: ScreenUtil()
                                    .setSp(35, allowFontScalingSelf: true),
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.w400,
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? darktheme
                                        : white),
                            hintText: 'Email Address',
                            hoverColor:
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textdarkblue
                                    : white,
                            contentPadding: EdgeInsets.all(20.0),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.04),
                      Container(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: FractionallySizedBox(
                          widthFactor: 1,
                          child: ElevatedButton(
                            onPressed: () {
                              Get.back();
                              // Navigator.of(context).pop();
                              if (phoneNumber.text.trim().isEmpty) {
                                redSnack("Please Enter Email Address");
                              } else {
                                String pnumber = phoneNumber.text.trim();
                                resetPassController
                                    .fetchResetPasswordResponse(pnumber);
                                buildDigitCode(context);
                              }
                              _email = phoneNumber.text.trim();
                            },
                            child: new Text(
                              'Continue',
                              style: TextStyle(
                                  fontSize: ScreenUtil()
                                      .setSp(35, allowFontScalingSelf: true),
                                  color: white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.w600),
                            ),
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(height * 0.025),
                                primary: buttongreen,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.05),
                    ],
                  );
                },
              ),
            ),
          );
        });
  }

  buildDigitCode(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    // AdaptiveThemeMode adaptiveThemeMode;
    showModalBottomSheet(
        backgroundColor:
            adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          TextEditingController phoneController = TextEditingController();
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: StatefulBuilder(
                builder: (BuildContext context, StateSetter stateSetter) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: height * 0.02),
                          height: height * 0.01,
                          width: width * 0.2,
                          decoration: BoxDecoration(
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? dividercolor
                                : white,
                            borderRadius: BorderRadius.circular(7),
                            //shape: BoxShape.rectangle,
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.03),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: textWidget(
                            FOURDIGITCODE,
                            ScreenUtil().setSp(50, allowFontScalingSelf: true),
                            FontWeight.w600,
                            adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white,
                            TextAlign.end),
                      ),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: Text(
                          ENTERFOURDIGIT,
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textgrey
                                      : white,
                              fontSize: ScreenUtil()
                                  .setSp(30, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(
                        height: height * 0.04,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: OtpTextField(
                          numberOfFields: 6,
                          margin: EdgeInsets.only(left: 10),
                          decoration: InputDecoration(
                              filled: true,
                              fillColor:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? dividercolor
                                      : darktheme),
                          borderRadius: BorderRadius.circular(12),
                          enabledBorderColor:
                              adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? dividercolor
                                  : white,
                          cursorColor:
                              adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? textdarkblue
                                  : white,
                          enabled: true,
                          textStyle: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textdarkblue
                                      : white),
                          focusedBorderColor:
                              adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? dividercolor
                                  : white,
                          showFieldAsBox: true,
                          //set to true to show as box or false to show as dash
                          onCodeChanged: (String code) {
                            //handle validation or checks here
                          },
                          onSubmit: (String verificationCode) {
                            _otp = verificationCode;

                            showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: Text("Verification Code"),
                                    content: Text(
                                        'Code entered is $verificationCode'),
                                  );
                                });
                          }, // end onSubmit
                        ),
                      ),
                      SizedBox(
                        height: height * 0.04,
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: FractionallySizedBox(
                          widthFactor: 1,
                          child: ElevatedButton(
                            onPressed: () {
                              Get.back();
                              buildResetPassword(context);
                            },
                            child: new Text(
                              'Continue',
                              style: TextStyle(
                                  fontSize: ScreenUtil()
                                      .setSp(35, allowFontScalingSelf: true),
                                  color: white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.w600),
                            ),
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(height * 0.025),
                                primary: buttongreen,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.05),
                    ],
                  );
                },
              ),
            ),
          );
        });
  }

  buildResetPassword(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    showModalBottomSheet(
        backgroundColor:
            adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          TextEditingController phoneController = TextEditingController();
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: StatefulBuilder(
                builder: (BuildContext context, StateSetter stateSetter) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: height * 0.02),
                          height: height * 0.01,
                          width: width * 0.2,
                          decoration: BoxDecoration(
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? dividercolor
                                : white,
                            borderRadius: BorderRadius.circular(7),
                            //shape: BoxShape.rectangle,
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.03),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: Text(
                          'Reset Password',
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textdarkblue
                                      : white,
                              fontSize: ScreenUtil()
                                  .setSp(50, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: Text(
                          SETNEWPASSWORD,
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textgrey
                                      : white,
                              fontSize: ScreenUtil()
                                  .setSp(30, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(height: height * 0.04),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: Text(
                          'New Password',
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textgrey
                                      : white,
                              fontSize: ScreenUtil()
                                  .setSp(40, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                      Container(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: TextFormField(
                          obscureText: !_isObscure1,
                          controller: _newPasswordController,
                          textInputAction: TextInputAction.done,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textdarkblue
                                      : white),
                          cursorColor:
                              adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? textdarkblue
                                  : white,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor:
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? white
                                    : darktheme,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? textgrey
                                        : white,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? textgrey
                                        : white,
                              ),
                            ),
                            focusColor:
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textgrey
                                    : white,
                            hintStyle: TextStyle(
                              fontSize: ScreenUtil()
                                  .setSp(35, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w400,
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textgrey
                                      : white,
                            ),
                            hintText: 'New Password',
                            contentPadding: EdgeInsets.all(20),
                            suffixIcon: IconButton(
                              icon: Icon(
                                _isObscure1
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? textgrey
                                        : white,
                              ),
                              onPressed: () {
                                stateSetter(() {
                                  _isObscure1 = !_isObscure1;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: Text(
                          'Confirm Password',
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textgrey
                                      : white,
                              fontSize: ScreenUtil()
                                  .setSp(40, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                      Container(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: TextFormField(
                          obscureText: !_isObscure2,
                          controller: _confirmPasswordController,
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textdarkblue
                                      : white),
                          cursorColor:
                              adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? textdarkblue
                                  : white,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor:
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? white
                                    : darktheme,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? textgrey
                                        : white,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: BorderSide(
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? textgrey
                                        : white,
                              ),
                            ),
                            focusColor:
                                adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textgrey
                                    : white,
                            hintStyle: TextStyle(
                                fontSize: ScreenUtil()
                                    .setSp(35, allowFontScalingSelf: true),
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.w400,
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? textgrey
                                        : white),
                            hintText: 'Confirm Password',
                            contentPadding: EdgeInsets.all(20),
                            suffixIcon: IconButton(
                              icon: Icon(
                                _isObscure2
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color:
                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? textgrey
                                        : white,
                              ),
                              onPressed: () {
                                stateSetter(() {
                                  _isObscure2 = !_isObscure2;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.04),
                      Container(
                        padding: EdgeInsets.only(
                            left: width * 0.06, right: width * 0.06),
                        child: FractionallySizedBox(
                          widthFactor: 1,
                          child: ElevatedButton(
                            onPressed: () {
                              if (_newPasswordController.text.trim().isEmpty) {
                                redSnack('New Password Field is Empty');
                              } else if (_confirmPasswordController.text
                                  .trim()
                                  .isEmpty) {
                                redSnack('Confirm Password Field is Empty');
                              } else if (_confirmPasswordController.text !=
                                  _newPasswordController.text) {
                                redSnack(
                                    "Password and confirm password deos not match");
                              } else if (_newPasswordController.text.length <
                                  8) {
                                redSnack(
                                    "Password should be minimum 8 characters");
                              } else {
                                String npass =
                                    _newPasswordController.text.trim();
                                String email = _email.trim();
                                String otp = _otp;

                                changePassController
                                    .fetchChangePasswordResponse(
                                        email, otp, npass);

                                Get.back();
                              }
                            },
                            child: new Text(
                              'Reset Password',
                              style: TextStyle(
                                  fontSize: ScreenUtil()
                                      .setSp(35, allowFontScalingSelf: true),
                                  color: white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.w600),
                            ),
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(20),
                                primary: buttongreen,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.04),
                    ],
                  );
                },
              ),
            ),
          );
        });
  }

  void setUserInformation() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setBool(LOGGED_IN, true);
    Get.offAll(HomeScreen());
  }
}
