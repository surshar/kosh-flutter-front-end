import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:cryptocase/login_screen/login_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../api_client.dart';

class LoginController extends GetxController {
  var loginResponse = LoginModel().obs;

  fetchLoginResponse(String email, String password) async {
    Get.dialog(CupertinoActivityIndicator());
    var responseResult = LoginModel();
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response = await ApiClient().getLoginResponse(email, password);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = LoginModel.fromJson(jsonMap);
      if (response.statusCode == 200 || response.statusCode == 201) {
        print(responseResult);
        Get.offAll(HomeScreen());
        addDataInPrefs(responseResult.refreshToken, responseResult.accessToken);
        greenSnack("Login Successful");
      } else {
        Get.back();
        redSnack("Login Failed");
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
  }

  void addDataInPrefs(String refreshToken, String accessToken) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString(APITOKEN, accessToken);
    await _prefs.setString(REFRESHTOKEN, refreshToken);
    await _prefs.setBool(LOGGED_IN, true);
    await _prefs.setInt(TIMESTAMP, DateTime.now().millisecondsSinceEpoch);
  }
}
