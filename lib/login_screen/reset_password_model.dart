
import 'dart:convert';

ChangePasswordModel resetPasswordModelFromJson(String str) => ChangePasswordModel.fromJson(json.decode(str));

String resetPasswordModelToJson(ChangePasswordModel data) => json.encode(data.toJson());

class ChangePasswordModel {
  ChangePasswordModel({
    this.message,
  });

  String message;

  factory ChangePasswordModel.fromJson(Map<String, dynamic> json) => ChangePasswordModel(
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
  };
}
