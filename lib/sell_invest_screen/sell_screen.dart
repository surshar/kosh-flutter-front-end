import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/login_screen/done_Screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:group_button/group_button.dart';

class SellScreenUi extends StatefulWidget {
  @override
  _SellScreenUiState createState() => _SellScreenUiState();
}

class _SellScreenUiState extends State<SellScreenUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  var text;
  TextEditingController amountController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        resizeToAvoidBottomInset: false,
        body: Container(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20),
                child: appbarinvestsell(
                  height * 0.1,
                  width,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                ),
              ),
              Container(
                  margin: EdgeInsets.only(left: 0, top: height * 0.01),
                  height: height * 0.75,
                  child: buildMainScreen(context, width, height)),
              Expanded(
                child: GestureDetector(
                  onVerticalDragUpdate: (dragUpdateDetails) {
                    Get.to(DoneScreen(), arguments: ('Sell'));
                  },
                  child: Container(
                    color: buttongreen,
                    width: width,
                    height: height * 0.12,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: width * 0.3),
                          child: SvgPicture.asset(
                            'assets/swipe_up.svg',
                            height: height * 0.02,
                            width: width * 0.02,
                            color: white,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 12.0),
                          child: textWidget(
                              'Swipe up to confirm',
                              ScreenUtil().setSp(35, allowFontScalingSelf: true),
                              FontWeight.w500,
                              adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                              TextAlign.start),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  buildMainScreen(BuildContext context, double width, double height) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: width * 0.12,
                            height: width * 0.12,
                            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                color: textblue),
                            child: SvgPicture.asset(
                              'assets/drop_hedge_icon.svg',
                              fit: BoxFit.fill,
                            ),
                          ),
                          Container(
                            width: width * 0.4,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: textWidget(
                                HEDGEBUCKETNAME,
                                ScreenUtil().setSp(45, allowFontScalingSelf: true),
                                FontWeight.w600,
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                                TextAlign.start),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: darktheme,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(12), bottomLeft: Radius.circular(12)),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: textWidget(
                            '₹ 5673.90',
                            ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            FontWeight.w600,
                            white,
                            TextAlign.start),
                      ),
                      textWidget('Bucket Value', ScreenUtil().setSp(23, allowFontScalingSelf: true),
                          FontWeight.w500, white, TextAlign.start),
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: height * 0.02, left: 20),
            child: textWidget(
                CONFIRMSELL,
                ScreenUtil().setSp(50, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                TextAlign.start),
          ),
          Padding(
            padding: EdgeInsets.only(top: height * 0.01, left: 20),
            child: Row(
              children: [
                textWidget(
                    MINSELL,
                    ScreenUtil().setSp(35, allowFontScalingSelf: true),
                    FontWeight.w400,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    TextAlign.start),
                textWidget(
                    LEARNMORE,
                    ScreenUtil().setSp(35, allowFontScalingSelf: true),
                    FontWeight.w400,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : textgreen,
                    TextAlign.start),
              ],
            ),
          ),
          Container(
            width: width,
            margin: EdgeInsets.only(top: 40, right: 20, left: 20),
            child: TextFormField(
              keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false),
              controller: amountController,
              textInputAction: TextInputAction.done,
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
              onChanged: (text) {
                amountController.text != null
                    ? amountController.text
                    : amountController.text = "2000";
                setState(() {});
              },
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                fontFamily: 'WorkSans',
                fontWeight: FontWeight.w500,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
                    )),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
                    )),
                hintStyle: TextStyle(
                    fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w500,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white),
                // hintText:"2000",
                prefixIcon: Padding(
                  padding: EdgeInsets.only(left: width * 0.27, right: 10),
                  child: Text(
                    CURRENCYRUPESS,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                        fontFamily: 'WorkSans',
                        fontWeight: FontWeight.w500,
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white),
                  ),
                ),
                hoverColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 20, left: 20),
            child: Divider(
              height: height * 0.001,
              thickness: 1.5,
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: height * 0.1, right: 20, left: 20),
            child: Center(
              child: GroupButton(
                spacing: 20,
                isRadio: true,
                direction: Axis.horizontal,
                onSelected: (i, isSelected) {
                  FocusScope.of(context).unfocus();
                  getDate(i);
                },
                buttons: [
                  "25%",
                  "50%",
                  "75%",
                  "100%",
                ],
                selectedTextStyle: TextStyle(
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    fontFamily: 'WorkSans'),
                unselectedTextStyle: TextStyle(
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    fontFamily: 'WorkSans'),
                selectedColor:
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : buttongreen,
                unselectedColor:
                    adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
                selectedBorderColor:
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : buttongreen,
                unselectedBorderColor:
                    adaptiveThemeMode == AdaptiveThemeMode.light ? bordercolor : white,
                borderRadius: BorderRadius.circular(7),
                selectedShadow: <BoxShadow>[BoxShadow(color: Colors.transparent)],
                unselectedShadow: <BoxShadow>[BoxShadow(color: Colors.transparent)],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: height * 0.03, left: 28),
            child: textWidget(
                CANCELINSTALLMENT,
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w400,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
        ],
      ),
    );
  }

  void getDate(int i) {
    if (i == 0) {
      amountController.text = "1000";
      setState(() {});
    } else if (i == 1) {
      amountController.text = "2000";
      setState(() {});
    } else if (i == 2) {
      amountController.text = "3000";
      setState(() {});
    } else if (i == 3) {
      amountController.text = "4000";
      setState(() {});
    } else {}
  }
}
