import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:cryptocase/onboarding_screen/onboarding_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeUi extends StatefulWidget {
  @override
  _ThemeUiState createState() => _ThemeUiState();
}

class _ThemeUiState extends State<ThemeUi> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  AdaptiveThemeMode adaptiveThemeMode;
  bool camefrom = Get.arguments;

  String themeMode = "light";

  @override
  void initState() {
    getTheme();
    super.initState();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: height * 0.1),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textWidget(APPNAME, ScreenUtil().setSp(80, allowFontScalingSelf: true),
                      FontWeight.w600, textdarkblue, TextAlign.start)
                  : textWidget(APPNAME, ScreenUtil().setSp(80, allowFontScalingSelf: true),
                      FontWeight.w600, white, TextAlign.start),

              SizedBox(height: height * 0.2),
              Text(
                'Choose a style',
                style: TextStyle(
                    fontSize: width * .06,
                    fontWeight: FontWeight.bold,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white),
              ),
              SizedBox(height: height * 0.03),
              Container(
                width: width * .6,
                child: Text(
                  'Select your theme to customize your interface',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  ),
                ),
              ),
              SizedBox(height: height * 0.1),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                    onTap: () async {
                      print('light clicked');
                      AdaptiveTheme.of(context).setLight();
                      themeMode = "light";
                      getTheme();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(40),
                          ),
                          border: Border.all(
                            width: 1,
                            color:
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            style: BorderStyle.solid,
                          ),
                          color: textdarkblue),
                      padding: EdgeInsets.only(left: 30, top: 10, right: 30, bottom: 10),
                      child: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textWidget("Light", ScreenUtil().setSp(60, allowFontScalingSelf: true),
                              FontWeight.w600, white, TextAlign.start)
                          : textWidget("Light", ScreenUtil().setSp(60, allowFontScalingSelf: true),
                              FontWeight.w600, white, TextAlign.start),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      AdaptiveTheme.of(context).setDark();
                      themeMode = "dark";
                      getTheme();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(40),
                          ),
                          border: Border.all(
                            width: 1,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : buttongreen,
                            style: BorderStyle.solid,
                          ),
                          color:
                              adaptiveThemeMode == AdaptiveThemeMode.light ? white : buttongreen),
                      padding: EdgeInsets.only(left: 30, top: 10, right: 30, bottom: 10),
                      child: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textWidget("Dark", ScreenUtil().setSp(60, allowFontScalingSelf: true),
                              FontWeight.w600, textdarkblue, TextAlign.start)
                          : textWidget("Dark", ScreenUtil().setSp(60, allowFontScalingSelf: true),
                              FontWeight.w600, white, TextAlign.start),
                    ),
                  )
                ],
              ),

              SizedBox(
                height: height * .05,
              ),

              // skip & next
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: height * 0.02, horizontal: width * 0.04),
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () async {
                          SharedPreferences prefs = await SharedPreferences.getInstance();
                          await prefs.setBool('isThemSelected', true);
                          await prefs.setString(THEMEMODE, themeMode);

                          if (camefrom == true) {
                            Get.to(() => HomeScreen());
                          } else {
                            Get.to(() => OnboardingUi());
                          }
                        },
                        shape: CircleBorder(),
                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                            ? buttongreen
                            : buttongreen,
                        child: Padding(
                          padding: EdgeInsets.all(width * 0.05),
                          child: Icon(
                            Icons.arrow_forward,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? const Color(0xFFFFFFFF)
                                : const Color(0xFFFFFFFF),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container buildDot({double width, double height, Color color}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4),
      width: width,
      height: height,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        color: color,
      ),
    );
  }
}
