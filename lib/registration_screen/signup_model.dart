
import 'dart:convert';

SignUpModel signUpModelFromJson(String str) => SignUpModel.fromJson(json.decode(str));

String signUpModelToJson(SignUpModel data) => json.encode(data.toJson());

class SignUpModel {
  SignUpModel({
    this.success,
    this.error,
    this.accessToken,
    this.refreshToken,
  });

  bool success;
  String error;
  String accessToken;
  String refreshToken;

  factory SignUpModel.fromJson(Map<String, dynamic> json) => SignUpModel(
    success: json["success"],
    error: json["error"],
    accessToken: json["accessToken"],
    refreshToken: json["refreshToken"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "error": error,
    "accessToken": accessToken,
    "refreshToken": refreshToken,
  };
}
