import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:cryptocase/registration_screen/signup_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignupUi extends StatefulWidget {
  @override
  _SignupUiState createState() => _SignupUiState();
}

class _SignupUiState extends State<SignupUi> {
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmpasswordController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  final signupcontroller = Get.put(SignUpController());
  bool _isObscure1 = false;
  bool _isObscure2 = false;
  AdaptiveThemeMode adaptiveThemeMode;
  var btnVisible = false;
  var day, month, year;
  bool isEmailVerified = false;
  int cameFrom = Get.arguments;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: height * 0.05),
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                appBarsignup(
                    height * 0.05,
                    width,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    cameFrom == 1 ? "Update Profile" : CREATEACCOUNT),
                Container(
                    margin: EdgeInsets.only(left: 0, top: 10),
                    height: height * 0.77,
                    child: buildMainScreen(context, width, height)),
                Container(
                  margin: EdgeInsets.only(left: 0, right: 0),
                  height: height * 0.07,
                  child: GestureDetector(
                    onTap: () {
                      if (firstName.text.trim().isEmpty) {
                        redSnack("First name field is empty");
                      } else if (lastName.text.trim().isEmpty) {
                        redSnack("Last name field is empty");
                      } else if (emailController.text.trim().isEmpty) {
                        redSnack("Email field is empty");
                      } else if (mobileController.text.trim().isEmpty) {
                        redSnack("Mobile no. field is empty");
                      } else if (passwordController.text.trim().isEmpty) {
                        redSnack("Password field is empty");
                      } else if (confirmpasswordController.text.trim().isEmpty) {
                        redSnack("Confirm Password field is empty");
                      } else if (mobileController.text.length < 10) {
                        redSnack("Invalid mobile number.");
                      } else if (confirmpasswordController.text != passwordController.text) {
                        redSnack("Password and confirm password deos not match");
                      } else if (passwordController.text.length < 8) {
                        redSnack("Password should be minimum 8 characters");
                      } else {
                        String fname = firstName.text.trim();
                        String lname = lastName.text.trim();
                        String email = emailController.text.trim();
                        String mob = mobileController.text.trim();

                        String dob =
                            year.toString() + "-" + month.toString() + "-" + day.toString();
                        String password = passwordController.text.trim();
                        String confirmpassword = confirmpasswordController.text.trim();

                        signupcontroller.fetchSignUpResponse(
                            fname, lname, email, password, confirmpassword, dob, mob);
                      }
                    },
                    child: loginBottomButton(
                        // width * 0.9,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? buttongreen : buttongreen,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                        cameFrom == 1 ? "Update Profile" : "Next"),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  buildMainScreen(BuildContext context, double height, double width) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () {
              AdaptiveTheme.of(context).toggleThemeMode();
              getTheme();
            },
            child: textWidget(
                FILLFORM,
                ScreenUtil().setSp(45, allowFontScalingSelf: true),
                FontWeight.w600,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                TextAlign.start),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: textWidget(
                NAME,
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(right: 5, top: 10),
                child: TextField(
                  controller: firstName,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w400,
                  ),
                  cursorColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  decoration: InputDecoration(
                    fillColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                    filled: true,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.0),
                        borderSide: BorderSide(
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                        )),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.0),
                        borderSide: BorderSide(
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                        )),
                    hintStyle: TextStyle(
                        fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                        fontFamily: 'WorkSans',
                        fontWeight: FontWeight.w400,
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white),
                    hintText: 'Enter First Name',
                    hoverColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: height * 0.04),
                  ),
                ),
              )),
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 5, top: 10),
                child: TextField(
                  controller: lastName,
                  style: TextStyle(
                    fontSize: 14,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w400,
                  ),
                  cursorColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  decoration: InputDecoration(
                      fillColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          borderSide: BorderSide(
                            color:
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                          )),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          borderSide: BorderSide(
                            color:
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                          )),
                      hintStyle: TextStyle(
                          fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w400,
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white),
                      hintText: 'Enter Last Name',
                      hoverColor:
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: height * 0.04)),
                ),
              )),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: textWidget(
                "Mobile No.",
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: new TextFormField(
              maxLength: 10,
              controller: mobileController,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                fontFamily: 'WorkSans',
                fontWeight: FontWeight.w400,
              ),
              cursorColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
              decoration: InputDecoration(
                  fillColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                  filled: true,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                      )),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                      )),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                      )),
                  hintStyle: TextStyle(
                    fontFamily: 'WorkSans',
                    fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                    fontWeight: FontWeight.w400,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white,
                  ),
                  hintText: '  Enter your Mobile No.',
                  counterText: '',
                  prefixIcon: Container(
                    decoration: new BoxDecoration(
                      border: Border(
                        right: BorderSide(width: 0.5, color: Colors.grey),
                      ),
                    ),
                    height: 40.0,
                    width: 20,
                    margin: const EdgeInsets.all(5.0),
                    //width: 300.0,
                    child: Center(
                        child: Text(
                      '+91',
                      style: TextStyle(
                        fontFamily: 'WorkSans',
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white,
                      ),
                    )),
                  ),
                  contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: height * 0.04)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: textWidget(
                "E-Mail Id",
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: TextField(
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                fontFamily: 'WorkSans',
                fontWeight: FontWeight.w400,
              ),
              cursorColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              decoration: InputDecoration(
                  fillColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                  filled: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                      )),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide(
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                      )),
                  hintStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                      fontFamily: 'WorkSans',
                      fontWeight: FontWeight.w400,
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white),
                  hintText: 'Enter your mail id',
                  contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: height * 0.04)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: textWidget(
                "Date of Birth",
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 0),
                    height: 50,
                    width: width,
                    decoration: BoxDecoration(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      border: Border.all(
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                          width: 1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DropdownButton<String>(
                        isExpanded: true,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : textgrey,
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w500,
                        ),
                        items: SELECTDAY.map((value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        value: day,
                        underline: SizedBox.shrink(),
                        hint: Text(
                          'Date',
                          style: (TextStyle(
                            fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                            color:
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                        onChanged: (value) {
                          setState(() {
                            day = value;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 50,
                    width: width,
                    decoration: BoxDecoration(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      border: Border.all(
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                          width: 1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DropdownButton<String>(
                        isExpanded: true,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : textgrey,
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w500,
                        ),
                        items: SELECTMONTHS.map((value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        value: month,
                        underline: SizedBox.shrink(),
                        hint: Text(
                          'Month',
                          style: (TextStyle(
                            fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                            color:
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                        onChanged: (value) {
                          setState(() {
                            month = value;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 50,
                    width: width,
                    decoration: BoxDecoration(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      border: Border.all(
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                          width: 1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DropdownButton<String>(
                        isExpanded: true,
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : textgrey,
                          fontFamily: 'WorkSans',
                          fontWeight: FontWeight.w500,
                        ),
                        items: SELECTYEAR.map((value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        value: year,
                        underline: SizedBox.shrink(),
                        hint: Text(
                          'Year',
                          style: (TextStyle(
                            fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                            color:
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                        onChanged: (value) {
                          setState(() {
                            year = value;
                          });
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              'Password',
              style: TextStyle(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                  fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                  fontFamily: 'WorkSans',
                  fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(height: height * 0.02),
          Container(
            padding: EdgeInsets.all(0),
            child: TextFormField(
              controller: passwordController,
              obscureText: !_isObscure1,
              textInputAction: TextInputAction.done,
              cursorColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              decoration: InputDecoration(
                filled: true,
                fillColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                    )),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                    )),
                hintStyle: TextStyle(
                    fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w400,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white),
                hintText: 'Password',
                contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: height * 0.04),
                suffixIcon: IconButton(
                  icon: Icon(
                    _isObscure1 ? Icons.visibility : Icons.visibility_off,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                  ),
                  onPressed: () {
                    setState(() {
                      _isObscure1 = !_isObscure1;
                    });
                  },
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              'Confirm Password',
              style: TextStyle(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                  fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                  fontFamily: 'WorkSans',
                  fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(height: height * 0.02),
          Container(
            padding: EdgeInsets.all(0),
            child: TextFormField(
              controller: confirmpasswordController,
              textInputAction: TextInputAction.done,
              obscureText: !_isObscure2,
              cursorColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              decoration: InputDecoration(
                filled: true,
                fillColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                    )),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                    )),
                hintStyle: TextStyle(
                    fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w400,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white),
                hintText: 'Confirm Password',
                contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: height * 0.04),
                suffixIcon: IconButton(
                  icon: Icon(_isObscure2 ? Icons.visibility : Icons.visibility_off,
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white),
                  onPressed: () {
                    setState(() {
                      _isObscure2 = !_isObscure2;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  buildDigitCode(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    // AdaptiveThemeMode adaptiveThemeMode;
    showModalBottomSheet(
        backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          TextEditingController phoneController = TextEditingController();
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: StatefulBuilder(
                builder: (BuildContext context, StateSetter stateSetter) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: height * 0.02),
                          height: height * 0.01,
                          width: width * 0.2,
                          decoration: BoxDecoration(
                            color:
                                adaptiveThemeMode == AdaptiveThemeMode.light ? dividercolor : white,
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.03),
                      Padding(
                        padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                        child: textWidget(
                            FOURDIGITCODE,
                            ScreenUtil().setSp(50, allowFontScalingSelf: true),
                            FontWeight.w600,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            TextAlign.end),
                      ),
                      SizedBox(
                        height: height * 0.02,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                        child: Text(
                          ENTERFOURDIGIT,
                          style: TextStyle(
                              color:
                                  adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                              fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                              fontFamily: 'WorkSans',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(
                        height: height * 0.04,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                        child: OtpTextField(
                          numberOfFields: 6,
                          margin: EdgeInsets.only(left: 10),
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? dividercolor
                                  : darktheme),
                          borderRadius: BorderRadius.circular(12),
                          borderColor: textborder,
                          showFieldAsBox: true,
                          onCodeChanged: (String code) {
                            //handle validation or checks here
                          },
                          onSubmit: (String verificationCode) {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: Text("Verification Code"),
                                    content: Text('Code entered is $verificationCode'),
                                  );
                                });
                          }, // end onSubmit
                        ),
                      ),
                      SizedBox(
                        height: height * 0.04,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                        child: FractionallySizedBox(
                          widthFactor: 1,
                          child: ElevatedButton(
                            onPressed: () {
                              Get.back();
                              isEmailVerified = true;
                            },
                            child: new Text(
                              'Continue',
                              style: TextStyle(
                                  fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                  color: white,
                                  fontFamily: 'WorkSans',
                                  fontWeight: FontWeight.w600),
                            ),
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(height * 0.025),
                                primary: buttongreen,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.05),
                    ],
                  );
                },
              ),
            ),
          );
        });
  }

  void setUserInformation() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setBool(LOGGED_IN, true);
    Get.offAll(HomeScreen());
  }
}
