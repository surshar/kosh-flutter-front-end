import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/registration_screen/signup_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import '../api_client.dart';
import '../kyc_screen/kycdetail_ui.dart';

class SignUpController extends GetxController{
  var signupResponse = SignUpModel().obs;

  fetchSignUpResponse(String firstName, String lastName, String email,
      String password, String confirmPassword, String dob, String mobile_no) async {
    Get.dialog(CupertinoActivityIndicator(
    ));
    var responseResult = SignUpModel();
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
      await ApiClient().getSignUpResponse(firstName,lastName,email,password,confirmPassword, dob, mobile_no);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = SignUpModel.fromJson(jsonMap);
      if (response.statusCode == 201 || response.statusCode == 200) {
        greenSnack("Registration Successful");
        addDataInPrefs(responseResult.refreshToken,responseResult.accessToken);
        Get.offAll(KYCDetailsUI());
      } else if (response.statusCode == 422 ){
        Get.back();
        redSnack(responseResult.error);
      }else {
        Get.back();
        redSnack("Registration Failed");
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
  }

  void addDataInPrefs(String refreshToken,String accessToken) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString(APITOKEN, accessToken);
    await _prefs.setString(REFRESHTOKEN, refreshToken);
    await _prefs.setBool(LOGGED_IN, true);
    await _prefs.setInt(TIMESTAMP, DateTime.now().millisecondsSinceEpoch);
  }

}