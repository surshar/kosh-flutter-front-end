import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/token_screen/token_qraph_model.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../api_client.dart';

class TokenController extends GetxController {
  Future<TokenQraphModel> fetchTokenResponse(int token_id) async {
    var responseResult = TokenQraphModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response = await ApiClient().getTokenResponse(token_id);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);

      if (response.statusCode == 200 || response.statusCode == 201) {
        responseResult = TokenQraphModel.fromJson(jsonMap);
            return responseResult;
      } else {
        return responseResult;
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
      return responseResult;
    }
  }
}
