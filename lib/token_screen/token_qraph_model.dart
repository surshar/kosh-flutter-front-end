
import 'dart:convert';

TokenQraphModel bucketQraphModelFromJson(String str) => TokenQraphModel.fromJson(json.decode(str));

String bucketQraphModelToJson(TokenQraphModel data) => json.encode(data.toJson());

class TokenQraphModel {
  TokenQraphModel({
    this.status,
    this.data,
  });

  String status;
  List<Datum> data;

  factory TokenQraphModel.fromJson(Map<String, dynamic> json) => TokenQraphModel(
    status: json["status"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.bucketId,
    this.currentValue,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int bucketId;
  String currentValue;
  DateTime createdAt;
  DateTime updatedAt;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    bucketId: json["bucket_id"],
    currentValue: json["current_value"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "bucket_id": bucketId,
    "current_value": currentValue,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
  };
}
