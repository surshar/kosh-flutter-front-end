import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/bucket_detail_model.dart';
import 'package:cryptocase/home_screen/bucket_details_controller.dart';
import 'package:cryptocase/token_screen/token_controller.dart';
import 'package:cryptocase/token_screen/token_qraph_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:intl/intl.dart';

class SalesData {
  SalesData(this.year, this.sales);

  final double year;
  final double sales;
}

class TokenUi extends StatefulWidget {
  @override
  _TokenUiState createState() => _TokenUiState();
}

class _TokenUiState extends State<TokenUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  List<SalesData> _linechartData;
  List<Datum> _graphchartData;
  TooltipBehavior _linetooltipBehavior;

  bool isBtnOneselected = true;
  bool isBtnTwoselected = false;
  bool isBtnThreeselected = false;
  bool isBtnFourselected = false;
  bool isBtnFiveselected = false;
  bool isBtnSixselected = false;
  var bucketToken = Get.arguments;
  final buckketController = Get.put(BucketDetailController());
  final tokencontroller = Get.put(TokenController());

  @override
  void initState() {
    super.initState();
    getTheme();
    getBucketData();
    _linechartData = getLineChartData();
    _linetooltipBehavior = TooltipBehavior(enable: true);
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  Future<BucketDetailModel> getBucketData() async {
    return await buckketController.fetchBucketDetailResponse(bucketToken.id);
  }

  Future<List<Datum>> getTokenGraph() async {
    var token = await tokencontroller.fetchTokenResponse(bucketToken.tokenId);
    return token.data;
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
      body: FutureBuilder<BucketDetailModel>(
          future: getBucketData(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container(
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                child: Center(
                  child: CupertinoActivityIndicator(),
                ),
              );
            } else {
              return Container(
                padding: EdgeInsets.all(20),
                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: Column(
                    children: [
                      appBarsignup(
                          height * 0.1,
                          width,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                          bucketToken.token.tokenName + " " + bucketToken.token.tokenAbbr),
                      Expanded(
                        child: ListView(
                          children: [
                            Container(
                              width: width,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    width: width * 0.18,
                                    height: width * 0.18,
                                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        color: textblue),
                                    child: SvgPicture.asset(
                                      'assets/drop_hedge_icon.svg',
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Expanded(
                                      child: Container(
                                    margin: EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        textWidget(
                                            'Current price',
                                            ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                            FontWeight.w500,
                                            adaptiveThemeMode == AdaptiveThemeMode.light
                                                ? textdarkblue
                                                : white,
                                            TextAlign.start),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 8.0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              textWidget(
                                                  //"₹ 60,500",
                                                  bucketToken.token.buyPrice.toStringAsFixed(2),
                                                  ScreenUtil()
                                                      .setSp(65, allowFontScalingSelf: true),
                                                  FontWeight.w800,
                                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                                      ? textdarkblue
                                                      : white,
                                                  TextAlign.start),
                                              textWidget(
                                                  '+5.47%',
                                                  ScreenUtil()
                                                      .setSp(40, allowFontScalingSelf: true),
                                                  FontWeight.w500,
                                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                                      ? textgreen
                                                      : textgreen,
                                                  TextAlign.start),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                                ],
                              ),
                            ),
                            Container(
                                height: height * 0.3,
                                child: FutureBuilder<List<Datum>>(
                                    future: getTokenGraph(),
                                    builder: (context, snapshot) {
                                      if (!snapshot.hasData) {
                                        return Container(
                                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? white
                                              : darktheme,
                                          child: Center(
                                            child: CupertinoActivityIndicator(),
                                          ),
                                        );
                                      } else {
                                        return SfCartesianChart(
                                          legend: Legend(isVisible: false),
                                          tooltipBehavior: _linetooltipBehavior,
                                          margin: EdgeInsets.symmetric(vertical: 10),
                                          palette: [
                                            adaptiveThemeMode == AdaptiveThemeMode.light
                                                ? textdarkblue
                                                : textgreen
                                          ],
                                          series: <ChartSeries>[
                                            SplineSeries<Datum, num>(
                                                dataSource: _graphchartData,
                                                xValueMapper: (Datum sales, _) =>
                                                    sales.createdAt.millisecondsSinceEpoch,
                                                yValueMapper: (Datum sales, _) =>
                                                    double.parse(sales.currentValue),
                                                dataLabelSettings:
                                                    DataLabelSettings(isVisible: false),
                                                enableTooltip: true)
                                          ],
                                          primaryXAxis: NumericAxis(
                                            edgeLabelPlacement: EdgeLabelPlacement.shift,
                                            isVisible: false,
                                          ),
                                          primaryYAxis: NumericAxis(
                                              labelFormat: '{value}M',
                                              isVisible: false,
                                              numberFormat:
                                                  NumberFormat.simpleCurrency(decimalDigits: 0)),
                                        );
                                      }
                                    })),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(7)),
                                  color: adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? greybackground
                                      : backgroundlightblue),
                              height: height * 0.05,
                              margin: EdgeInsets.only(bottom: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        isBtnOneselected = true;
                                        isBtnTwoselected = false;
                                        isBtnThreeselected = false;
                                        isBtnFourselected = false;
                                        isBtnFiveselected = false;
                                        isBtnSixselected = false;
                                        _linechartData = getSecondLineChartData();
                                        setState(() {});
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(right: 5),
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(7)),
                                            color: isBtnOneselected
                                                ? white
                                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                                    ? greybackground
                                                    : backgroundlightblue),
                                        child: Center(
                                          child: textWidget(
                                              "1h",
                                              ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                              FontWeight.w500,
                                              isBtnOneselected
                                                  ? textdarkblue
                                                  : adaptiveThemeMode == AdaptiveThemeMode.light
                                                      ? textgrey
                                                      : white,
                                              TextAlign.start),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () async {
                                        isBtnOneselected = false;
                                        isBtnTwoselected = true;
                                        isBtnThreeselected = false;
                                        isBtnFourselected = false;
                                        isBtnFiveselected = false;
                                        isBtnSixselected = false;
                                        _graphchartData = await getTokenGraph();
                                        setState(() {});
                                      },
                                      child: Container(
                                        margin: EdgeInsets.symmetric(horizontal: 5),
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(7)),
                                            color: isBtnTwoselected
                                                ? white
                                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                                    ? greybackground
                                                    : backgroundlightblue),
                                        child: Center(
                                          child: textWidget(
                                              "24h",
                                              ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                              FontWeight.w500,
                                              isBtnTwoselected
                                                  ? textdarkblue
                                                  : adaptiveThemeMode == AdaptiveThemeMode.light
                                                      ? textgrey
                                                      : white,
                                              TextAlign.start),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        isBtnOneselected = false;
                                        isBtnTwoselected = false;
                                        isBtnThreeselected = true;
                                        isBtnFourselected = false;
                                        isBtnFiveselected = false;
                                        isBtnSixselected = false;
                                        _linechartData = getSecondLineChartData();
                                        setState(() {});
                                      },
                                      child: Container(
                                        margin: EdgeInsets.symmetric(horizontal: 5),
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(7)),
                                            color: isBtnThreeselected
                                                ? white
                                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                                    ? greybackground
                                                    : backgroundlightblue),
                                        child: Center(
                                          child: textWidget(
                                              "7d",
                                              ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                              FontWeight.w500,
                                              isBtnThreeselected
                                                  ? textdarkblue
                                                  : adaptiveThemeMode == AdaptiveThemeMode.light
                                                      ? textgrey
                                                      : white,
                                              TextAlign.start),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        isBtnOneselected = false;
                                        isBtnTwoselected = false;
                                        isBtnThreeselected = false;
                                        isBtnFourselected = true;
                                        isBtnFiveselected = false;
                                        isBtnSixselected = false;
                                        _linechartData = getSecondLineChartData();

                                        setState(() {});
                                      },
                                      child: Container(
                                        margin: EdgeInsets.symmetric(horizontal: 5),
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(7)),
                                            color: isBtnFourselected
                                                ? white
                                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                                    ? greybackground
                                                    : backgroundlightblue),
                                        child: Center(
                                          child: textWidget(
                                              "30d",
                                              ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                              FontWeight.w500,
                                              isBtnFourselected
                                                  ? textdarkblue
                                                  : adaptiveThemeMode == AdaptiveThemeMode.light
                                                      ? textgrey
                                                      : white,
                                              TextAlign.start),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        isBtnOneselected = false;
                                        isBtnTwoselected = false;
                                        isBtnThreeselected = false;
                                        isBtnFourselected = false;
                                        isBtnFiveselected = true;
                                        isBtnSixselected = false;
                                        _linechartData = getSecondLineChartData();
                                        setState(() {});
                                      },
                                      child: Container(
                                        margin: EdgeInsets.symmetric(horizontal: 5),
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(7)),
                                            color: isBtnFiveselected
                                                ? white
                                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                                    ? greybackground
                                                    : backgroundlightblue),
                                        child: Center(
                                          child: textWidget(
                                              "1y",
                                              ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                              FontWeight.w500,
                                              isBtnFiveselected
                                                  ? textdarkblue
                                                  : adaptiveThemeMode == AdaptiveThemeMode.light
                                                      ? textgrey
                                                      : white,
                                              TextAlign.start),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        isBtnOneselected = false;
                                        isBtnTwoselected = false;
                                        isBtnThreeselected = false;
                                        isBtnFourselected = false;
                                        isBtnFiveselected = false;
                                        isBtnSixselected = true;
                                        _linechartData = getSecondLineChartData();

                                        setState(() {});
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(left: 5),
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(7)),
                                            color: isBtnSixselected
                                                ? white
                                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                                    ? greybackground
                                                    : backgroundlightblue),
                                        child: Center(
                                          child: textWidget(
                                              "All",
                                              ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                              FontWeight.w500,
                                              isBtnSixselected
                                                  ? textdarkblue
                                                  : adaptiveThemeMode == AdaptiveThemeMode.light
                                                      ? textgrey
                                                      : white,
                                              TextAlign.start),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  textWidget(
                                      "About the " +
                                          bucketToken.token.tokenName +
                                          " " +
                                          bucketToken.token.tokenAbbr,
                                      ScreenUtil().setSp(45, allowFontScalingSelf: true),
                                      FontWeight.w500,
                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textdarkblue
                                          : white,
                                      TextAlign.start),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5.0),
                                    child: Html(
                                      data: bucketToken.token.tokenDescription,
                                      defaultTextStyle: TextStyle(
                                        fontSize:
                                            ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                        fontWeight: FontWeight.w400,
                                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                                            ? textgrey
                                            : white,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5.0),
                                    child: Container(
                                      width: width,
                                      child: textWidget(
                                          "Read More",
                                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                          FontWeight.w400,
                                          adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textgrey
                                              : white,
                                          TextAlign.end),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                                    child: Divider(
                                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textgrey
                                          : darktheme,
                                      thickness: 1,
                                    ),
                                  ),
                                  Container(
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: textWidget(
                                                    'Market Cap',
                                                    ScreenUtil()
                                                        .setSp(40, allowFontScalingSelf: true),
                                                    FontWeight.w500,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textdarkblue
                                                        : white,
                                                    TextAlign.start),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20.0),
                                                  child: textWidget(
                                                      'Volume 24h',
                                                      ScreenUtil()
                                                          .setSp(40, allowFontScalingSelf: true),
                                                      FontWeight.w500,
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? textdarkblue
                                                          : white,
                                                      TextAlign.start),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 5.0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: textWidget(
                                                    '₹ 345.168 M',
                                                    ScreenUtil()
                                                        .setSp(32, allowFontScalingSelf: true),
                                                    FontWeight.w600,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textgrey
                                                        : white,
                                                    TextAlign.start),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20.0),
                                                  child: textWidget(
                                                      '₹ 345,1093.5',
                                                      ScreenUtil()
                                                          .setSp(32, allowFontScalingSelf: true),
                                                      FontWeight.w600,
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? textgrey
                                                          : white,
                                                      TextAlign.start),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: textWidget(
                                                    'Circ Supply',
                                                    ScreenUtil()
                                                        .setSp(40, allowFontScalingSelf: true),
                                                    FontWeight.w500,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textdarkblue
                                                        : white,
                                                    TextAlign.start),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20.0),
                                                  child: textWidget(
                                                      'Max Supply',
                                                      ScreenUtil()
                                                          .setSp(40, allowFontScalingSelf: true),
                                                      FontWeight.w500,
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? textdarkblue
                                                          : white,
                                                      TextAlign.start),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 5.0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: textWidget(
                                                    '₹ 2985.168 M',
                                                    ScreenUtil()
                                                        .setSp(32, allowFontScalingSelf: true),
                                                    FontWeight.w600,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textgrey
                                                        : white,
                                                    TextAlign.start),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20.0),
                                                  child: textWidget(
                                                      'N/A',
                                                      ScreenUtil()
                                                          .setSp(32, allowFontScalingSelf: true),
                                                      FontWeight.w600,
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? textgrey
                                                          : white,
                                                      TextAlign.start),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: textWidget(
                                                    'Total Supply',
                                                    ScreenUtil()
                                                        .setSp(40, allowFontScalingSelf: true),
                                                    FontWeight.w500,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textdarkblue
                                                        : white,
                                                    TextAlign.start),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20.0),
                                                  child: textWidget(
                                                      'Rank',
                                                      ScreenUtil()
                                                          .setSp(40, allowFontScalingSelf: true),
                                                      FontWeight.w500,
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? textdarkblue
                                                          : white,
                                                      TextAlign.start),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 5.0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: textWidget(
                                                    '1.000 Bn',
                                                    ScreenUtil()
                                                        .setSp(32, allowFontScalingSelf: true),
                                                    FontWeight.w600,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textgrey
                                                        : white,
                                                    TextAlign.start),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20.0),
                                                  child: textWidget(
                                                      '#120',
                                                      ScreenUtil()
                                                          .setSp(32, allowFontScalingSelf: true),
                                                      FontWeight.w600,
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? textgrey
                                                          : white,
                                                      TextAlign.start),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: textWidget(
                                                    'All Time High',
                                                    ScreenUtil()
                                                        .setSp(40, allowFontScalingSelf: true),
                                                    FontWeight.w500,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textdarkblue
                                                        : white,
                                                    TextAlign.start),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20.0),
                                                  child: textWidget(
                                                      'Mkt Dominance',
                                                      ScreenUtil()
                                                          .setSp(40, allowFontScalingSelf: true),
                                                      FontWeight.w500,
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? textdarkblue
                                                          : white,
                                                      TextAlign.start),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 5.0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                child: textWidget(
                                                    '₹ 6.849',
                                                    ScreenUtil()
                                                        .setSp(32, allowFontScalingSelf: true),
                                                    FontWeight.w600,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textgrey
                                                        : white,
                                                    TextAlign.start),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20.0),
                                                  child: textWidget(
                                                      '9.04%',
                                                      ScreenUtil()
                                                          .setSp(32, allowFontScalingSelf: true),
                                                      FontWeight.w600,
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? textgrey
                                                          : white,
                                                      TextAlign.start),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
          }),
    );
  }

  List<SalesData> getLineChartData() {
    final List<SalesData> chartData = [
      SalesData(2017, 40),
      SalesData(2018, 12),
      SalesData(2019, 50),
      SalesData(2020, 30),
      SalesData(2021, 80)
    ];
    return chartData;
  }

  List<SalesData> getSecondLineChartData() {
    final List<SalesData> chartData = [
      SalesData(2017, 10),
      SalesData(2018, 18),
      SalesData(2019, 80),
      SalesData(2020, 30),
      SalesData(2021, 10)
    ];
    return chartData;
  }
}
