import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/home_screen/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class Withdraw_Done extends StatefulWidget {
  @override
  _DoneScreenState createState() => _DoneScreenState();
}

class _DoneScreenState extends State<Withdraw_Done> {
  String action = Get.arguments;
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: buttongreen,
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: height * 0.12,
            ),
            Container(
                margin: EdgeInsets.only(left: 0, top: height * 0.01),
                height: height * 0.75,
                child: buildMainScreen(context, width, height)),
            Container(
              child: GestureDetector(
                onTap: () {
                  Get.offAll(HomeScreen());
                },
                child: Text(
                  'Back to Main Menu',
                  style: TextStyle(
                      fontSize:
                          ScreenUtil().setSp(48, allowFontScalingSelf: true),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'WorkSans',
                      color: white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  buildMainScreen(BuildContext context, double width, double height) {
    return Container(
      height: height * 0.75,
      child: Column(
        children: [
          SvgPicture.asset('assets/done_icon.svg'),
          Container(
            margin: EdgeInsets.only(top: height * 0.06),
            child: Text(
              'INR withdrawal 2,500',
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(60, allowFontScalingSelf: true),
                  fontWeight: FontWeight.w600,
                  fontFamily: 'WorkSans',
                  color: white),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: height * 0.03),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  'Your withdrawal request is completed\nand should reflect in your bank account\nin few minutes.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize:
                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                      fontWeight: FontWeight.w600,
                      fontFamily: 'WorkSans',
                      color: white),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
