import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/kyc_screen/kycdetail_ui.dart';
import 'package:cryptocase/login_screen/login_ui.dart';
import 'package:cryptocase/registration_screen/signup_ui.dart';
import 'package:cryptocase/profile_screen/trade_history_screen_ui.dart';
import 'package:cryptocase/profile_screen/withdrawn_screen.dart';
import 'package:cryptocase/theme_screen/theme_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'deposite_inr_ui.dart';
import 'logout_controller.dart';

class ProfileScreenUi extends StatefulWidget {
  @override
  _ProfileScreenUiState createState() => _ProfileScreenUiState();
}

class _ProfileScreenUiState extends State<ProfileScreenUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  final logoutController = Get.put(LogoutController());
  bool isLoggedin = false;

  @override
  void initState() {
    super.initState();
    getTheme();
    checkLogin();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  void checkLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    isLoggedin = preferences.getBool(LOGGED_IN) ?? false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
      body: Column(
        children: [
          Stack(children: [
            Container(
              height: height * 0.30,
              padding: EdgeInsets.only(top: height * 0.05),
              width: width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20)),
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [buttongreen, bgdarkgreen1, textdarkblue],
                ),
              ),
              child: Column(
                children: [
                  textWidget(
                      PROFILE,
                      ScreenUtil().setSp(45, allowFontScalingSelf: true),
                      FontWeight.w500,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                      TextAlign.start),
                  SizedBox(height: height * 0.02),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        textWidget(
                            isLoggedin ? ALINAJAMES : 'Guest',
                            ScreenUtil().setSp(55, allowFontScalingSelf: true),
                            FontWeight.w600,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                            TextAlign.start),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              Get.to(SignupUi(), arguments: 1);
                            },
                            child: Visibility(
                              visible: isLoggedin ? true : false,
                              child: Icon(
                                Icons.edit,
                                color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                                size: 20,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Visibility(
                      visible: isLoggedin ? true : false,
                      child: textWidget(
                          KYCVERIFIED,
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                          TextAlign.start),
                    ),
                  ),
                  Visibility(
                    visible: isLoggedin ? true : false,
                    child: textWidget(
                        MOBILENUMBER,
                        ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                        TextAlign.start),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: height * 0.22, left: 20, right: 20),
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 20),
              decoration: BoxDecoration(
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                borderRadius: BorderRadius.all(
                  Radius.circular(12),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    blurRadius: 30.0,
                    spreadRadius: 0.0,
                    offset: const Offset(
                      0.0,
                      4.0,
                    ),
                  )
                ],
              ),
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 18.0),
                  child: textWidget(
                      PAYMENTMETHOD,
                      ScreenUtil().setSp(40, allowFontScalingSelf: true),
                      FontWeight.w500,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                      TextAlign.start),
                ),
                Visibility(
                  visible: isLoggedin ? false : true,
                  child: Center(
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        buildBottomeLoginScreen(context, adaptiveThemeMode);
                      },
                      child: textWidget(
                          'ADD NEW PAYMENT',
                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                          TextAlign.start),
                    ),
                  ),
                ),
                Visibility(
                  visible: isLoggedin ? true : false,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset('assets/bank_icon.svg',
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white),
                      SizedBox(
                        width: width * 0.05,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            textWidget(
                                BANKOFINDIA,
                                ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                FontWeight.w500,
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                                TextAlign.start),
                            textWidget(
                                BANKNUMBER,
                                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                FontWeight.w500,
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                                TextAlign.start),
                          ],
                        ),
                      ),
                      textWidget(
                          '₹ 9,304',
                          ScreenUtil().setSp(44, allowFontScalingSelf: true),
                          FontWeight.w600,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textgreen : textgreen,
                          TextAlign.start),
                    ],
                  ),
                ),
                Visibility(
                  visible: isLoggedin ? true : false,
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            Get.to(Deposite_INR());
                          },
                          child: Container(
                            height: height * 0.05,
                            margin: EdgeInsets.only(top: 12, right: 5),
                            decoration: BoxDecoration(
                              color: adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? white
                                  : textdarkblue,
                              borderRadius: BorderRadius.all(
                                Radius.circular(6),
                              ),
                              border: Border.all(color: textborder),
                            ),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    'assets/plus.svg',
                                    color: adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? textdarkblue
                                        : white,
                                  ),
                                  textWidget(
                                      ADDFUNDS,
                                      ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                      FontWeight.w500,
                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textdarkblue
                                          : white,
                                      TextAlign.start),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            Get.to(WithDrawScreenUi());
                          },
                          child: Container(
                            height: height * 0.05,
                            margin: EdgeInsets.only(top: 12, left: 5),
                            decoration: BoxDecoration(
                                color: adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? white
                                    : textdarkblue,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(6),
                                ),
                                border: Border.all(color: textborder)),
                            child: Center(
                              child: textWidget(
                                  WITHDRAWINR,
                                  ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                  FontWeight.w500,
                                  adaptiveThemeMode == AdaptiveThemeMode.light
                                      ? textdarkblue
                                      : white,
                                  TextAlign.start),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ]),
            ),
          ]),
          Expanded(
            child: Container(
                margin: EdgeInsets.only(left: 0, top: height * 0.01, bottom: 0.01),
                // height: height * 0.40,
                child: buildMainScreen(context, width, height)),
          ),
        ],
      ),
    );
  }

  buildMainScreen(BuildContext context, double width, double height) {
    return Container(
      padding: EdgeInsets.only(left: 30, right: 30, bottom: 10, top: 20),
      child: MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: ListView(
          children: [
            Row(
              children: [
                SvgPicture.asset(
                  'assets/message_question.svg',
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                  width: 20,
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: textWidget(
                      HELP,
                      ScreenUtil().setSp(37, allowFontScalingSelf: true),
                      FontWeight.w500,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                      TextAlign.start),
                )
              ],
            ),
            Visibility(
              visible: isLoggedin ? true : false,
              child: Divider(
                thickness: 1,
                height: height * 0.04,
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                Get.to(KYCDetailsUI());
              },
              child: Visibility(
                visible: isLoggedin ? true : false,
                child: Container(
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/message_question.svg',
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                        width: 20,
                        height: 20,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: textWidget(
                              KYC,
                              ScreenUtil().setSp(37, allowFontScalingSelf: true),
                              FontWeight.w500,
                              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                              TextAlign.start))
                    ],
                  ),
                ),
              ),
            ),
            Divider(
              thickness: 1,
              height: height * 0.04,
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
            ),
            Row(
              children: [
                SvgPicture.asset(
                  'assets/about.svg',
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                  width: 20,
                  height: 20,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: textWidget(
                        ABOUT,
                        ScreenUtil().setSp(37, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.start))
              ],
            ),
            Divider(
              thickness: 1,
              height: height * 0.04,
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
            ),
            Row(
              children: [
                SvgPicture.asset(
                  'assets/lock.svg',
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                  width: 20,
                  height: 20,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: textWidget(
                        PRIVACY,
                        ScreenUtil().setSp(37, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.start))
              ],
            ),
            Visibility(
              visible: isLoggedin ? true : false,
              child: Divider(
                thickness: 1,
                height: height * 0.04,
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                Get.to(TradeHistoryUi());
              },
              child: Visibility(
                visible: isLoggedin ? true : false,
                child: Container(
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/trade_history_icon.svg',
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                        width: 20,
                        height: 20,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: textWidget(
                              TRADEHISTORY,
                              ScreenUtil().setSp(37, allowFontScalingSelf: true),
                              FontWeight.w500,
                              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                              TextAlign.start))
                    ],
                  ),
                ),
              ),
            ),
            Divider(
              thickness: 1,
              height: height * 0.04,
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
            ),
            Row(
              children: [
                SvgPicture.asset(
                  'assets/star.svg',
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                  width: 20,
                  height: 20,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: textWidget(
                        RATESTAR,
                        ScreenUtil().setSp(37, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.start))
              ],
            ),
            Divider(
              thickness: 1,
              height: height * 0.04,
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
            ),
            Row(
              children: [
                SvgPicture.asset(
                  'assets/share.svg',
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                  width: 20,
                  height: 20,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: textWidget(
                        SHARE,
                        ScreenUtil().setSp(37, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.start))
              ],
            ),
            Divider(
              thickness: 1,
              height: height * 0.04,
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                Get.to(ThemeUi(), arguments: true);
              },
              child: Container(
                child: Row(
                  children: [
                    SvgPicture.asset(
                      'assets/theme_icon.svg',
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                      width: 20,
                      height: 20,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: textWidget(
                            THEME,
                            ScreenUtil().setSp(37, allowFontScalingSelf: true),
                            FontWeight.w500,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            TextAlign.start))
                  ],
                ),
              ),
            ),
            Divider(
              thickness: 1,
              height: height * 0.04,
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () async {
                await logoutController.fetchLogoutResponse();
              },
              child: Visibility(
                visible: isLoggedin ? true : false,
                child: Container(
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/logout_icon.svg',
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                        width: 20,
                        height: 20,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: textWidget(
                              LOGOUT,
                              ScreenUtil().setSp(37, allowFontScalingSelf: true),
                              FontWeight.w500,
                              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                              TextAlign.start))
                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () async {
                Get.offAll(() => LoginUi());
              },
              child: Visibility(
                visible: isLoggedin ? false : true,
                child: Container(
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/logout_icon.svg',
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : white,
                        width: 20,
                        height: 20,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: textWidget(
                              LOGIN,
                              ScreenUtil().setSp(37, allowFontScalingSelf: true),
                              FontWeight.w500,
                              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                              TextAlign.start))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
