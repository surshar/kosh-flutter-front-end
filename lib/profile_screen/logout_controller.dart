import 'dart:convert';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:get/get.dart';
import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/login_screen/login_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../api_client.dart';
import 'logout_model.dart';

class LogoutController extends GetxController {
  var loginResponse = LogoutModel().obs;

  fetchLogoutResponse() async {
    Get.dialog(CupertinoActivityIndicator());
    var responseResult = LogoutModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      SharedPreferences preference = await SharedPreferences.getInstance();
      preference.clear();
      Get.back();
      Get.offAll(LoginUi());
      greenSnack("Logout Successful");
      http.Response response = await ApiClient().getLogoutResponse();
      var jsonString = response.body;
      print(jsonString);
      print(response.statusCode);
      var jsonMap = json.decode(jsonString);
      responseResult = LogoutModel.fromJson(jsonMap);
      if (response.statusCode == 200 || response.statusCode == 201) {
      } else {
        Get.back();
        redSnack("Logout Failed");
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
    }
  }
}
