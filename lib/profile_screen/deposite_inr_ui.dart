import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get.dart';

import 'deposite_inr_proceed.dart';

class Deposite_INR extends StatefulWidget {
  @override
  _Deposite_INRState createState() => _Deposite_INRState();
}

class _Deposite_INRState extends State<Deposite_INR> {
  AdaptiveThemeMode adaptiveThemeMode;
  int _radioSelected = 1;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? cardbackground : textdarkblue,
      body: Container(
        padding: EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 20),
        child: Column(
          children: [
            Container(
                child: NewAppBar(
              true,
              height * 0.1,
              width,
              'Deposite INR',
              ScreenUtil().setSp(45, allowFontScalingSelf: true),
              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              IconButton(
                  icon: new Icon(Icons.arrow_back_ios,
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white),
                  onPressed: () {
                    Get.back();
                  }),
              IconButton(
                onPressed: () {
                  alertDailog(context);
                },
                icon: SvgPicture.asset("assets/headphone.svg",
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white),
              ),
            )),
            Container(
              height: height * 0.74,
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  textWidget(
                      'Alina James',
                      ScreenUtil().setSp(70, allowFontScalingSelf: true),
                      FontWeight.w600,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                      TextAlign.center),
                  SizedBox(
                    height: height * 0.03,
                  ),
                  Card(
                    elevation: 0,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Radio(
                            value: 1,
                            groupValue: _radioSelected,
                            activeColor: textgreen,
                            onChanged: (value) {
                              setState(() {
                                _radioSelected = value;
                                // isapplyBtnEnable = true;
                                // _radioVal = 'checkin';
                              });
                            },
                          ),
                          SizedBox(
                            width: width * 0.03,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  textWidget(
                                      'Deposit via',
                                      ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                      FontWeight.w500,
                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textdarkblue
                                          : white,
                                      TextAlign.center),
                                  SizedBox(
                                    width: width * 0.02,
                                  ),
                                  Text(
                                    'RTGS',
                                    style: TextStyle(
                                      fontFamily: 'WorkSans Italic',
                                      fontWeight: FontWeight.w500,
                                      fontSize: ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textgrey
                                          : white,
                                    ),
                                  ),
                                  SizedBox(
                                    width: width * 0.02,
                                  ),
                                  Image.asset(
                                    "assets/imps_logo.png",
                                    width: width * 0.15,
                                  ),
                                  SizedBox(
                                    width: width * 0.02,
                                  ),
                                  textWidget(
                                      'NRFT',
                                      ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                      FontWeight.w500,
                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textgrey
                                          : white,
                                      TextAlign.center),
                                ],
                              ),
                              SizedBox(
                                height: height * 0.01,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  textWidget(
                                      'Upto 4 hours to reflect in your account',
                                      ScreenUtil().setSp(27, allowFontScalingSelf: true),
                                      FontWeight.w500,
                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textgrey
                                          : cardbackground,
                                      TextAlign.center),
                                  SizedBox(
                                    width: width * 0.01,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textblue
                                              : textgreen,
                                        ),
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Padding(
                                      padding: EdgeInsets.all(3),
                                      child: textWidget(
                                          '0% Fees',
                                          ScreenUtil().setSp(27, allowFontScalingSelf: true),
                                          FontWeight.w500,
                                          adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textgrey
                                              : textgrey,
                                          TextAlign.center),
                                    ),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.01,
                  ),
                  Card(
                    elevation: 0,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Radio(
                            value: 2,
                            groupValue: _radioSelected,
                            activeColor: textgreen,
                            onChanged: (value) {
                              setState(() {
                                _radioSelected = value;
                                // isapplyBtnEnable = true;
                                // _radioVal = 'checkin';
                              });
                            },
                          ),
                          SizedBox(
                            width: width * 0.03,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  textWidget(
                                      'Deposit via',
                                      ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                      FontWeight.w500,
                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textdarkblue
                                          : white,
                                      TextAlign.start),
                                  SizedBox(
                                    width: width * 0.04,
                                  ),
                                  SvgPicture.asset(
                                    "assets/paytm_logo.svg",
                                    width: width * 0.08,
                                    color: adaptiveThemeMode == AdaptiveThemeMode.light
                                        ? paytmcolor
                                        : white,
                                  ),
                                  SizedBox(
                                    width: width * 0.04,
                                  ),
                                  SvgPicture.asset("assets/gpay_logo.svg",
                                      width: width * 0.08,
                                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? gpaycolor
                                          : white),
                                  SizedBox(
                                    width: width * 0.04,
                                  ),
                                  SvgPicture.asset("assets/paypal_logo.svg",
                                      width: width * 0.08,
                                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? paypalcolor
                                          : white),
                                  SizedBox(
                                    width: width * 0.03,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: height * 0.012,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  textWidget(
                                      'Upto 4 hours to reflect in your account',
                                      ScreenUtil().setSp(27, allowFontScalingSelf: true),
                                      FontWeight.w500,
                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? textgrey
                                          : cardbackground,
                                      TextAlign.center),
                                  SizedBox(
                                    height: height * 0.02,
                                  ),
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.03,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: width * 0.06, right: width * 0.06),
                    child: OutlineButton(
                      onPressed: () {},
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                      borderSide: BorderSide(
                          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white),
                      child: Padding(
                        padding: EdgeInsets.all(height * 0.01),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "assets/add_button.svg",
                              color: adaptiveThemeMode == AdaptiveThemeMode.light
                                  ? textdarkblue
                                  : white,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Add payment method',
                              textAlign: TextAlign.center,
                              style: (TextStyle(
                                fontSize: ScreenUtil().setSp(30, allowFontScalingSelf: true),
                                color: adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textdarkblue
                                    : white,
                                fontFamily: 'WorkSans',
                                fontWeight: FontWeight.w500,
                              )),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Get.to(Deposite_INR_Proceed(), arguments: _radioSelected);
              },
              child: Container(
                height: height * 0.07,
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.only(left: 10, right: 10),
                child: getButton('Confirm Payment Method'),
              ),
            )
          ],
        ),
      ),
    );
  }

  void alertDailog(
    BuildContext context,
  ) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            child: Container(
                width: width * 0.9,
                height: height * 0.2,
                decoration: BoxDecoration(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    textWidget(
                        "Contact",
                        ScreenUtil().setSp(50, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.center),
                    SizedBox(height: height * 0.02),
                    textWidget(
                        "Yash Sanghavi",
                        ScreenUtil().setSp(50, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.center),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    textWidget(
                        "yash@koshcrypto.app",
                        ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.center),
                  ],
                )),
          );
        });
  }
}
