import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class TradeHistoryUi extends StatefulWidget {
  @override
  __TradeHistoryUiState createState() => __TradeHistoryUiState();
}

class __TradeHistoryUiState extends State<TradeHistoryUi> {
  AdaptiveThemeMode adaptiveThemeMode;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Container(
                child: NewAppBar(
                    false,
                    height * 0.1,
                    width,
                    'Trade History',
                    ScreenUtil().setSp(45, allowFontScalingSelf: true),
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    IconButton(
                        icon: new Icon(Icons.arrow_back_ios,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white),
                        onPressed: () {
                          Get.back();
                        }),
                    IconButton(
                      icon: SvgPicture.asset(
                        "assets/headphone.svg",
                        width: width * 0.05,
                        height: height * 0.025,
                      ),
                    ))),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
                      child: SvgPicture.asset(
                        "assets/green_arrow.svg",
                        width: width * 0.01,
                        height: height * 0.04,
                      ),
                    ),
                    SizedBox(width: width * 0.02),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'You have added ₹4,500 in your\nwallet.',
                          textAlign: TextAlign.justify,
                          style: (TextStyle(
                            fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500,
                          )),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(
                          '12:47 pm',
                          textAlign: TextAlign.center,
                          style: (TextStyle(
                            fontSize: 12,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : cardbackground,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
                      child: SvgPicture.asset(
                        "assets/red_arrow.svg",
                        width: width * 0.01,
                        height: height * 0.04,
                      ),
                    ),
                    SizedBox(width: width * 0.02),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'You have withdrawn ₹4,500 from\nyour wallet.',
                          textAlign: TextAlign.justify,
                          style: (TextStyle(
                            fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                            color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500,
                          )),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(
                          '12:47 pm',
                          textAlign: TextAlign.center,
                          style: (TextStyle(
                            fontSize: 12,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : cardbackground,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
