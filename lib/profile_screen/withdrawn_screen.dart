import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/profile_screen/withdraw_done.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:group_button/group_button.dart';

class WithDrawScreenUi extends StatefulWidget {
  @override
  _WithDrawScreenUiState createState() => _WithDrawScreenUiState();
}

class _WithDrawScreenUiState extends State<WithDrawScreenUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  var text;
  int _radioSelected = 1;
  TextEditingController amountController = TextEditingController();

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        body: Container(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20, right: 30, top: 20),
                child: appbarrebalance(
                    false,
                    height * 0.1,
                    width,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    'Withdraw INR',
                    'assets/share_icon.svg'),
              ),
              Container(
                  margin: EdgeInsets.only(left: 0, top: height * 0.01),
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  height: height * 0.75,
                  child: buildMainScreen(context, width, height)),
              Expanded(
                child: GestureDetector(
                  onVerticalDragUpdate: (dragUpdateDetails) {
                    // showAlertDialog(context);
                    Get.to(Withdraw_Done());
                  },
                  child: Container(
                    color: buttongreen,
                    width: width,
                    height: height * 0.12,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: width * 0.3),
                          child: SvgPicture.asset(
                            'assets/swipe_up.svg',
                            height: height * 0.02,
                            width: width * 0.02,
                            color: white,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 12.0),
                          child: textWidget(
                              'Swipe up to confirm',
                              ScreenUtil().setSp(35, allowFontScalingSelf: true),
                              FontWeight.w500,
                              adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                              TextAlign.start),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  buildMainScreen(BuildContext context, double width, double height) {
    return Container(
      child: Column(children: [
        Container(
          padding: EdgeInsets.all(20),
          width: width * 0.5,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20)),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [buttongreen, bgdarkgreen1, textdarkblue],
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SvgPicture.asset('assets/empty_wallet.svg'),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  textWidget('₹ 5,673', ScreenUtil().setSp(50, allowFontScalingSelf: true),
                      FontWeight.w600, white, TextAlign.start),
                  textWidget(CURRENTBALANCE, ScreenUtil().setSp(35, allowFontScalingSelf: true),
                      FontWeight.w500, white, TextAlign.start),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: height * 0.04),
          child: textWidget(
              ENTERAMOUNT,
              ScreenUtil().setSp(35, allowFontScalingSelf: true),
              FontWeight.w600,
              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              TextAlign.start),
        ),
        Container(
          width: width,
          margin: EdgeInsets.only(top: height * 0.01),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false),
              textInputAction: TextInputAction.done,
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
              onChanged: (text) {
                amountController.text != null
                    ? amountController.text
                    : amountController.text = "2000";
                setState(() {});
              },
              controller: amountController,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? textlightgrey : white,
                fontFamily: 'WorkSans',
                fontWeight: FontWeight.w500,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
                    )),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
                    )),
                hintStyle: TextStyle(
                    fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                    fontFamily: 'WorkSans',
                    fontWeight: FontWeight.w500,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white),
                // hintText: '2000',
                prefixIcon: Padding(
                  padding: EdgeInsets.only(left: width * 0.27, right: 10),
                  child: Text(
                    CURRENCYRUPESS,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(110, allowFontScalingSelf: true),
                        fontFamily: 'WorkSans',
                        fontWeight: FontWeight.w500,
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? darktheme : white),
                  ),
                ),
                hoverColor: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              ),
            ),
          ),
        ),
        Padding(
          padding:
              EdgeInsets.only(left: 50.0, right: 50, top: height * 0.01, bottom: height * 0.01),
          child: Divider(
            color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
          ),
        ),
        textWidget(
            AMOUNTWITHDRAW,
            ScreenUtil().setSp(35, allowFontScalingSelf: true),
            FontWeight.w500,
            adaptiveThemeMode == AdaptiveThemeMode.light ? textred : textred,
            TextAlign.center),
        Padding(
          padding: EdgeInsets.only(left: 0, top: height * 0.04),
          child: GroupButton(
            spacing: 14,
            isRadio: true,
            buttonWidth: width * 0.18,
            direction: Axis.horizontal,
            onSelected: (i, isSelected) {
              getDate(i);
            },
            buttons: [
              "+1,000",
              "+2,000",
              "+3,000",
              "+4,000",
            ],
            selectedTextStyle: TextStyle(
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : white,
                fontWeight: FontWeight.w500,
                fontSize: 14,
                fontFamily: 'WorkSans'),
            unselectedTextStyle: TextStyle(
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                fontWeight: FontWeight.w500,
                fontSize: 14,
                fontFamily: 'WorkSans'),
            selectedColor:
                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : buttongreen,
            unselectedColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
            selectedBorderColor:
                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : buttongreen,
            unselectedBorderColor:
                adaptiveThemeMode == AdaptiveThemeMode.light ? bordercolor : white,
            borderRadius: BorderRadius.circular(7),
            selectedShadow: <BoxShadow>[BoxShadow(color: Colors.transparent)],
            unselectedShadow: <BoxShadow>[BoxShadow(color: Colors.transparent)],
          ),
        ),
        Container(
            margin: EdgeInsets.only(top: height * 0.03),
            height: height * 0.09,
            decoration: BoxDecoration(
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                  bottomLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12)),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  blurRadius: 30.0,
                  spreadRadius: 0.0,
                  offset: const Offset(
                    0.0,
                    4.0,
                  ),
                )
              ],
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Radio(
                  value: 1,
                  groupValue: _radioSelected,
                  activeColor: textgreen,
                  onChanged: (value) {
                    setState(() {
                      _radioSelected = value;
                      // isapplyBtnEnable = true;
                      // _radioVal = 'checkin';
                    });
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: SvgPicture.asset('assets/bank_icon.svg',
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    textWidget(
                        BANKOFINDIA,
                        ScreenUtil().setSp(40, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.start),
                    Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: textWidget(
                          BANKNUMBER,
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                          TextAlign.start),
                    ),
                  ],
                ),
              ],
            )),
        Container(
            margin: EdgeInsets.only(top: height * 0.01),
            height: height * 0.09,
            decoration: BoxDecoration(
              color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                  bottomLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12)),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  blurRadius: 30.0,
                  spreadRadius: 0.0,
                  offset: const Offset(
                    0.0,
                    4.0,
                  ),
                )
              ],
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Radio(
                  value: 2,
                  groupValue: _radioSelected,
                  activeColor: textgreen,
                  onChanged: (value) {
                    setState(() {
                      _radioSelected = value;
                      // isapplyBtnEnable = true;
                      // _radioVal = 'checkin';
                    });
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: SvgPicture.asset('assets/bank_icon.svg',
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    textWidget(
                        BANKOFINDIA,
                        ScreenUtil().setSp(40, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.start),
                    Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: textWidget(
                          BANKNUMBER,
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textborder : white,
                          TextAlign.start),
                    ),
                  ],
                ),
              ],
            )),
        Padding(
          padding: EdgeInsets.only(top: height * 0.01),
          child: textWidget(
              PROCESSTIME,
              ScreenUtil().setSp(35, allowFontScalingSelf: true),
              FontWeight.w500,
              adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
              TextAlign.center),
        ),
      ]),
    );
  }

  void getDate(int i) {
    if (i == 0) {
      amountController.text = "1,000";
      setState(() {});
    } else if (i == 1) {
      amountController.text = "2,000";
      setState(() {});
    } else if (i == 2) {
      amountController.text = "3,000";
      setState(() {});
    } else if (i == 3) {
      amountController.text = "4,000";
      setState(() {});
    } else {}
  }
}
