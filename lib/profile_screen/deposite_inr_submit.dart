import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get.dart';

class Deposite_INR_Submit extends StatefulWidget {
  @override
  _Deposite_INRState createState() => _Deposite_INRState();
}

class _Deposite_INRState extends State<Deposite_INR_Submit> {
  AdaptiveThemeMode adaptiveThemeMode;
  int value = Get.arguments;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        body: Container(
          padding: EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 20),
          child: Column(
            children: [
              Container(
                  child: NewAppBar(
                true,
                height * 0.1,
                width,
                'Deposite INR',
                ScreenUtil().setSp(45, allowFontScalingSelf: true),
                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                IconButton(
                    icon: new Icon(Icons.arrow_back_ios,
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white),
                    onPressed: () {
                      Get.back();
                    }),
                IconButton(
                  onPressed: () {
                    alertDailog(context);
                  },
                  icon: SvgPicture.asset("assets/headphone.svg",
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white),
                ),
              )),
              Container(
                height: height * 0.74,
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Card(
                      elevation: 0,
                      color:
                          adaptiveThemeMode == AdaptiveThemeMode.light ? cardbackground : textgrey,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(15),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * 0.03,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Visibility(
                                  visible: value == 1 ? true : false,
                                  child: Row(
                                    children: [
                                      textWidget(
                                          'Deposit via',
                                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                          FontWeight.w500,
                                          adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textdarkblue
                                              : white,
                                          TextAlign.center),
                                      SizedBox(
                                        width: width * 0.02,
                                      ),
                                      Text(
                                        'RTGS',
                                        style: TextStyle(
                                          fontFamily: 'WorkSans Italic',
                                          fontWeight: FontWeight.w500,
                                          fontSize:
                                              ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textgrey
                                              : white,
                                        ),
                                      ),
                                      SizedBox(
                                        width: width * 0.02,
                                      ),
                                      Image.asset(
                                        "assets/imps_logo.png",
                                        width: width * 0.15,
                                      ),
                                      SizedBox(
                                        width: width * 0.02,
                                      ),
                                      textWidget(
                                          'NRFT',
                                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                          FontWeight.w500,
                                          adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textgrey
                                              : white,
                                          TextAlign.center),
                                    ],
                                  ),
                                ),
                                Visibility(
                                  visible: value == 2 ? true : false,
                                  child: Row(
                                    children: [
                                      textWidget(
                                          'Deposit via',
                                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                                          FontWeight.w500,
                                          adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textdarkblue
                                              : white,
                                          TextAlign.start),
                                      SizedBox(
                                        width: width * 0.04,
                                      ),
                                      SvgPicture.asset(
                                        "assets/paytm_logo.svg",
                                        width: width * 0.08,
                                        color: adaptiveThemeMode == AdaptiveThemeMode.light
                                            ? paytmcolor
                                            : white,
                                      ),
                                      SizedBox(
                                        width: width * 0.04,
                                      ),
                                      SvgPicture.asset("assets/gpay_logo.svg",
                                          width: width * 0.08,
                                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? gpaycolor
                                              : white),
                                      SizedBox(
                                        width: width * 0.04,
                                      ),
                                      SvgPicture.asset("assets/paypal_logo.svg",
                                          width: width * 0.08,
                                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? paypalcolor
                                              : white),
                                      SizedBox(
                                        width: width * 0.03,
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: height * 0.01,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    textWidget(
                                        'Upto 4 hours to reflect in your account',
                                        ScreenUtil().setSp(27, allowFontScalingSelf: true),
                                        FontWeight.w500,
                                        adaptiveThemeMode == AdaptiveThemeMode.light
                                            ? textgrey
                                            : cardbackground,
                                        TextAlign.center),
                                    SizedBox(
                                      width: width * 0.01,
                                    ),
                                    Visibility(
                                      visible: value == 1 ? true : false,
                                      child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              color: adaptiveThemeMode == AdaptiveThemeMode.light
                                                  ? textblue
                                                  : textgreen,
                                            ),
                                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                                ? white
                                                : cardbackground,
                                            borderRadius: BorderRadius.circular(5)),
                                        child: Padding(
                                          padding: EdgeInsets.all(3),
                                          child: textWidget(
                                              '0% Fees',
                                              ScreenUtil().setSp(27, allowFontScalingSelf: true),
                                              FontWeight.w500,
                                              adaptiveThemeMode == AdaptiveThemeMode.light
                                                  ? textgrey
                                                  : textgrey,
                                              TextAlign.center),
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: height * 0.02),
                    textWidget(
                        TRANSACTION,
                        ScreenUtil().setSp(40, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.left),
                    SizedBox(height: height * 0.02),
                    textWidget(
                        TEXTTRANSFERED,
                        ScreenUtil().setSp(40, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.left),
                    SizedBox(height: height * 0.01),
                    DepositeIntTextField(
                      false,
                      TEXTTRANSFERED,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                      height * 0.023,
                    ),
                    SizedBox(height: height * 0.03),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        textWidget(
                            TRANSACTIONID,
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            TextAlign.left),
                        textWidget(
                            FINDTEXT,
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textblue : buttongreen,
                            TextAlign.left),
                      ],
                    ),
                    SizedBox(height: height * 0.01),
                    DepositeIntTextField(
                      false,
                      TRANSACTIONID,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                      height * 0.023,
                    ),
                    SizedBox(
                      height: height * 0.02,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                              ? cardbackground
                              : textgrey),
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.02,
                            right: width * 0.02,
                            top: height * 0.013,
                            bottom: height * 0.013),
                        child: textWidget(
                            BALANCETEXT,
                            ScreenUtil().setSp(30, allowFontScalingSelf: true),
                            FontWeight.w400,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  //Get.to(Deposite_INR_Proceed());
                },
                child: Container(
                  height: height * 0.07,
                  padding: EdgeInsets.only(left: 10, right: 10),
                  alignment: Alignment.bottomCenter,
                  child: getButton('Submit'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void alertDailog(
    BuildContext context,
  ) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            child: Container(
                width: width * 0.9,
                height: height * 0.2,
                decoration: BoxDecoration(
                  color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    textWidget(
                        "Contact",
                        ScreenUtil().setSp(50, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.center),
                    SizedBox(height: height * 0.02),
                    textWidget(
                        "Yash Sanghavi",
                        ScreenUtil().setSp(50, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.center),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    textWidget(
                        "yash@koshcrypto.app",
                        ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        FontWeight.w500,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                        TextAlign.center),
                  ],
                )),
          );
        });
  }
}
