import 'dart:ui';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/home_screen/all_bucket_controller.dart';
import 'package:cryptocase/home_screen/all_bucket_model.dart';
import 'package:cryptocase/home_screen/bucket_details_controller.dart';
import 'package:cryptocase/my_bucket_screen/my_bucket_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class GDPData {
  GDPData(this.continent, this.gdp);

  final String continent;
  final int gdp;
}

class SalesData {
  SalesData(this.year, this.sales);

  final double year;
  final double sales;
}

class HomeUi extends StatefulWidget {
  @override
  _HomeUiState createState() => _HomeUiState();
}

class _HomeUiState extends State<HomeUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  PageController _pageController = new PageController();
  final bucketController = Get.put(AllBucketController());
  final bucketDetailController = Get.put(BucketDetailController());

  List<GDPData> _chartData;
  TooltipBehavior _tooltipBehavior;
  List<SalesData> _linechartData;
  TooltipBehavior _linetooltipBehavior;

  bool isLoggedin = false;

  bool isBtnOneselected = true;
  bool isBtnTwoselected = false;
  bool isBtnThreeselected = false;
  bool isBtnFourselected = false;
  bool isBtnFiveselected = false;
  bool isBtnSixselected = false;

  @override
  void initState() {
    checkLogin();
    super.initState();
    getData();
    getTheme();

    _chartData = getChartData();
    _tooltipBehavior = TooltipBehavior(enable: true);
    _linechartData = getLineChartData();
    _linetooltipBehavior = TooltipBehavior(enable: true);
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  void getData() async {
    //setState(() {});
    var data = await bucketController.fetchAllBucketResponse();
  }

  String greetingMessage(){
    var timeNow = DateTime.now().hour;
    if (timeNow < 12) {
      return 'Good Morning';
    } else if ((timeNow >= 12) && (timeNow <= 16)) {
      return 'Good Afternoon';
    } else {
      return 'Good Evening';
    }
  }

  void checkLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    isLoggedin = preferences.getBool(LOGGED_IN) ?? false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: adaptiveThemeMode == AdaptiveThemeMode.light
          ? backgroundgrey
          : backgroundlightblue,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                  color: adaptiveThemeMode == AdaptiveThemeMode.light
                      ? white
                      : darktheme),
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                    child: appbarwithIcons(
                        "${greetingMessage()} ${isLoggedin ? 'Alina' : 'Guest'}! 👋",
                        "assets/notification.svg",
                        height * 0.1,
                        width,
                        adaptiveThemeMode == AdaptiveThemeMode.light
                            ? textdarkblue
                            : white,
                        adaptiveThemeMode == AdaptiveThemeMode.light
                            ? textgrey
                            : white,
                        true),
                  ),
                  Expanded(
                    child: buildpageview(context, height, width),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                    child: SmoothPageIndicator(
                      controller: _pageController,
                      count: 2,
                      effect: SlideEffect(
                          activeDotColor: buttongreen,
                          dotColor: greyindicator,
                          radius: 16,
                          dotWidth: width * 0.05,
                          dotHeight: height * 0.01),
                      onDotClicked: (index) => _pageController.animateToPage(
                          index,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.bounceInOut),
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 5),
            width: width,
            height: height * 0.3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                textWidget(
                    "Buckets on Fire 🔥",
                    ScreenUtil().setSp(50, allowFontScalingSelf: true),
                    FontWeight.w600,
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : white,
                    TextAlign.start),
                Expanded(
                    child: FutureBuilder<AllBucketModel>(
                        future: AllBucketController().fetchAllBucketResponse(),
                        builder: (context, value) {
                          if (!value.hasData) {
                            return Center(
                              child: Container(
                                  width: width * 0.90,
                                  height: width * 0.45,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                    color: adaptiveThemeMode ==
                                        AdaptiveThemeMode.light
                                        ? white
                                        : darktheme,
                                  ),
                                  child: const CupertinoActivityIndicator()),
                            );
                          } else {
                            return ListView.builder(
                              // controller: _listController.initialIndex.compareTo(value.data.toString()),
                                itemCount: value.data.data == null
                                    ? 0
                                    : value.data.data.length,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return Container(
                                      margin:
                                      EdgeInsets.only(left: 2, right: 9),
                                      height: double.infinity,
                                      width: width * 0.9,
                                      child: getHedgeAgainstInrCard(
                                        index.toString(),
                                        height,
                                        width,
                                        adaptiveThemeMode,
                                        isLoggedin,
                                        context,
                                        index,
                                        datum: value.data.data[index],
                                        onTap: (){
                                          Get.to(() => MyBucketUi());
                                        } ,
                                      ));
                                });
                          }
                          //
                        })),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildpageview(BuildContext context, double height, double width) {
    return PageView(
      controller: _pageController,
      children: [
        buildFirst(context, height, width),
        buildSecond(context, height, width),
        // buildSecond(context),
      ],
    );
  }

  buildFirst(BuildContext context, double height, double width) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Stack(children: [
              Center(
                child: Container(
                  width: width * 0.4,
                  height: width * 0.4,
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      textWidget(
                          "Portfolio Value",
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textgrey
                              : white,
                          TextAlign.start),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: textWidget(
                            "₹ 50,505",
                            ScreenUtil().setSp(70, allowFontScalingSelf: true),
                            FontWeight.w800,
                            adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textdarkblue
                                : white,
                            TextAlign.center),
                      ),
                      textWidget(
                          "+ ₹ 1,021 (5.47%)",
                          ScreenUtil().setSp(35, allowFontScalingSelf: true),
                          FontWeight.w400,
                          adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textgreen
                              : textgreen,
                          TextAlign.start),
                    ],
                  ),
                ),
              ),
              Container(
                child: SfCircularChart(
                  tooltipBehavior: _tooltipBehavior,
                  margin: EdgeInsets.all(0),
                  palette: [chartPurple, chartPink, chartBlue, chartLightPink],
                  series: <CircularSeries>[
                    DoughnutSeries<GDPData, String>(
                      dataSource: _chartData,
                      xValueMapper: (GDPData data, _) => data.continent,
                      yValueMapper: (GDPData data, _) => data.gdp,
                      innerRadius: '70%',
                      dataLabelSettings: DataLabelSettings(isVisible: false),
                      enableTooltip: true,
                    )
                  ],
                ),
              ),
            ]),
          ),
          Center(
            child: textWidget(
                "Age of Portfolio 2.6 Years",
                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                FontWeight.w500,
                adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                TextAlign.start),
          ),
          // SizedBox(
          //   height: 30,
          // )
        ],
      ),
    );
  }

  buildSecond(BuildContext context, double height, double width) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          textWidget(
              "Portfolio Value",
              ScreenUtil().setSp(35, allowFontScalingSelf: true),
              FontWeight.w500,
              adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
              TextAlign.start),
          textWidget(
              "₹ 50,505",
              ScreenUtil().setSp(80, allowFontScalingSelf: true),
              FontWeight.w800,
              adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textdarkblue
                  : white,
              TextAlign.start),
          textWidget(
              "+ ₹ 1,021 (5.47%)",
              ScreenUtil().setSp(45, allowFontScalingSelf: true),
              FontWeight.w400,
              adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textgreen
                  : textgreen,
              TextAlign.start),
          Expanded(
            child: Container(
                child: SfCartesianChart(
                  legend: Legend(isVisible: false),
                  tooltipBehavior: _linetooltipBehavior,
                  margin: EdgeInsets.symmetric(vertical: 10),
                  palette: [
                    adaptiveThemeMode == AdaptiveThemeMode.light
                        ? textdarkblue
                        : textgreen
                  ],
                  series: <ChartSeries>[
                    SplineSeries<SalesData, double>(
                        dataSource: _linechartData,
                        xValueMapper: (SalesData sales, _) => sales.year,
                        yValueMapper: (SalesData sales, _) => sales.sales,
                        dataLabelSettings: DataLabelSettings(isVisible: false),
                        enableTooltip: true)
                  ],
                  primaryXAxis: NumericAxis(
                    edgeLabelPlacement: EdgeLabelPlacement.shift,
                    isVisible: false,
                  ),
                  primaryYAxis: NumericAxis(
                      labelFormat: '{value}M',
                      isVisible: false,
                      numberFormat: NumberFormat.simpleCurrency(decimalDigits: 0)),
                )),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(7)),
                color: adaptiveThemeMode == AdaptiveThemeMode.light
                    ? greybackground
                    : backgroundlightblue),
            height: height * 0.05,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      isBtnOneselected = true;
                      isBtnTwoselected = false;
                      isBtnThreeselected = false;
                      isBtnFourselected = false;
                      isBtnFiveselected = false;
                      isBtnSixselected = false;
                      _linechartData = getLineChartData();

                      setState(() {});
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 5),
                      height: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: isBtnOneselected
                              ? white
                              : adaptiveThemeMode == AdaptiveThemeMode.light
                              ? greybackground
                              : backgroundlightblue),
                      child: Center(
                        child: textWidget(
                            "1h",
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            isBtnOneselected
                                ? textdarkblue
                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            TextAlign.start),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      isBtnOneselected = false;
                      isBtnTwoselected = true;
                      isBtnThreeselected = false;
                      isBtnFourselected = false;
                      isBtnFiveselected = false;
                      isBtnSixselected = false;
                      _linechartData = getSecondLineChartData();
                      setState(() {});
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      height: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: isBtnTwoselected
                              ? white
                              : adaptiveThemeMode == AdaptiveThemeMode.light
                              ? greybackground
                              : backgroundlightblue),
                      child: Center(
                        child: textWidget(
                            "24h",
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            isBtnTwoselected
                                ? textdarkblue
                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            TextAlign.start),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      isBtnOneselected = false;
                      isBtnTwoselected = false;
                      isBtnThreeselected = true;
                      isBtnFourselected = false;
                      isBtnFiveselected = false;
                      isBtnSixselected = false;
                      _linechartData = getLineChartData();

                      setState(() {});
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      height: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: isBtnThreeselected
                              ? white
                              : adaptiveThemeMode == AdaptiveThemeMode.light
                              ? greybackground
                              : backgroundlightblue),
                      child: Center(
                        child: textWidget(
                            "7d",
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            isBtnThreeselected
                                ? textdarkblue
                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            TextAlign.start),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      isBtnOneselected = false;
                      isBtnTwoselected = false;
                      isBtnThreeselected = false;
                      isBtnFourselected = true;
                      isBtnFiveselected = false;
                      isBtnSixselected = false;
                      _linechartData = getSecondLineChartData();

                      setState(() {});
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      height: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: isBtnFourselected
                              ? white
                              : adaptiveThemeMode == AdaptiveThemeMode.light
                              ? greybackground
                              : backgroundlightblue),
                      child: Center(
                        child: textWidget(
                            "30d",
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            isBtnFourselected
                                ? textdarkblue
                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            TextAlign.start),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      isBtnOneselected = false;
                      isBtnTwoselected = false;
                      isBtnThreeselected = false;
                      isBtnFourselected = false;
                      isBtnFiveselected = true;
                      isBtnSixselected = false;
                      _linechartData = getLineChartData();

                      setState(() {});
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5),
                      height: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: isBtnFiveselected
                              ? white
                              : adaptiveThemeMode == AdaptiveThemeMode.light
                              ? greybackground
                              : backgroundlightblue),
                      child: Center(
                        child: textWidget(
                            "1y",
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            isBtnFiveselected
                                ? textdarkblue
                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            TextAlign.start),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      isBtnOneselected = false;
                      isBtnTwoselected = false;
                      isBtnThreeselected = false;
                      isBtnFourselected = false;
                      isBtnFiveselected = false;
                      isBtnSixselected = true;
                      _linechartData = getSecondLineChartData();

                      setState(() {});
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 5),
                      height: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: isBtnSixselected
                              ? white
                              : adaptiveThemeMode == AdaptiveThemeMode.light
                              ? greybackground
                              : backgroundlightblue),
                      child: Center(
                        child: textWidget(
                            "All",
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            isBtnSixselected
                                ? textdarkblue
                                : adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            TextAlign.start),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          // SizedBox(
          //   height: 30,
          // )
        ],
      ),
    );
  }

  List<GDPData> getChartData() {
    final List<GDPData> chartData = [
      GDPData('Bitcoin', 1600),
      GDPData('Dodgecoin', 1500),
      GDPData('Ethereum', 1750),
      GDPData('Litecoin', 1100),
    ];
    return chartData;
  }

  List<SalesData> getLineChartData() {
    final List<SalesData> chartData = [
      SalesData(2017, 40),
      SalesData(2018, 12),
      SalesData(2019, 50),
      SalesData(2020, 30),
      SalesData(2021, 80)
    ];
    return chartData;
  }

  List<SalesData> getSecondLineChartData() {
    final List<SalesData> chartData = [
      SalesData(2017, 10),
      SalesData(2018, 18),
      SalesData(2019, 80),
      SalesData(2020, 30),
      SalesData(2021, 10)
    ];
    return chartData;
  }

  viewBucketData(String index) {
    // Get.to(MyBucketUi());
  }
}
