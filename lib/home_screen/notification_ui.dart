import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get.dart';

class Notification_Ui extends StatefulWidget {
  @override
  _Notification_UiState createState() => _Notification_UiState();
}

class _Notification_UiState extends State<Notification_Ui> {
  AdaptiveThemeMode adaptiveThemeMode;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor:
          adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(right: 40),
              child: NewAppBar(
                  false,
                  height * 0.1,
                  width,
                  'Notification',
                  ScreenUtil().setSp(45, allowFontScalingSelf: true),
                  adaptiveThemeMode == AdaptiveThemeMode.light
                      ? textdarkblue
                      : white,
                  IconButton(
                      icon: new Icon(Icons.arrow_back_ios,
                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                              ? textdarkblue
                              : white),
                      onPressed: () {
                        Get.back();
                      }),
                  IconButton(
                    icon: SvgPicture.asset(
                      "assets/headphone.svg",
                      width: width * 0.05,
                      height: height * 0.025,
                    ),
                  )),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? white
                          : white,
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 15, right: 15, top: 10, bottom: 10),
                        child: SvgPicture.asset(
                          "assets/ethereum_icon.svg",
                          width: width * 0.01,
                          height: height * 0.04,
                        ),
                      ),
                    ),
                    SizedBox(width: width * 0.02),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          ETHEREUMTEXT,
                          textAlign: TextAlign.justify,
                          style: (TextStyle(
                            fontSize: ScreenUtil()
                                .setSp(35, allowFontScalingSelf: true),
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500,
                          )),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(
                          '2 mins ago',
                          textAlign: TextAlign.center,
                          style: (TextStyle(
                            fontSize: 12,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : cardbackground,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: width * 0.1,
                    ),
                    SvgPicture.asset("assets/green_dot.svg",
                        width: width * 0.01, height: height * 0.02)
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textdarkblue
                          : textgrey,
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 10, right: 10, top: 10, bottom: 10),
                        child: SvgPicture.asset(
                          "assets/empty_wallet.svg",
                          width: width * 0.01,
                          height: height * 0.04,
                        ),
                      ),
                    ),
                    SizedBox(width: width * 0.02),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          ADDWALLET,
                          textAlign: TextAlign.justify,
                          style: (TextStyle(
                            fontSize: ScreenUtil()
                                .setSp(35, allowFontScalingSelf: true),
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500,
                          )),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(
                          '12:47 pm',
                          textAlign: TextAlign.center,
                          style: (TextStyle(
                            fontSize: 12,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : cardbackground,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? white
                          : white,
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 15, right: 15, top: 10, bottom: 10),
                        child: SvgPicture.asset(
                          "assets/ethereum_icon.svg",
                          width: width * 0.01,
                          height: height * 0.04,
                        ),
                      ),
                    ),
                    SizedBox(width: width * 0.02),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          ETHEREUMTEXT,
                          textAlign: TextAlign.justify,
                          style: (TextStyle(
                            fontSize: ScreenUtil()
                                .setSp(35, allowFontScalingSelf: true),
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500,
                          )),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(
                          '2 mins ago',
                          textAlign: TextAlign.center,
                          style: (TextStyle(
                            fontSize: 12,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : cardbackground,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      color: adaptiveThemeMode == AdaptiveThemeMode.light
                          ? textdarkblue
                          : textred,
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 10, right: 10, top: 10, bottom: 10),
                        child: SvgPicture.asset(
                          "assets/empty_wallet.svg",
                          width: width * 0.01,
                          height: height * 0.04,
                        ),
                      ),
                    ),
                    SizedBox(width: width * 0.02),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          ADDWALLET,
                          textAlign: TextAlign.justify,
                          style: (TextStyle(
                            fontSize: ScreenUtil()
                                .setSp(35, allowFontScalingSelf: true),
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : white,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w500,
                          )),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(
                          '12:47 pm',
                          textAlign: TextAlign.center,
                          style: (TextStyle(
                            fontSize: 12,
                            color: adaptiveThemeMode == AdaptiveThemeMode.light
                                ? textgrey
                                : cardbackground,
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.w400,
                          )),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
