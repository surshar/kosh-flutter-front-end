import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/bucket_detail_model.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../api_client.dart';

class BucketDetailController extends GetxController {
  var bucketDetailResponse = BucketDetailModel().obs;
  var dataList = List<Data>().obs;
  String id = "1";

  Future<BucketDetailModel> fetchBucketDetailResponse(int bucket_id) async {

    var responseResult = BucketDetailModel();
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response = await ApiClient().getBucketDetailResponse(bucket_id);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = BucketDetailModel.fromJson(jsonMap);
      if (response.statusCode == 200 || response.statusCode == 201) {

        return responseResult;
      } else {
        Get.back();
        return responseResult;
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
      return responseResult;

    }
  }
}
