
import 'dart:convert';

BucketDetailModel bucketDetailModelFromJson(String str) => BucketDetailModel.fromJson(json.decode(str));

String bucketDetailModelToJson(BucketDetailModel data) => json.encode(data.toJson());

class BucketDetailModel {
  BucketDetailModel({
    this.status,
    this.data,
  });

  String status;
  Data data;

  factory BucketDetailModel.fromJson(Map<String, dynamic> json) => BucketDetailModel(
    status: json["status"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.versionNo,
    this.bucketName,
    this.bucketDescription,
    this.exchangeId,
    this.oldFlag,
    this.versions,
    this.createdBy,
    this.lastUpdatedBy,
    this.createdAt,
    this.updatedAt,
    this.bucketTokens,
    this.totalBuyPrice,
    this.totalSellPrice,
  });

  int id;
  String versionNo;
  String bucketName;
  String bucketDescription;
  int exchangeId;
  dynamic oldFlag;
  dynamic versions;
  int createdBy;
  int lastUpdatedBy;
  DateTime createdAt;
  DateTime updatedAt;
  List<BucketToken> bucketTokens;
  double totalBuyPrice;
  double totalSellPrice;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    versionNo: json["version_no"],
    bucketName: json["bucket_name"],
    bucketDescription: json["bucket_description"],
    exchangeId: json["exchange_id"],
    oldFlag: json["old_flag"],
    versions: json["versions"],
    createdBy: json["created_by"],
    lastUpdatedBy: json["last_updated_by"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    bucketTokens: List<BucketToken>.from(json["BucketTokens"].map((x) => BucketToken.fromJson(x))),
    totalBuyPrice: json["total_buy_price"].toDouble(),
    totalSellPrice: json["total_sell_price"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "version_no": versionNo,
    "bucket_name": bucketName,
    "bucket_description": bucketDescription,
    "exchange_id": exchangeId,
    "old_flag": oldFlag,
    "versions": versions,
    "created_by": createdBy,
    "last_updated_by": lastUpdatedBy,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "BucketTokens": List<dynamic>.from(bucketTokens.map((x) => x.toJson())),
    "total_buy_price": totalBuyPrice,
    "total_sell_price": totalSellPrice,
  };
}

class BucketToken {
  BucketToken({
    this.quantity,
    this.id,
    this.bucketId,
    this.tokenId,
    this.createdAt,
    this.updatedAt,
    this.token,
    this.totalBuyPrice,
    this.totalSellPrice,
  });

  double quantity;
  int id;
  int bucketId;
  int tokenId;
  DateTime createdAt;
  DateTime updatedAt;
  Token token;
  double totalBuyPrice;
  double totalSellPrice;

  factory BucketToken.fromJson(Map<String, dynamic> json) => BucketToken(
    quantity: json["quantity"].toDouble(),
    id: json["id"],
    bucketId: json["bucket_id"],
    tokenId: json["token_id"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    token: Token.fromJson(json["Token"]),
    totalBuyPrice: json["total_buy_price"].toDouble(),
    totalSellPrice: json["total_sell_price"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "quantity": quantity,
    "id": id,
    "bucket_id": bucketId,
    "token_id": tokenId,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "Token": token.toJson(),
    "total_buy_price": totalBuyPrice,
    "total_sell_price": totalSellPrice,
  };
}

class Token {
  Token({
    this.id,
    this.tokenName,
    this.tokenAbbr,
    this.baseValueVauld,
    this.tokenDescription,
    this.createdBy,
    this.lastUpdatedBy,
    this.createdAt,
    this.updatedAt,
    this.buyPrice,
    this.sellPrice,
  });

  int id;
  String tokenName;
  String tokenAbbr;
  double baseValueVauld;
  dynamic tokenDescription;
  int createdBy;
  int lastUpdatedBy;
  DateTime createdAt;
  DateTime updatedAt;
  double buyPrice;
  double sellPrice;

  factory Token.fromJson(Map<String, dynamic> json) => Token(
    id: json["id"],
    tokenName: json["token_name"],
    tokenAbbr: json["token_abbr"],
    baseValueVauld: json["base_value_vauld"].toDouble(),
    tokenDescription: json["token_description"],
    createdBy: json["created_by"],
    lastUpdatedBy: json["last_updated_by"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    buyPrice: json["buy_price"].toDouble(),
    sellPrice: json["sell_price"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "token_name": tokenName,
    "token_abbr": tokenAbbr,
    "base_value_vauld": baseValueVauld,
    "token_description": tokenDescription,
    "created_by": createdBy,
    "last_updated_by": lastUpdatedBy,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "buy_price": buyPrice,
    "sell_price": sellPrice,
  };
}
