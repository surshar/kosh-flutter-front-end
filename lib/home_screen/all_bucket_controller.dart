import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/all_bucket_model.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../api_client.dart';

class AllBucketController extends GetxController {
  var allbucketResponse = AllBucketModel().obs;
  var dataList = List<Datum>().obs;
  String id = "1";

  Future<AllBucketModel> fetchAllBucketResponse() async {

    var responseResult = AllBucketModel();
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response = await ApiClient().getAllBucketResponse();
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);

      if (response.statusCode == 200 || response.statusCode == 201) {
        responseResult = AllBucketModel.fromJson(jsonMap);
        if (int.parse(id) == 1) {
          dataList.value = responseResult.data;
          return responseResult;
        } else {
          dataList.addAll(responseResult.data);
          return responseResult;
        }

      } else {
        Get.back();
        redSnack("Failed");
        return responseResult;
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
      return responseResult;

    }
  }
}
