import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/home_screen/home_ui.dart';
import 'package:cryptocase/portfolio_screen/portfolio_ui.dart';
import 'package:cryptocase/profile_screen/profile_screenui.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _index = 0;
  int selectedIndex;
  AdaptiveThemeMode adaptiveThemeMode;

  @override
  void initState() {
    super.initState();
    getTheme();
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var child;
    switch (_index) {
      case 0:
        child = HomeUi();
        break;
      case 1:
        child = PortfilioUi();
        break;
      case 2:
        child = ProfileScreenUi();
        break;
    }
    return Scaffold(
        backgroundColor:
            adaptiveThemeMode == AdaptiveThemeMode.light ? white : textdarkblue,
        body: SizedBox.expand(child: child),
        bottomNavigationBar: Row(
          children: [
            buildBottomNavigationBarItem('assets/Home.svg', 0),
            buildBottomNavigationBarItem('assets/Portfolio.svg', 1),
            buildBottomNavigationBarItem('assets/Profile.svg', 2),
          ],
        ));
  }

  Widget buildBottomNavigationBarItem(String image, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _index = index;
        });
      },
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 3,
        decoration: index == _index
            ? BoxDecoration(
                border: Border(
                  top: BorderSide(width: 4, color: textgreen),
                ),
              )
            : BoxDecoration(),
        child: Padding(
          padding: const EdgeInsets.all(17),
          child: SvgPicture.asset(
              image,
              color: index == _index
                  ? adaptiveThemeMode == AdaptiveThemeMode.light
                  ? textdarkblue
                  : white
                  : unselectedcolor,
            )
        ),
      ),
    );
  }
}
