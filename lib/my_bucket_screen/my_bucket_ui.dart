import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptocase/app_utils/colors.dart';
import 'package:cryptocase/app_utils/strings.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/home_screen/bucket_detail_model.dart';
import 'package:cryptocase/home_screen/bucket_details_controller.dart';
import 'package:cryptocase/login_screen/rebalance_screen.dart';
import 'package:cryptocase/my_bucket_screen/bucket_graph_controller.dart';
import 'package:cryptocase/my_bucket_screen/bucket_graph_model.dart' as bucket;
import 'package:cryptocase/my_bucket_screen/new_bucket_controller.dart';
import 'package:cryptocase/my_bucket_screen/new_bucket_model.dart' as token;
import 'package:cryptocase/portfolio_screen/portfolio_ui.dart';
import 'package:cryptocase/sell_invest_screen/invest_screen.dart';
import 'package:cryptocase/sell_invest_screen/sell_screen.dart';
import 'package:cryptocase/token_screen/token_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:intl/intl.dart';

class SalesData {
  SalesData(this.year, this.sales);

  final double year;
  final double sales;
}

class MyBucketUi extends StatefulWidget {
  @override
  _MyBucketUiState createState() => _MyBucketUiState();
}

class _MyBucketUiState extends State<MyBucketUi> {
  AdaptiveThemeMode adaptiveThemeMode;
  PageController _pageController = new PageController();
  final buckketController = Get.put(BucketDetailController());
  final bucketgraphController = Get.put(BucketGraphController());
  final newbucketcontroller = Get.put(NewBucketController());
  bool isTabselected = true;
  List<SalesData> _linechartData;
  List<bucket.Datum> _qraphchartData;
  TooltipBehavior _linetooltipBehavior;
  bool isBtnOneselected = true;
  bool isBtnTwoselected = false;
  bool isBtnThreeselected = false;
  bool isBtnFourselected = false;
  bool isBtnFiveselected = false;
  bool isBtnSixselected = false;
  bool isLoggedin = false;
  int position = Get.arguments;
  int tokenscreen = 0;
  String tokenname;
  BucketDetailModel crypto;

  @override
  void initState() {
    super.initState();
    getTheme();
    checkLogin();
    getBucketData();
    _linechartData = getLineChartData();
    _linetooltipBehavior = TooltipBehavior(enable: true);
  }

  void getTheme() async {
    adaptiveThemeMode = await AdaptiveTheme.getThemeMode();
    setState(() {});
  }

  void checkLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    isLoggedin = preferences.getBool(LOGGED_IN) ?? false;
    setState(() {});
  }

  Future<BucketDetailModel> getBucketData() async {
    return await buckketController.fetchBucketDetailResponse(position);
  }

  Future<List<bucket.Datum>> getBucketGraph() async {
    var bucketGraph = await bucketgraphController.fetchTokenResponse(position);
    return bucketGraph.data;
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor:
          adaptiveThemeMode == AdaptiveThemeMode.light ? backgroundwhite : backgroundlightblue,
      body: FutureBuilder<BucketDetailModel>(
          future: getBucketData(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container(
                color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                child: Center(
                  child: CupertinoActivityIndicator(),
                ),
              );
            } else {
              return Column(
                children: [
                  Container(
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                    padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: Container(
                      height: height * 0.08,
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? white : darktheme,
                      margin: EdgeInsets.only(top: height * 0.02),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.back(result: PortfilioUi());
                            },
                            child: Icon(Icons.arrow_back_ios,
                                color: adaptiveThemeMode == AdaptiveThemeMode.light
                                    ? textdarkblue
                                    : white),
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: width * 0.02),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    width: width * 0.11,
                                    height: width * 0.11,
                                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        color: textblue),
                                    child: SvgPicture.asset(
                                      'assets/drop_hedge_icon.svg',
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.symmetric(horizontal: 10),
                                      child: textWidget(
                                          //position==0?"All Weather Crypto 🌦️":"YOLO bucket (Meme Coins) 🚀",
                                          HEDGEBUCKETNAME,
                                          ScreenUtil().setSp(45, allowFontScalingSelf: true),
                                          FontWeight.w600,
                                          adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textdarkblue
                                              : white,
                                          TextAlign.start),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Container(
                              child: MediaQuery.removePadding(
                                context: context,
                                removeTop: true,
                                child: ListView(
                                  shrinkWrap: true,
                                  children: [
                                    Container(
                                      height: height * 0.4,
                                      child: Stack(
                                        children: [
                                          Container(
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.only(
                                                      bottomLeft: Radius.circular(25),
                                                      bottomRight: Radius.circular(25)),
                                                  color:
                                                      adaptiveThemeMode == AdaptiveThemeMode.light
                                                          ? white
                                                          : darktheme)),
                                          Container(
                                            padding: EdgeInsets.all(15),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                textWidget(
                                                    "Amount Invested",
                                                    ScreenUtil()
                                                        .setSp(35, allowFontScalingSelf: true),
                                                    FontWeight.w500,
                                                    adaptiveThemeMode == AdaptiveThemeMode.light
                                                        ? textgrey
                                                        : white,
                                                    TextAlign.start),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    textWidget(
                                                        snapshot.data.data.totalSellPrice
                                                            .toStringAsFixed(2),
                                                        ScreenUtil()
                                                            .setSp(80, allowFontScalingSelf: true),
                                                        FontWeight.w800,
                                                        adaptiveThemeMode == AdaptiveThemeMode.light
                                                            ? textdarkblue
                                                            : white,
                                                        TextAlign.start),
                                                    textWidget(
                                                        "+ ₹ 1,021 (5.47%)",
                                                        ScreenUtil()
                                                            .setSp(45, allowFontScalingSelf: true),
                                                        FontWeight.w400,
                                                        adaptiveThemeMode == AdaptiveThemeMode.light
                                                            ? textgreen
                                                            : textgreen,
                                                        TextAlign.start),
                                                  ],
                                                ),
                                                Expanded(
                                                  child: Container(
                                                      child: FutureBuilder<List<bucket.Datum>>(
                                                          future: getBucketGraph(),
                                                          builder: (context, snapshot) {
                                                            if (!snapshot.hasData) {
                                                              return Container(
                                                                color: adaptiveThemeMode ==
                                                                        AdaptiveThemeMode.light
                                                                    ? white
                                                                    : darktheme,
                                                                child: Center(
                                                                  child:
                                                                      CupertinoActivityIndicator(),
                                                                ),
                                                              );
                                                            } else {
                                                              return SfCartesianChart(
                                                                legend: Legend(isVisible: false),
                                                                tooltipBehavior:
                                                                    _linetooltipBehavior,
                                                                margin: EdgeInsets.symmetric(
                                                                    vertical: 10),
                                                                palette: [
                                                                  adaptiveThemeMode ==
                                                                          AdaptiveThemeMode.light
                                                                      ? textdarkblue
                                                                      : textgreen
                                                                ],
                                                                series: <ChartSeries>[
                                                                  SplineSeries<bucket.Datum, num>(
                                                                      dataSource: _qraphchartData,
                                                                      xValueMapper:
                                                                          (bucket.Datum sales, _) =>
                                                                              sales.createdAt.day,
                                                                      yValueMapper:
                                                                          (bucket.Datum sales, _) =>
                                                                              double.parse(sales
                                                                                  .currentValue),
                                                                      dataLabelSettings:
                                                                          DataLabelSettings(
                                                                              isVisible: false),
                                                                      enableTooltip: true)
                                                                ],
                                                                primaryXAxis: NumericAxis(
                                                                  edgeLabelPlacement:
                                                                      EdgeLabelPlacement.shift,
                                                                  isVisible: false,
                                                                ),
                                                                primaryYAxis: NumericAxis(
                                                                    labelFormat: '{value}M',
                                                                    isVisible: false,
                                                                    numberFormat:
                                                                        NumberFormat.simpleCurrency(
                                                                            decimalDigits: 0)),
                                                              );
                                                            }
                                                          })),
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(Radius.circular(7)),
                                                      color: adaptiveThemeMode ==
                                                              AdaptiveThemeMode.light
                                                          ? greybackground
                                                          : backgroundlightblue),
                                                  height: height * 0.05,
                                                  margin: EdgeInsets.only(bottom: 10),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            isBtnOneselected = true;
                                                            isBtnTwoselected = false;
                                                            isBtnThreeselected = false;
                                                            isBtnFourselected = false;
                                                            isBtnFiveselected = false;
                                                            isBtnSixselected = false;
                                                            _linechartData = getLineChartData();

                                                            setState(() {});
                                                          },
                                                          child: Container(
                                                            margin: EdgeInsets.only(right: 5),
                                                            height: double.infinity,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.all(
                                                                    Radius.circular(7)),
                                                                color: isBtnOneselected
                                                                    ? white
                                                                    : adaptiveThemeMode ==
                                                                            AdaptiveThemeMode.light
                                                                        ? greybackground
                                                                        : backgroundlightblue),
                                                            child: Center(
                                                              child: textWidget(
                                                                  "1h",
                                                                  ScreenUtil().setSp(40,
                                                                      allowFontScalingSelf: true),
                                                                  FontWeight.w500,
                                                                  isBtnOneselected
                                                                      ? textdarkblue
                                                                      : adaptiveThemeMode ==
                                                                              AdaptiveThemeMode
                                                                                  .light
                                                                          ? textgrey
                                                                          : white,
                                                                  TextAlign.start),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () async {
                                                            isBtnOneselected = false;
                                                            isBtnTwoselected = true;
                                                            isBtnThreeselected = false;
                                                            isBtnFourselected = false;
                                                            isBtnFiveselected = false;
                                                            isBtnSixselected = false;
                                                            _qraphchartData =
                                                                await getBucketGraph();
                                                            setState(() {});
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.symmetric(horizontal: 5),
                                                            height: double.infinity,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.all(
                                                                    Radius.circular(7)),
                                                                color: isBtnTwoselected
                                                                    ? white
                                                                    : adaptiveThemeMode ==
                                                                            AdaptiveThemeMode.light
                                                                        ? greybackground
                                                                        : backgroundlightblue),
                                                            child: Center(
                                                              child: textWidget(
                                                                  "24h",
                                                                  ScreenUtil().setSp(40,
                                                                      allowFontScalingSelf: true),
                                                                  FontWeight.w500,
                                                                  isBtnTwoselected
                                                                      ? textdarkblue
                                                                      : adaptiveThemeMode ==
                                                                              AdaptiveThemeMode
                                                                                  .light
                                                                          ? textgrey
                                                                          : white,
                                                                  TextAlign.start),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            isBtnOneselected = false;
                                                            isBtnTwoselected = false;
                                                            isBtnThreeselected = true;
                                                            isBtnFourselected = false;
                                                            isBtnFiveselected = false;
                                                            isBtnSixselected = false;
                                                            _linechartData = getLineChartData();

                                                            setState(() {});
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.symmetric(horizontal: 5),
                                                            height: double.infinity,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.all(
                                                                    Radius.circular(7)),
                                                                color: isBtnThreeselected
                                                                    ? white
                                                                    : adaptiveThemeMode ==
                                                                            AdaptiveThemeMode.light
                                                                        ? greybackground
                                                                        : backgroundlightblue),
                                                            child: Center(
                                                              child: textWidget(
                                                                  "7d",
                                                                  ScreenUtil().setSp(40,
                                                                      allowFontScalingSelf: true),
                                                                  FontWeight.w500,
                                                                  isBtnThreeselected
                                                                      ? textdarkblue
                                                                      : adaptiveThemeMode ==
                                                                              AdaptiveThemeMode
                                                                                  .light
                                                                          ? textgrey
                                                                          : white,
                                                                  TextAlign.start),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            isBtnOneselected = false;
                                                            isBtnTwoselected = false;
                                                            isBtnThreeselected = false;
                                                            isBtnFourselected = true;
                                                            isBtnFiveselected = false;
                                                            isBtnSixselected = false;
                                                            _linechartData =
                                                                getSecondLineChartData();

                                                            setState(() {});
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.symmetric(horizontal: 5),
                                                            height: double.infinity,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.all(
                                                                    Radius.circular(7)),
                                                                color: isBtnFourselected
                                                                    ? white
                                                                    : adaptiveThemeMode ==
                                                                            AdaptiveThemeMode.light
                                                                        ? greybackground
                                                                        : backgroundlightblue),
                                                            child: Center(
                                                              child: textWidget(
                                                                  "30d",
                                                                  ScreenUtil().setSp(40,
                                                                      allowFontScalingSelf: true),
                                                                  FontWeight.w500,
                                                                  isBtnFourselected
                                                                      ? textdarkblue
                                                                      : adaptiveThemeMode ==
                                                                              AdaptiveThemeMode
                                                                                  .light
                                                                          ? textgrey
                                                                          : white,
                                                                  TextAlign.start),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            isBtnOneselected = false;
                                                            isBtnTwoselected = false;
                                                            isBtnThreeselected = false;
                                                            isBtnFourselected = false;
                                                            isBtnFiveselected = true;
                                                            isBtnSixselected = false;
                                                            _linechartData = getLineChartData();

                                                            setState(() {});
                                                          },
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.symmetric(horizontal: 5),
                                                            height: double.infinity,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.all(
                                                                    Radius.circular(7)),
                                                                color: isBtnFiveselected
                                                                    ? white
                                                                    : adaptiveThemeMode ==
                                                                            AdaptiveThemeMode.light
                                                                        ? greybackground
                                                                        : backgroundlightblue),
                                                            child: Center(
                                                              child: textWidget(
                                                                  "1y",
                                                                  ScreenUtil().setSp(40,
                                                                      allowFontScalingSelf: true),
                                                                  FontWeight.w500,
                                                                  isBtnFiveselected
                                                                      ? textdarkblue
                                                                      : adaptiveThemeMode ==
                                                                              AdaptiveThemeMode
                                                                                  .light
                                                                          ? textgrey
                                                                          : white,
                                                                  TextAlign.start),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            isBtnOneselected = false;
                                                            isBtnTwoselected = false;
                                                            isBtnThreeselected = false;
                                                            isBtnFourselected = false;
                                                            isBtnFiveselected = false;
                                                            isBtnSixselected = true;
                                                            _linechartData =
                                                                getSecondLineChartData();

                                                            setState(() {});
                                                          },
                                                          child: Container(
                                                            margin: EdgeInsets.only(left: 5),
                                                            height: double.infinity,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.all(
                                                                    Radius.circular(7)),
                                                                color: isBtnSixselected
                                                                    ? white
                                                                    : adaptiveThemeMode ==
                                                                            AdaptiveThemeMode.light
                                                                        ? greybackground
                                                                        : backgroundlightblue),
                                                            child: Center(
                                                              child: textWidget(
                                                                  "All",
                                                                  ScreenUtil().setSp(40,
                                                                      allowFontScalingSelf: true),
                                                                  FontWeight.w500,
                                                                  isBtnSixselected
                                                                      ? textdarkblue
                                                                      : adaptiveThemeMode ==
                                                                              AdaptiveThemeMode
                                                                                  .light
                                                                          ? textgrey
                                                                          : white,
                                                                  TextAlign.start),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                      height: height * 0.06,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(12)),
                                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? greybackground
                                              : textgrey),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: () {
                                                isTabselected = true;
                                                _pageController.animateToPage(0,
                                                    duration: Duration(milliseconds: 500),
                                                    curve: Curves.ease);

                                                setState(() {});
                                              },
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(Radius.circular(12)),
                                                    color: isTabselected == true
                                                        ? textdarkblue
                                                        : adaptiveThemeMode ==
                                                                AdaptiveThemeMode.light
                                                            ? greybackground
                                                            : textgrey),
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 4, horizontal: 4),
                                                child: Center(
                                                  child: textWidget(
                                                      "Overview",
                                                      ScreenUtil()
                                                          .setSp(45, allowFontScalingSelf: true),
                                                      FontWeight.w500,
                                                      isTabselected == true
                                                          ? white
                                                          : adaptiveThemeMode ==
                                                                  AdaptiveThemeMode.light
                                                              ? textgrey
                                                              : textborder,
                                                      TextAlign.start),
                                                ),
                                                height: height,
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: () {
                                                isTabselected = false;
                                                _pageController.animateToPage(1,
                                                    duration: Duration(milliseconds: 500),
                                                    curve: Curves.ease);

                                                setState(() {});
                                              },
                                              child: Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(Radius.circular(12)),
                                                      color: isTabselected == false
                                                          ? textdarkblue
                                                          : adaptiveThemeMode ==
                                                                  AdaptiveThemeMode.light
                                                              ? greybackground
                                                              : textgrey),
                                                  margin: EdgeInsets.symmetric(
                                                      vertical: 4, horizontal: 4),
                                                  child: Center(
                                                    child: textWidget(
                                                        "Weightage",
                                                        ScreenUtil()
                                                            .setSp(45, allowFontScalingSelf: true),
                                                        FontWeight.w500,
                                                        isTabselected == false
                                                            ? white
                                                            : adaptiveThemeMode ==
                                                                    AdaptiveThemeMode.light
                                                                ? textgrey
                                                                : textborder,
                                                        TextAlign.start),
                                                  )),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                    // buildOverviewPage(context, height, width),
                                    Container(
                                        margin: EdgeInsets.symmetric(horizontal: 15),
                                        child: buildPageView(snapshot, context, height, width))
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: height * 0.15,
                            margin: EdgeInsets.only(left: 15, right: 15, bottom: 15),
                            child: Column(
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            if (isLoggedin) {
                                              Get.to(() => SellScreenUi());
                                            } else {
                                              buildBottomeLoginScreen(context, adaptiveThemeMode);
                                            }
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(right: 5, top: 5, bottom: 5),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                                color: textred),
                                            child: Center(
                                              child: Text(
                                                "Sell",
                                                style: TextStyle(
                                                    color: white,
                                                    fontFamily: 'WorkSans',
                                                    fontSize: ScreenUtil()
                                                        .setSp(45, allowFontScalingSelf: true),
                                                    fontWeight: FontWeight.w500),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            if (isLoggedin) {
                                              Get.to(() => InvestScreenUi());
                                            } else {
                                              buildBottomeLoginScreen(context, adaptiveThemeMode);
                                            }
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(left: 5, top: 5, bottom: 5),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                                color: buttongreen),
                                            child: Center(
                                              child: Text(
                                                "Buy More",
                                                style: TextStyle(
                                                    color: white,
                                                    fontFamily: 'WorkSans',
                                                    fontSize: ScreenUtil()
                                                        .setSp(45, allowFontScalingSelf: true),
                                                    fontWeight: FontWeight.w500),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      if (isLoggedin) {
                                        Get.to(() => RebalanceScreen());
                                      } else {
                                        buildBottomeLoginScreen(context, adaptiveThemeMode);
                                      }
                                    },
                                    child: Container(
                                      width: width,
                                      margin: EdgeInsets.symmetric(vertical: 5),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(12),
                                        ),
                                        border: Border.all(
                                          width: 1,
                                          color: adaptiveThemeMode == AdaptiveThemeMode.light
                                              ? textgrey
                                              : white,
                                          style: BorderStyle.solid,
                                        ),
                                      ),
                                      padding:
                                          EdgeInsets.only(left: 30, top: 10, right: 30, bottom: 10),
                                      child: adaptiveThemeMode == AdaptiveThemeMode.light
                                          ? Center(
                                              child: textWidget(
                                                  "Rebalance",
                                                  ScreenUtil()
                                                      .setSp(50, allowFontScalingSelf: true),
                                                  FontWeight.w500,
                                                  textdarkblue,
                                                  TextAlign.start))
                                          : Center(
                                              child: textWidget(
                                                  "Rebalance",
                                                  ScreenUtil()
                                                      .setSp(50, allowFontScalingSelf: true),
                                                  FontWeight.w500,
                                                  white,
                                                  TextAlign.start)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            }
          }),
    );
  }

  Widget buildPageView(AsyncSnapshot<BucketDetailModel> snapshot, BuildContext context,
      double height, double width) {
    return Container(
      height: height * 0.8,
      child: PageView(
        controller: _pageController,
        scrollDirection: Axis.horizontal,
        onPageChanged: (page) {
          if (page == 0) {
            isTabselected = true;
            setState(() {});
          } else {
            isTabselected = false;
            setState(() {});
          }
        },
        children: [
          buildOverviewPage(snapshot, context, height, width),
          buildWeightagePage(snapshot, context, height, width)
        ],
      ),
    );
  }

  buildOverviewPage(AsyncSnapshot<BucketDetailModel> snapshot, BuildContext context, double height,
      double width) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Html(
                  data: "About the ${HEDGEBUCKETNAME}",
                  defaultTextStyle: TextStyle(
                    fontSize: ScreenUtil().setSp(45, allowFontScalingSelf: true),
                    fontWeight: FontWeight.w500,
                    color: adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  ),
                ),
                //TextAlign.start pending
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Expanded(
                    child: Html(
                      data: HEDGEBUCKETDESCRIPTION,
                      defaultTextStyle: TextStyle(
                        fontSize: ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        fontWeight: FontWeight.w400,
                        color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Container(
                    width: width,
                    child: textWidget(
                        "Read More",
                        ScreenUtil().setSp(35, allowFontScalingSelf: true),
                        FontWeight.w400,
                        adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                        TextAlign.end),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                textWidget(
                    "Perfomance",
                    ScreenUtil().setSp(45, allowFontScalingSelf: true),
                    FontWeight.w500,
                    adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                    TextAlign.start),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: textWidget(
                      "Current value of ₹ 550 invested once on inception would be",
                      ScreenUtil().setSp(35, allowFontScalingSelf: true),
                      FontWeight.w400,
                      adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                      TextAlign.start),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildWeightagePage(AsyncSnapshot<BucketDetailModel> snapshot, BuildContext context,
      double height, double width) {
    num amount = totalAmount(snapshot.data.data.bucketTokens).toDouble();
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textWidget(
                  'Balance to coins',
                  ScreenUtil().setSp(37, allowFontScalingSelf: true),
                  FontWeight.w500,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  TextAlign.start),
              textWidget(
                  'Weightage(%)',
                  ScreenUtil().setSp(37, allowFontScalingSelf: true),
                  FontWeight.w500,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  TextAlign.start),
            ],
          ),
        ),
        position == 0 ? buidChartOne() : buidChartTwo(),

        Container(
          height: height * 0.1,
          child: ListView.builder(
              itemCount: snapshot.data.data.bucketTokens.length,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, int index) {
                return Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Get.to(() => TokenUi(), arguments: snapshot.data.data.bucketTokens[index]);
                      },
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0, right: 8, bottom: 8, left: 8),
                            child: SvgPicture.asset('assets/ethereum_icon.svg'),
                          ),
                          Expanded(
                            child: textWidget(
                                snapshot.data.data.bucketTokens[index].token.tokenName,
                                ScreenUtil().setSp(35, allowFontScalingSelf: true),
                                FontWeight.w400,
                                adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                                TextAlign.start),
                          ),
                          textWidget(
                              "${percentage(amount, snapshot.data.data.bucketTokens[index].quantity)}%",
                              ScreenUtil().setSp(35, allowFontScalingSelf: true),
                              FontWeight.w500,
                              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                              TextAlign.start),
                        ],
                      ),
                    ),
                    Divider(
                      color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
                    ),
                  ],
                );
              }),
        ),
        Visibility(
          visible: position == 0 ? true : false,
          child: Divider(
            color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: textWidget(
              'Statistics',
              ScreenUtil().setSp(45, allowFontScalingSelf: true),
              FontWeight.w500,
              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              TextAlign.start),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: textWidget(
              'Low/High',
              ScreenUtil().setSp(35, allowFontScalingSelf: true),
              FontWeight.w400,
              adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
              TextAlign.start),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: LinearPercentIndicator(
            percent: 0.4,
            lineHeight: 10,
            backgroundColor: Color.fromRGBO(0, 0, 0, 0.05),
            progressColor: textgreen,
            padding: const EdgeInsets.all(0.0),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textWidget(
                  '₹ 0.9865',
                  ScreenUtil().setSp(35, allowFontScalingSelf: true),
                  FontWeight.w500,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  TextAlign.start),
              textWidget(
                  '₹ 0.99905',
                  ScreenUtil().setSp(35, allowFontScalingSelf: true),
                  FontWeight.w500,
                  adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                  TextAlign.start),
            ],
          ),
        ),
        Divider(
          color: adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : darktheme,
        ),
        Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: textWidget(
                          'Market Cap',
                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                          TextAlign.start),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: textWidget(
                            'Volume 24h',
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: textWidget(
                          '₹ 345.168 M',
                          ScreenUtil().setSp(32, allowFontScalingSelf: true),
                          FontWeight.w600,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                          TextAlign.start),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: textWidget(
                            '₹ 345,1093.5',
                            ScreenUtil().setSp(32, allowFontScalingSelf: true),
                            FontWeight.w600,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: textWidget(
                          'Circ Supply',
                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                          TextAlign.start),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: textWidget(
                            'Max Supply',
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: textWidget(
                          '₹ 2985.168 M',
                          ScreenUtil().setSp(32, allowFontScalingSelf: true),
                          FontWeight.w600,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                          TextAlign.start),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: textWidget(
                            'N/A',
                            ScreenUtil().setSp(32, allowFontScalingSelf: true),
                            FontWeight.w600,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: textWidget(
                          'Total Supply',
                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                          TextAlign.start),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: textWidget(
                            'Rank',
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: textWidget(
                          '1.000 Bn',
                          ScreenUtil().setSp(32, allowFontScalingSelf: true),
                          FontWeight.w600,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                          TextAlign.start),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: textWidget(
                            '#120',
                            ScreenUtil().setSp(32, allowFontScalingSelf: true),
                            FontWeight.w600,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: textWidget(
                          'All Time High',
                          ScreenUtil().setSp(40, allowFontScalingSelf: true),
                          FontWeight.w500,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                          TextAlign.start),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: textWidget(
                            'Mkt Dominance',
                            ScreenUtil().setSp(40, allowFontScalingSelf: true),
                            FontWeight.w500,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textdarkblue : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: textWidget(
                          '₹ 6.849',
                          ScreenUtil().setSp(32, allowFontScalingSelf: true),
                          FontWeight.w600,
                          adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                          TextAlign.start),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: textWidget(
                            '9.04%',
                            ScreenUtil().setSp(32, allowFontScalingSelf: true),
                            FontWeight.w600,
                            adaptiveThemeMode == AdaptiveThemeMode.light ? textgrey : white,
                            TextAlign.start),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<SalesData> getLineChartData() {
    final List<SalesData> chartData = [
      SalesData(2017, 40),
      SalesData(2018, 12),
      SalesData(2019, 50),
      SalesData(2020, 30),
      SalesData(2021, 80)
    ];
    return chartData;
  }

  List<SalesData> getSecondLineChartData() {
    final List<SalesData> chartData = [
      SalesData(2017, 10),
      SalesData(2018, 18),
      SalesData(2019, 80),
      SalesData(2020, 30),
      SalesData(2021, 10)
    ];
    return chartData;
  }

  Container buidChartOne() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: SfLinearGauge(
          useRangeColorForAxis: true,
          animateAxis: true,
          showLabels: false,
          showAxisTrack: false,
          showTicks: false,
          axisTrackStyle: LinearAxisTrackStyle(thickness: 15, edgeStyle: LinearEdgeStyle.bothCurve),
          ranges: <LinearGaugeRange>[
            LinearGaugeRange(
                startValue: 0,
                endValue: 30,
                position: LinearElementPosition.outside,
                color: barblack),
            LinearGaugeRange(
                startValue: 30,
                endValue: 50,
                position: LinearElementPosition.outside,
                color: baryellow),
            LinearGaugeRange(
                startValue: 50,
                endValue: 65,
                position: LinearElementPosition.outside,
                color: bargreen),
            LinearGaugeRange(
                startValue: 65,
                endValue: 75,
                position: LinearElementPosition.outside,
                color: barblue),
            LinearGaugeRange(
                startValue: 75,
                endValue: 100,
                position: LinearElementPosition.outside,
                color: barpink),
          ],
        ));
  }

  Container buidChartTwo() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: SfLinearGauge(
          useRangeColorForAxis: true,
          animateAxis: true,
          showLabels: false,
          showAxisTrack: false,
          showTicks: false,
          axisTrackStyle: LinearAxisTrackStyle(thickness: 15, edgeStyle: LinearEdgeStyle.bothCurve),
          ranges: <LinearGaugeRange>[
            LinearGaugeRange(
                startValue: 0,
                endValue: 19,
                position: LinearElementPosition.outside,
                color: barblack),
            LinearGaugeRange(
                startValue: 19,
                endValue: 65,
                position: LinearElementPosition.outside,
                color: baryellow),
            LinearGaugeRange(
                startValue: 65,
                endValue: 88,
                position: LinearElementPosition.outside,
                color: bargreen),
            LinearGaugeRange(
                startValue: 88,
                endValue: 100,
                position: LinearElementPosition.outside,
                color: barblue),
          ],
        ));
  }

  num totalAmount(List<BucketToken> bucketToken) {
    num sum = 0;
    for (var data in bucketToken) {
      sum += data.quantity;
    }
    return sum;
  }

  String percentage(num totalAmount, num quantity){
    return ((quantity * 100)/totalAmount).round().toString();
  }

}
