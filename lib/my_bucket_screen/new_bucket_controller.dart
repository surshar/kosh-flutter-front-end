import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/my_bucket_screen/bucket_graph_model.dart';
import 'package:cryptocase/my_bucket_screen/new_bucket_model.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../api_client.dart';

class NewBucketController extends GetxController {
  Future<NewBucketModel> fetchTokenResponse(int bucket_id) async {
    var responseResult = NewBucketModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response =
      await ApiClient().getAllBucketResponse();
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = NewBucketModel.fromJson(jsonMap);
      if (response.statusCode == 200 || response.statusCode == 201) {
        print("new model reponse : ${responseResult.toJson()}");
        return responseResult;
      } else {
        print("new model error reponse : ${responseResult.toJson()}");
        return responseResult;
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
      return responseResult;
    }
  }
}
