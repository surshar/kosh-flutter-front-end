import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:cryptocase/app_utils/widgets.dart';
import 'package:cryptocase/my_bucket_screen/bucket_graph_model.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../api_client.dart';

class BucketGraphController extends GetxController {
  Future<BucketQraphModel> fetchTokenResponse(int bucket_id) async {
    var responseResult = BucketQraphModel();

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      http.Response response = await ApiClient().getBucketGraphResponse(bucket_id);
      var jsonString = response.body;
      var jsonMap = json.decode(jsonString);
      responseResult = BucketQraphModel.fromJson(jsonMap);
      if (response.statusCode == 200 || response.statusCode == 201) {
        return responseResult;
      } else {
        return responseResult;
      }
    } else {
      Get.back();
      redSnack("Please check your internet connection");
      return responseResult;
    }
  }
}
