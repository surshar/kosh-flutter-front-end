
import 'dart:convert';

NewBucketModel newBucketModelFromJson(String str) => NewBucketModel.fromJson(json.decode(str));

String newBucketModelToJson(NewBucketModel data) => json.encode(data.toJson());

class NewBucketModel {
  NewBucketModel({
    this.status,
    this.data,
  });

  String status;
  List<Datum> data;

  factory NewBucketModel.fromJson(Map<String, dynamic> json) => NewBucketModel(
    status: json["status"],
    data: json["status"] == "success" ? List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))): null,
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data":status =="success"? List<dynamic>.from(data.map((x) => x.toJson())): null,
  };
}

class Datum {
  Datum({
    this.id,
    this.versionNo,
    this.bucketName,
    this.bucketImage,
    this.bucketDescription,
    this.exchangeId,
    this.oldFlag,
    this.versions,
    this.createdBy,
    this.lastUpdatedBy,
    this.createdAt,
    this.updatedAt,
    this.bucketTokens,
  });

  int id;
  String versionNo;
  String bucketName;
  dynamic bucketImage;
  String bucketDescription;
  int exchangeId;
  dynamic oldFlag;
  dynamic versions;
  int createdBy;
  int lastUpdatedBy;
  DateTime createdAt;
  DateTime updatedAt;
  List<BucketToken> bucketTokens;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    versionNo: json["version_no"],
    bucketName: json["bucket_name"],
    bucketImage: json["bucket_image"],
    bucketDescription: json["bucket_description"],
    exchangeId: json["exchange_id"],
    oldFlag: json["old_flag"],
    versions: json["versions"],
    createdBy: json["created_by"],
    lastUpdatedBy: json["last_updated_by"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    bucketTokens:json["BucketTokens"] != null ? List<BucketToken>.from(json["BucketTokens"].map((x) => BucketToken.fromJson(x))): null,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "version_no": versionNo,
    "bucket_name": bucketName,
    "bucket_image": bucketImage,
    "bucket_description": bucketDescription,
    "exchange_id": exchangeId,
    "old_flag": oldFlag,
    "versions": versions,
    "created_by": createdBy,
    "last_updated_by": lastUpdatedBy,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "BucketTokens":bucketTokens != null ? List<dynamic>.from(bucketTokens.map((x) => x.toJson())): null,
  };
}

class BucketToken {
  BucketToken({
    this.quantity,
    this.id,
    this.bucketId,
    this.tokenId,
    this.createdAt,
    this.updatedAt,
    this.token,
  });

  double quantity;
  int id;
  int bucketId;
  int tokenId;
  DateTime createdAt;
  DateTime updatedAt;
  Token token;

  factory BucketToken.fromJson(Map<String, dynamic> json) => BucketToken(
    quantity: json["quantity"].toDouble(),
    id: json["id"],
    bucketId: json["bucket_id"],
    tokenId: json["token_id"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    token: Token.fromJson(json["Token"]),
  );

  Map<String, dynamic> toJson() => {
    "quantity": quantity,
    "id": id,
    "bucket_id": bucketId,
    "token_id": tokenId,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "Token": token.toJson(),
  };
}

class Token {
  Token({
    this.id,
    this.tokenName,
    this.tokenImage,
    this.tokenAbbr,
    this.baseValueVauld,
    this.tokenDescription,
    this.createdBy,
    this.lastUpdatedBy,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String tokenName;
  dynamic tokenImage;
  String tokenAbbr;
  double baseValueVauld;
  TokenDescription tokenDescription;
  int createdBy;
  dynamic lastUpdatedBy;
  DateTime createdAt;
  DateTime updatedAt;

  factory Token.fromJson(Map<String, dynamic> json) => Token(
    id: json["id"],
    tokenName: json["token_name"],
    tokenImage: json["token_image"],
    tokenAbbr: json["token_abbr"],
    baseValueVauld: json["base_value_vauld"].toDouble(),
    tokenDescription: json["token_description"] == null ? null : tokenDescriptionValues.map[json["token_description"]],
    createdBy: json["created_by"],
    lastUpdatedBy: json["last_updated_by"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "token_name": tokenName,
    "token_image": tokenImage,
    "token_abbr": tokenAbbr,
    "base_value_vauld": baseValueVauld,
    "token_description": tokenDescription == null ? null : tokenDescriptionValues.reverse[tokenDescription],
    "created_by": createdBy,
    "last_updated_by": lastUpdatedBy,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
  };
}

enum TokenDescription { EMPTY, P_DIV }

final tokenDescriptionValues = EnumValues({
  "": TokenDescription.EMPTY,
  "</p></div>": TokenDescription.P_DIV
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
